#!/bin/bash
set -xe
mkdir temp
cd temp
npm init -y
npm install esbuild

cat >index.js <<-EOF
import {
  argbFromHex,
  DynamicColor,
  DynamicScheme,
  hexFromArgb,
  MaterialDynamicColors,
  themeFromSourceColor,
} from '@material/material-color-utilities';

const params = new URLSearchParams(location.search);

const sourceColor = params.get('source-color') || localStorage.getItem('source-color') || '#856222';
localStorage.setItem('source-color', sourceColor);

let contrast = params.get('contrast-level');
if (contrast === undefined || contrast === null) {
  contrast = localStorage.getItem('contrast-level');
}
contrast = contrast ? Number(contrast) : 0.5;
localStorage.setItem('contrast-level', contrast);

let isDark = params.get('is-dark');
if (isDark === undefined || isDark === null) {
  isDark = localStorage.getItem('is-dark');
}
isDark = isDark ? isDark === "true" : true;
localStorage.setItem('is-dark', isDark);

const getColors = () => {
  return Object.values(MaterialDynamicColors).filter(
    (e) => e instanceof DynamicColor && e.name && !e.name.includes('palette'),
  );
};

const COLORS = getColors();

const color = argbFromHex(sourceColor);
const colorPalette = themeFromSourceColor(color);
const scheme = new DynamicScheme({
  sourceColorArgb: color,
  variant: 1,
  contrastLevel: contrast,
  isDark,
  neutralPalette: colorPalette.palettes.neutral,
  neutralVariantPalette: colorPalette.palettes.neutralVariant,
  primaryPalette: colorPalette.palettes.primary,
  secondaryPalette: colorPalette.palettes.secondary,
  tertiaryPalette: colorPalette.palettes.tertiary,
});
for (const color of COLORS) {
  document.documentElement.style.setProperty(
    \`--mat-sys-\${color.name.replaceAll('_', '-')}\`,
    hexFromArgb(scheme.getArgb(color)),
  );
}
EOF

npx esbuild index.js --bundle --minify --format=iife --outfile=color-apply.min.js
cp color-apply.min.js ../themes/fragjetzt/login/resources/js/color-apply.min.js
cd ..
rm -rf temp
