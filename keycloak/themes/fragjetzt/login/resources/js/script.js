function initButtons() {
  for (const button of document.querySelectorAll(
    ".mat-button, .mat-icon-button, .pf-c-button, a:not(.no-button)",
  )) {
    const set = new Set();
    button.addEventListener("mousedown", (e) => {
      const ripple = document.createElement("span");
      ripple.classList.add("ripple-element");
      const rect = button.getBoundingClientRect();
      const x = rect.width / 2 + Math.abs(rect.width / 2 - e.offsetX);
      const y = rect.height / 2 + Math.abs(rect.height / 2 - e.offsetY);
      const width = Math.sqrt(x * x + y * y) * 2;
      ripple.style.height = `${width}px`;
      ripple.style.width = `${width}px`;
      ripple.style.left = `${e.offsetX - width / 2}px`;
      ripple.style.top = `${e.offsetY - width / 2}px`;
      button.append(ripple);
      set.add(ripple);
      ripple.addEventListener(
        "transitionend",
        () => {
          if (!set.delete(ripple)) ripple.remove();
        },
        {
          once: true,
        },
      );
      setTimeout(() => {
        ripple.style.transform = "scale3d(1, 1, 1)";
      });
    });
    button.addEventListener("mouseup", () => {
      for (const ripple of button.querySelectorAll("span.ripple-element")) {
        if (!set.delete(ripple)) ripple.remove();
      }
    });
    button.addEventListener("mouseleave", () => {
      for (const ripple of button.querySelectorAll("span.ripple-element")) {
        if (!set.delete(ripple)) ripple.remove();
      }
    });
  }

  for (const button of document.querySelectorAll("button[data-menu-target]")) {
    button.addEventListener("click", (e) => {
      e.preventDefault();
      e.stopImmediatePropagation();
      const target = button.dataset?.menuTarget;
      const elem = target
        ? document.getElementById(target)
        : button.nextElementSibling;
      elem.style.visibility = "";
      const rect = button.getBoundingClientRect();
      elem.style.top = `${rect.bottom}px`;
      elem.style.right = `${innerWidth - rect.right}px`;
      let cleanup = () => "";
      document.addEventListener(
        "click",
        () => {
          elem.style.visibility = "hidden";
          cleanup();
        },
        { once: true },
      );
      // check menu
      if (elem.dataset.active) {
        const active = elem.dataset.active;
        for (const c of elem.children) {
          const container = c.querySelector("button");
          const i = container.querySelector("i.mat-icon");
          const text = container.innerText;
          if (text?.endsWith(active)) {
            container.disabled = true;
            c.style.pointerEvents = "none";
            c.disabled = true;
            const before = i.innerText;
            i.innerText = "check";
            cleanup = () => {
              container.disabled = false;
              c.style.pointerEvents = "";
              c.disabled = false;
              i.innerText = before;
            };
            break;
          }
        }
      }
    });
  }
}

function initInputs() {
  for (const elem of document.querySelectorAll("input.pf-c-form-control")) {
    if (!elem.placeholder) {
      elem.placeholder = " ";
    }
  }
}

addEventListener("load", () => {
  if (location.pathname.endsWith("/login-actions/registration")) {
    // firstname
    const firstnameInput = document.getElementById("firstName");
    firstnameInput.parentElement.parentElement.style.display = "none";
    // lastname
    const lastnameInput = document.getElementById("lastName");
    lastnameInput.parentElement.parentElement.style.display = "none";
  }
  initInputs();
  initButtons();
});
