#!/bin/bash

SED_BIN='sed'
HEX_OPT='--hex'

# Check for MacOs
if [[ $OSTYPE == 'darwin'* ]]; then
  if ! command -v gsed &> /dev/null; then
    echo "gsed could not be found"
    echo "Please install the Gnu Core Utilities:"
    echo "  brew install coreutils findutils gnu-tar gnu-sed gawk gnutls gnu-indent gnu-getopt grep"
    exit
  fi
  SED_BIN='gsed'
  HEX_OPT='-hex'
fi

# escape for sed parameter
function escape {
  echo "${1}" | $SED_BIN -e 's/\\/\\\\/g' -e 's/\$/\\$/g' -e 's/`/\\`/g' -e 's/"/\\"/g' -e 's/\//\\\//g'
}
