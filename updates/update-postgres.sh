#!/bin/bash
function writeEnv {
  sed -i "s/FJ_DB_VERSION=.*/FJ_DB_VERSION=$(escape "${FJ_DB_VERSION}")/" .env
  sed -i "s/FJ_DB_VERSION=.*/FJ_DB_VERSION='$(escape "${FJ_DB_VERSION}")'/" .postgres-version.env
}

function waitForConnection {
  while ! sudo docker compose exec -Ti postgres pg_isready -U fragjetzt > /dev/null; do
    sleep 1
  done
  sleep 1
}

function doMigration {
  waitForConnection
  sudo docker compose exec -T -i postgres psql -q -U fragjetzt -c "CREATE ROLE migrator WITH SUPERUSER INHERIT CREATEROLE CREATEDB LOGIN REPLICATION BYPASSRLS PASSWORD NULL;"
  sudo docker compose exec -T -i postgres psql -q -d postgres -U migrator < temp-dump.sql
  sudo docker compose exec -T -i postgres psql -q -U fragjetzt -c "REASSIGN OWNED BY migrator TO fragjetzt; DROP ROLE migrator;"
}

cd "$(dirname "$0")/../"
source scripts/util.sh

if [ -z "$1" ]; then
  echo "Usage: $0 <version>"
  exit 1
fi

if [ -f temp-dump.sql ]; then
  echo "Please delete the temp-dump.sql file before running this script."
  exit 1
fi

set -e

# Update the postgres container
sudo docker compose up -d postgres
waitForConnection
sudo docker compose exec postgres pg_dumpall -c -U fragjetzt > temp-dump.sql
sudo docker compose down -v postgres

# Update the postgres image
sudo docker pull postgres:$1

# Start the postgres container
FJ_DB_VERSION=$1
writeEnv
sudo docker compose up -d postgres
doMigration

# Clean up
echo "When you verified the data, you can delete the temp-dump.sql file."
