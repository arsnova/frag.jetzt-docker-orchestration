#!/bin/bash
psql -U "${POSTGRES_USER:-langchain}" -d "${POSTGRES_DB:-langchain}" <<EOF
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE OR REPLACE FUNCTION trigger_timestamp_create_update_func()
    RETURNS trigger AS
\$\$
DECLARE
BEGIN
    IF (TG_OP = 'INSERT') THEN
        NEW.created_at = NOW();
        NEW.updated_at = NULL;
        RETURN NEW;
    ELSEIF (TG_OP = 'UPDATE') THEN
        IF (OLD.created_at <> NEW.created_at) THEN
            NEW.created_at = OLD.created_at;
        END IF;
        NEW.updated_at = NOW();
        RETURN NEW;
    ELSEIF (TG_OP = 'DELETE') THEN
        RETURN OLD;
    END IF;
    RETURN NULL;
END;
\$\$ LANGUAGE plpgsql;
EOF


echo "Waiting for master to be available..."
while ! pg_isready -h 192.168.1.2 -U replicator -d fragjetzt -t 30 2>/dev/null; do
  echo "Retry..."
  sleep 1
done

echo "Master is available. Wait for migration to finish..."

COUNT=$(PGPASSWORD="replication_password" psql -h 192.168.1.2 -U replicator -d fragjetzt -c "SELECT COUNT(*) FROM flyway_schema_history;" 2>/dev/null | grep "^[[:blank:]]*[[:digit:]]*[[:blank:]]*$" | tr -d ' ' || echo "N")
re='^[0-9]+$'
while ! [[ $COUNT =~ $re ]] ; do
  echo "Retry..."
  sleep 2
  COUNT=$(PGPASSWORD="replication_password" psql -h 192.168.1.2 -U replicator -d fragjetzt -c "SELECT COUNT(*) FROM flyway_schema_history;" 2>/dev/null | grep "^[[:blank:]]*[[:digit:]]*[[:blank:]]*$" | tr -d ' ' || echo "N")
done

echo "Migration finished. Start replication..."

PGPASSWORD="replication_password" psql -h 192.168.1.2 -U replicator -d fragjetzt -c "select pg_drop_replication_slot('fragjetzt_subscription_replica_1');" 2>/dev/null || { echo "Initial setup"; }

PGPASSWORD="replication_password" pg_dump -h 192.168.1.2 -U replicator -d fragjetzt -s \
  -t room -t account -t room_access -t account_keycloak_role -O -x | psql -U "${POSTGRES_USER:-langchain}" \
  -d "${POSTGRES_DB:-langchain}"

psql -U "${POSTGRES_USER:-langchain}" -d "${POSTGRES_DB:-langchain}" <<EOF
CREATE SUBSCRIPTION fragjetzt_subscription_replica_1
CONNECTION 'host=192.168.1.2 port=5432 dbname=fragjetzt user=replicator password=replication_password'
PUBLICATION fragjetzt_primary_publication;
EOF

if [ $? -ne 0 ]; then
  echo "Failed to create subscription"
  exit 1
fi

echo "Replica 1 is ready"

