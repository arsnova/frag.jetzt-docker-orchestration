--
-- PostgreSQL database dump
--

-- Dumped from database version 15.6
-- Dumped by pg_dump version 15.6

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: admin_event_entity; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.admin_event_entity (
    id character varying(36) NOT NULL,
    admin_event_time bigint,
    realm_id character varying(255),
    operation_type character varying(255),
    auth_realm_id character varying(255),
    auth_client_id character varying(255),
    auth_user_id character varying(255),
    ip_address character varying(255),
    resource_path character varying(2550),
    representation text,
    error character varying(255),
    resource_type character varying(64)
);


ALTER TABLE public.admin_event_entity OWNER TO keycloak;

--
-- Name: associated_policy; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.associated_policy (
    policy_id character varying(36) NOT NULL,
    associated_policy_id character varying(36) NOT NULL
);


ALTER TABLE public.associated_policy OWNER TO keycloak;

--
-- Name: authentication_execution; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.authentication_execution (
    id character varying(36) NOT NULL,
    alias character varying(255),
    authenticator character varying(36),
    realm_id character varying(36),
    flow_id character varying(36),
    requirement integer,
    priority integer,
    authenticator_flow boolean DEFAULT false NOT NULL,
    auth_flow_id character varying(36),
    auth_config character varying(36)
);


ALTER TABLE public.authentication_execution OWNER TO keycloak;

--
-- Name: authentication_flow; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.authentication_flow (
    id character varying(36) NOT NULL,
    alias character varying(255),
    description character varying(255),
    realm_id character varying(36),
    provider_id character varying(36) DEFAULT 'basic-flow'::character varying NOT NULL,
    top_level boolean DEFAULT false NOT NULL,
    built_in boolean DEFAULT false NOT NULL
);


ALTER TABLE public.authentication_flow OWNER TO keycloak;

--
-- Name: authenticator_config; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.authenticator_config (
    id character varying(36) NOT NULL,
    alias character varying(255),
    realm_id character varying(36)
);


ALTER TABLE public.authenticator_config OWNER TO keycloak;

--
-- Name: authenticator_config_entry; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.authenticator_config_entry (
    authenticator_id character varying(36) NOT NULL,
    value text,
    name character varying(255) NOT NULL
);


ALTER TABLE public.authenticator_config_entry OWNER TO keycloak;

--
-- Name: broker_link; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.broker_link (
    identity_provider character varying(255) NOT NULL,
    storage_provider_id character varying(255),
    realm_id character varying(36) NOT NULL,
    broker_user_id character varying(255),
    broker_username character varying(255),
    token text,
    user_id character varying(255) NOT NULL
);


ALTER TABLE public.broker_link OWNER TO keycloak;

--
-- Name: client; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.client (
    id character varying(36) NOT NULL,
    enabled boolean DEFAULT false NOT NULL,
    full_scope_allowed boolean DEFAULT false NOT NULL,
    client_id character varying(255),
    not_before integer,
    public_client boolean DEFAULT false NOT NULL,
    secret character varying(255),
    base_url character varying(255),
    bearer_only boolean DEFAULT false NOT NULL,
    management_url character varying(255),
    surrogate_auth_required boolean DEFAULT false NOT NULL,
    realm_id character varying(36),
    protocol character varying(255),
    node_rereg_timeout integer DEFAULT 0,
    frontchannel_logout boolean DEFAULT false NOT NULL,
    consent_required boolean DEFAULT false NOT NULL,
    name character varying(255),
    service_accounts_enabled boolean DEFAULT false NOT NULL,
    client_authenticator_type character varying(255),
    root_url character varying(255),
    description character varying(255),
    registration_token character varying(255),
    standard_flow_enabled boolean DEFAULT true NOT NULL,
    implicit_flow_enabled boolean DEFAULT false NOT NULL,
    direct_access_grants_enabled boolean DEFAULT false NOT NULL,
    always_display_in_console boolean DEFAULT false NOT NULL
);


ALTER TABLE public.client OWNER TO keycloak;

--
-- Name: client_attributes; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.client_attributes (
    client_id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    value text
);


ALTER TABLE public.client_attributes OWNER TO keycloak;

--
-- Name: client_auth_flow_bindings; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.client_auth_flow_bindings (
    client_id character varying(36) NOT NULL,
    flow_id character varying(36),
    binding_name character varying(255) NOT NULL
);


ALTER TABLE public.client_auth_flow_bindings OWNER TO keycloak;

--
-- Name: client_initial_access; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.client_initial_access (
    id character varying(36) NOT NULL,
    realm_id character varying(36) NOT NULL,
    "timestamp" integer,
    expiration integer,
    count integer,
    remaining_count integer
);


ALTER TABLE public.client_initial_access OWNER TO keycloak;

--
-- Name: client_node_registrations; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.client_node_registrations (
    client_id character varying(36) NOT NULL,
    value integer,
    name character varying(255) NOT NULL
);


ALTER TABLE public.client_node_registrations OWNER TO keycloak;

--
-- Name: client_scope; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.client_scope (
    id character varying(36) NOT NULL,
    name character varying(255),
    realm_id character varying(36),
    description character varying(255),
    protocol character varying(255)
);


ALTER TABLE public.client_scope OWNER TO keycloak;

--
-- Name: client_scope_attributes; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.client_scope_attributes (
    scope_id character varying(36) NOT NULL,
    value character varying(2048),
    name character varying(255) NOT NULL
);


ALTER TABLE public.client_scope_attributes OWNER TO keycloak;

--
-- Name: client_scope_client; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.client_scope_client (
    client_id character varying(255) NOT NULL,
    scope_id character varying(255) NOT NULL,
    default_scope boolean DEFAULT false NOT NULL
);


ALTER TABLE public.client_scope_client OWNER TO keycloak;

--
-- Name: client_scope_role_mapping; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.client_scope_role_mapping (
    scope_id character varying(36) NOT NULL,
    role_id character varying(36) NOT NULL
);


ALTER TABLE public.client_scope_role_mapping OWNER TO keycloak;

--
-- Name: client_session; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.client_session (
    id character varying(36) NOT NULL,
    client_id character varying(36),
    redirect_uri character varying(255),
    state character varying(255),
    "timestamp" integer,
    session_id character varying(36),
    auth_method character varying(255),
    realm_id character varying(255),
    auth_user_id character varying(36),
    current_action character varying(36)
);


ALTER TABLE public.client_session OWNER TO keycloak;

--
-- Name: client_session_auth_status; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.client_session_auth_status (
    authenticator character varying(36) NOT NULL,
    status integer,
    client_session character varying(36) NOT NULL
);


ALTER TABLE public.client_session_auth_status OWNER TO keycloak;

--
-- Name: client_session_note; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.client_session_note (
    name character varying(255) NOT NULL,
    value character varying(255),
    client_session character varying(36) NOT NULL
);


ALTER TABLE public.client_session_note OWNER TO keycloak;

--
-- Name: client_session_prot_mapper; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.client_session_prot_mapper (
    protocol_mapper_id character varying(36) NOT NULL,
    client_session character varying(36) NOT NULL
);


ALTER TABLE public.client_session_prot_mapper OWNER TO keycloak;

--
-- Name: client_session_role; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.client_session_role (
    role_id character varying(255) NOT NULL,
    client_session character varying(36) NOT NULL
);


ALTER TABLE public.client_session_role OWNER TO keycloak;

--
-- Name: client_user_session_note; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.client_user_session_note (
    name character varying(255) NOT NULL,
    value character varying(2048),
    client_session character varying(36) NOT NULL
);


ALTER TABLE public.client_user_session_note OWNER TO keycloak;

--
-- Name: component; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.component (
    id character varying(36) NOT NULL,
    name character varying(255),
    parent_id character varying(36),
    provider_id character varying(36),
    provider_type character varying(255),
    realm_id character varying(36),
    sub_type character varying(255)
);


ALTER TABLE public.component OWNER TO keycloak;

--
-- Name: component_config; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.component_config (
    id character varying(36) NOT NULL,
    component_id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    value character varying(4000)
);


ALTER TABLE public.component_config OWNER TO keycloak;

--
-- Name: composite_role; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.composite_role (
    composite character varying(36) NOT NULL,
    child_role character varying(36) NOT NULL
);


ALTER TABLE public.composite_role OWNER TO keycloak;

--
-- Name: credential; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.credential (
    id character varying(36) NOT NULL,
    salt bytea,
    type character varying(255),
    user_id character varying(36),
    created_date bigint,
    user_label character varying(255),
    secret_data text,
    credential_data text,
    priority integer
);


ALTER TABLE public.credential OWNER TO keycloak;

--
-- Name: databasechangelog; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.databasechangelog (
    id character varying(255) NOT NULL,
    author character varying(255) NOT NULL,
    filename character varying(255) NOT NULL,
    dateexecuted timestamp without time zone NOT NULL,
    orderexecuted integer NOT NULL,
    exectype character varying(10) NOT NULL,
    md5sum character varying(35),
    description character varying(255),
    comments character varying(255),
    tag character varying(255),
    liquibase character varying(20),
    contexts character varying(255),
    labels character varying(255),
    deployment_id character varying(10)
);


ALTER TABLE public.databasechangelog OWNER TO keycloak;

--
-- Name: databasechangeloglock; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.databasechangeloglock (
    id integer NOT NULL,
    locked boolean NOT NULL,
    lockgranted timestamp without time zone,
    lockedby character varying(255)
);


ALTER TABLE public.databasechangeloglock OWNER TO keycloak;

--
-- Name: default_client_scope; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.default_client_scope (
    realm_id character varying(36) NOT NULL,
    scope_id character varying(36) NOT NULL,
    default_scope boolean DEFAULT false NOT NULL
);


ALTER TABLE public.default_client_scope OWNER TO keycloak;

--
-- Name: event_entity; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.event_entity (
    id character varying(36) NOT NULL,
    client_id character varying(255),
    details_json character varying(2550),
    error character varying(255),
    ip_address character varying(255),
    realm_id character varying(255),
    session_id character varying(255),
    event_time bigint,
    type character varying(255),
    user_id character varying(255)
);


ALTER TABLE public.event_entity OWNER TO keycloak;

--
-- Name: fed_user_attribute; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.fed_user_attribute (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    storage_provider_id character varying(36),
    value character varying(2024)
);


ALTER TABLE public.fed_user_attribute OWNER TO keycloak;

--
-- Name: fed_user_consent; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.fed_user_consent (
    id character varying(36) NOT NULL,
    client_id character varying(255),
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    storage_provider_id character varying(36),
    created_date bigint,
    last_updated_date bigint,
    client_storage_provider character varying(36),
    external_client_id character varying(255)
);


ALTER TABLE public.fed_user_consent OWNER TO keycloak;

--
-- Name: fed_user_consent_cl_scope; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.fed_user_consent_cl_scope (
    user_consent_id character varying(36) NOT NULL,
    scope_id character varying(36) NOT NULL
);


ALTER TABLE public.fed_user_consent_cl_scope OWNER TO keycloak;

--
-- Name: fed_user_credential; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.fed_user_credential (
    id character varying(36) NOT NULL,
    salt bytea,
    type character varying(255),
    created_date bigint,
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    storage_provider_id character varying(36),
    user_label character varying(255),
    secret_data text,
    credential_data text,
    priority integer
);


ALTER TABLE public.fed_user_credential OWNER TO keycloak;

--
-- Name: fed_user_group_membership; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.fed_user_group_membership (
    group_id character varying(36) NOT NULL,
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    storage_provider_id character varying(36)
);


ALTER TABLE public.fed_user_group_membership OWNER TO keycloak;

--
-- Name: fed_user_required_action; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.fed_user_required_action (
    required_action character varying(255) DEFAULT ' '::character varying NOT NULL,
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    storage_provider_id character varying(36)
);


ALTER TABLE public.fed_user_required_action OWNER TO keycloak;

--
-- Name: fed_user_role_mapping; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.fed_user_role_mapping (
    role_id character varying(36) NOT NULL,
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    storage_provider_id character varying(36)
);


ALTER TABLE public.fed_user_role_mapping OWNER TO keycloak;

--
-- Name: federated_identity; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.federated_identity (
    identity_provider character varying(255) NOT NULL,
    realm_id character varying(36),
    federated_user_id character varying(255),
    federated_username character varying(255),
    token text,
    user_id character varying(36) NOT NULL
);


ALTER TABLE public.federated_identity OWNER TO keycloak;

--
-- Name: federated_user; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.federated_user (
    id character varying(255) NOT NULL,
    storage_provider_id character varying(255),
    realm_id character varying(36) NOT NULL
);


ALTER TABLE public.federated_user OWNER TO keycloak;

--
-- Name: group_attribute; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.group_attribute (
    id character varying(36) DEFAULT 'sybase-needs-something-here'::character varying NOT NULL,
    name character varying(255) NOT NULL,
    value character varying(255),
    group_id character varying(36) NOT NULL
);


ALTER TABLE public.group_attribute OWNER TO keycloak;

--
-- Name: group_role_mapping; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.group_role_mapping (
    role_id character varying(36) NOT NULL,
    group_id character varying(36) NOT NULL
);


ALTER TABLE public.group_role_mapping OWNER TO keycloak;

--
-- Name: identity_provider; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.identity_provider (
    internal_id character varying(36) NOT NULL,
    enabled boolean DEFAULT false NOT NULL,
    provider_alias character varying(255),
    provider_id character varying(255),
    store_token boolean DEFAULT false NOT NULL,
    authenticate_by_default boolean DEFAULT false NOT NULL,
    realm_id character varying(36),
    add_token_role boolean DEFAULT true NOT NULL,
    trust_email boolean DEFAULT false NOT NULL,
    first_broker_login_flow_id character varying(36),
    post_broker_login_flow_id character varying(36),
    provider_display_name character varying(255),
    link_only boolean DEFAULT false NOT NULL
);


ALTER TABLE public.identity_provider OWNER TO keycloak;

--
-- Name: identity_provider_config; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.identity_provider_config (
    identity_provider_id character varying(36) NOT NULL,
    value text,
    name character varying(255) NOT NULL
);


ALTER TABLE public.identity_provider_config OWNER TO keycloak;

--
-- Name: identity_provider_mapper; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.identity_provider_mapper (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    idp_alias character varying(255) NOT NULL,
    idp_mapper_name character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL
);


ALTER TABLE public.identity_provider_mapper OWNER TO keycloak;

--
-- Name: idp_mapper_config; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.idp_mapper_config (
    idp_mapper_id character varying(36) NOT NULL,
    value text,
    name character varying(255) NOT NULL
);


ALTER TABLE public.idp_mapper_config OWNER TO keycloak;

--
-- Name: keycloak_group; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.keycloak_group (
    id character varying(36) NOT NULL,
    name character varying(255),
    parent_group character varying(36) NOT NULL,
    realm_id character varying(36)
);


ALTER TABLE public.keycloak_group OWNER TO keycloak;

--
-- Name: keycloak_role; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.keycloak_role (
    id character varying(36) NOT NULL,
    client_realm_constraint character varying(255),
    client_role boolean DEFAULT false NOT NULL,
    description character varying(255),
    name character varying(255),
    realm_id character varying(255),
    client character varying(36),
    realm character varying(36)
);


ALTER TABLE public.keycloak_role OWNER TO keycloak;

--
-- Name: migration_model; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.migration_model (
    id character varying(36) NOT NULL,
    version character varying(36),
    update_time bigint DEFAULT 0 NOT NULL
);


ALTER TABLE public.migration_model OWNER TO keycloak;

--
-- Name: offline_client_session; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.offline_client_session (
    user_session_id character varying(36) NOT NULL,
    client_id character varying(255) NOT NULL,
    offline_flag character varying(4) NOT NULL,
    "timestamp" integer,
    data text,
    client_storage_provider character varying(36) DEFAULT 'local'::character varying NOT NULL,
    external_client_id character varying(255) DEFAULT 'local'::character varying NOT NULL
);


ALTER TABLE public.offline_client_session OWNER TO keycloak;

--
-- Name: offline_user_session; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.offline_user_session (
    user_session_id character varying(36) NOT NULL,
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    created_on integer NOT NULL,
    offline_flag character varying(4) NOT NULL,
    data text,
    last_session_refresh integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.offline_user_session OWNER TO keycloak;

--
-- Name: policy_config; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.policy_config (
    policy_id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    value text
);


ALTER TABLE public.policy_config OWNER TO keycloak;

--
-- Name: protocol_mapper; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.protocol_mapper (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    protocol character varying(255) NOT NULL,
    protocol_mapper_name character varying(255) NOT NULL,
    client_id character varying(36),
    client_scope_id character varying(36)
);


ALTER TABLE public.protocol_mapper OWNER TO keycloak;

--
-- Name: protocol_mapper_config; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.protocol_mapper_config (
    protocol_mapper_id character varying(36) NOT NULL,
    value text,
    name character varying(255) NOT NULL
);


ALTER TABLE public.protocol_mapper_config OWNER TO keycloak;

--
-- Name: realm; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.realm (
    id character varying(36) NOT NULL,
    access_code_lifespan integer,
    user_action_lifespan integer,
    access_token_lifespan integer,
    account_theme character varying(255),
    admin_theme character varying(255),
    email_theme character varying(255),
    enabled boolean DEFAULT false NOT NULL,
    events_enabled boolean DEFAULT false NOT NULL,
    events_expiration bigint,
    login_theme character varying(255),
    name character varying(255),
    not_before integer,
    password_policy character varying(2550),
    registration_allowed boolean DEFAULT false NOT NULL,
    remember_me boolean DEFAULT false NOT NULL,
    reset_password_allowed boolean DEFAULT false NOT NULL,
    social boolean DEFAULT false NOT NULL,
    ssl_required character varying(255),
    sso_idle_timeout integer,
    sso_max_lifespan integer,
    update_profile_on_soc_login boolean DEFAULT false NOT NULL,
    verify_email boolean DEFAULT false NOT NULL,
    master_admin_client character varying(36),
    login_lifespan integer,
    internationalization_enabled boolean DEFAULT false NOT NULL,
    default_locale character varying(255),
    reg_email_as_username boolean DEFAULT false NOT NULL,
    admin_events_enabled boolean DEFAULT false NOT NULL,
    admin_events_details_enabled boolean DEFAULT false NOT NULL,
    edit_username_allowed boolean DEFAULT false NOT NULL,
    otp_policy_counter integer DEFAULT 0,
    otp_policy_window integer DEFAULT 1,
    otp_policy_period integer DEFAULT 30,
    otp_policy_digits integer DEFAULT 6,
    otp_policy_alg character varying(36) DEFAULT 'HmacSHA1'::character varying,
    otp_policy_type character varying(36) DEFAULT 'totp'::character varying,
    browser_flow character varying(36),
    registration_flow character varying(36),
    direct_grant_flow character varying(36),
    reset_credentials_flow character varying(36),
    client_auth_flow character varying(36),
    offline_session_idle_timeout integer DEFAULT 0,
    revoke_refresh_token boolean DEFAULT false NOT NULL,
    access_token_life_implicit integer DEFAULT 0,
    login_with_email_allowed boolean DEFAULT true NOT NULL,
    duplicate_emails_allowed boolean DEFAULT false NOT NULL,
    docker_auth_flow character varying(36),
    refresh_token_max_reuse integer DEFAULT 0,
    allow_user_managed_access boolean DEFAULT false NOT NULL,
    sso_max_lifespan_remember_me integer DEFAULT 0 NOT NULL,
    sso_idle_timeout_remember_me integer DEFAULT 0 NOT NULL,
    default_role character varying(255)
);


ALTER TABLE public.realm OWNER TO keycloak;

--
-- Name: realm_attribute; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.realm_attribute (
    name character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    value text
);


ALTER TABLE public.realm_attribute OWNER TO keycloak;

--
-- Name: realm_default_groups; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.realm_default_groups (
    realm_id character varying(36) NOT NULL,
    group_id character varying(36) NOT NULL
);


ALTER TABLE public.realm_default_groups OWNER TO keycloak;

--
-- Name: realm_enabled_event_types; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.realm_enabled_event_types (
    realm_id character varying(36) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE public.realm_enabled_event_types OWNER TO keycloak;

--
-- Name: realm_events_listeners; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.realm_events_listeners (
    realm_id character varying(36) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE public.realm_events_listeners OWNER TO keycloak;

--
-- Name: realm_localizations; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.realm_localizations (
    realm_id character varying(255) NOT NULL,
    locale character varying(255) NOT NULL,
    texts text NOT NULL
);


ALTER TABLE public.realm_localizations OWNER TO keycloak;

--
-- Name: realm_required_credential; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.realm_required_credential (
    type character varying(255) NOT NULL,
    form_label character varying(255),
    input boolean DEFAULT false NOT NULL,
    secret boolean DEFAULT false NOT NULL,
    realm_id character varying(36) NOT NULL
);


ALTER TABLE public.realm_required_credential OWNER TO keycloak;

--
-- Name: realm_smtp_config; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.realm_smtp_config (
    realm_id character varying(36) NOT NULL,
    value character varying(255),
    name character varying(255) NOT NULL
);


ALTER TABLE public.realm_smtp_config OWNER TO keycloak;

--
-- Name: realm_supported_locales; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.realm_supported_locales (
    realm_id character varying(36) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE public.realm_supported_locales OWNER TO keycloak;

--
-- Name: redirect_uris; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.redirect_uris (
    client_id character varying(36) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE public.redirect_uris OWNER TO keycloak;

--
-- Name: required_action_config; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.required_action_config (
    required_action_id character varying(36) NOT NULL,
    value text,
    name character varying(255) NOT NULL
);


ALTER TABLE public.required_action_config OWNER TO keycloak;

--
-- Name: required_action_provider; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.required_action_provider (
    id character varying(36) NOT NULL,
    alias character varying(255),
    name character varying(255),
    realm_id character varying(36),
    enabled boolean DEFAULT false NOT NULL,
    default_action boolean DEFAULT false NOT NULL,
    provider_id character varying(255),
    priority integer
);


ALTER TABLE public.required_action_provider OWNER TO keycloak;

--
-- Name: resource_attribute; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.resource_attribute (
    id character varying(36) DEFAULT 'sybase-needs-something-here'::character varying NOT NULL,
    name character varying(255) NOT NULL,
    value character varying(255),
    resource_id character varying(36) NOT NULL
);


ALTER TABLE public.resource_attribute OWNER TO keycloak;

--
-- Name: resource_policy; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.resource_policy (
    resource_id character varying(36) NOT NULL,
    policy_id character varying(36) NOT NULL
);


ALTER TABLE public.resource_policy OWNER TO keycloak;

--
-- Name: resource_scope; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.resource_scope (
    resource_id character varying(36) NOT NULL,
    scope_id character varying(36) NOT NULL
);


ALTER TABLE public.resource_scope OWNER TO keycloak;

--
-- Name: resource_server; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.resource_server (
    id character varying(36) NOT NULL,
    allow_rs_remote_mgmt boolean DEFAULT false NOT NULL,
    policy_enforce_mode smallint NOT NULL,
    decision_strategy smallint DEFAULT 1 NOT NULL
);


ALTER TABLE public.resource_server OWNER TO keycloak;

--
-- Name: resource_server_perm_ticket; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.resource_server_perm_ticket (
    id character varying(36) NOT NULL,
    owner character varying(255) NOT NULL,
    requester character varying(255) NOT NULL,
    created_timestamp bigint NOT NULL,
    granted_timestamp bigint,
    resource_id character varying(36) NOT NULL,
    scope_id character varying(36),
    resource_server_id character varying(36) NOT NULL,
    policy_id character varying(36)
);


ALTER TABLE public.resource_server_perm_ticket OWNER TO keycloak;

--
-- Name: resource_server_policy; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.resource_server_policy (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(255),
    type character varying(255) NOT NULL,
    decision_strategy smallint,
    logic smallint,
    resource_server_id character varying(36) NOT NULL,
    owner character varying(255)
);


ALTER TABLE public.resource_server_policy OWNER TO keycloak;

--
-- Name: resource_server_resource; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.resource_server_resource (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    type character varying(255),
    icon_uri character varying(255),
    owner character varying(255) NOT NULL,
    resource_server_id character varying(36) NOT NULL,
    owner_managed_access boolean DEFAULT false NOT NULL,
    display_name character varying(255)
);


ALTER TABLE public.resource_server_resource OWNER TO keycloak;

--
-- Name: resource_server_scope; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.resource_server_scope (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    icon_uri character varying(255),
    resource_server_id character varying(36) NOT NULL,
    display_name character varying(255)
);


ALTER TABLE public.resource_server_scope OWNER TO keycloak;

--
-- Name: resource_uris; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.resource_uris (
    resource_id character varying(36) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE public.resource_uris OWNER TO keycloak;

--
-- Name: role_attribute; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.role_attribute (
    id character varying(36) NOT NULL,
    role_id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    value character varying(255)
);


ALTER TABLE public.role_attribute OWNER TO keycloak;

--
-- Name: scope_mapping; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.scope_mapping (
    client_id character varying(36) NOT NULL,
    role_id character varying(36) NOT NULL
);


ALTER TABLE public.scope_mapping OWNER TO keycloak;

--
-- Name: scope_policy; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.scope_policy (
    scope_id character varying(36) NOT NULL,
    policy_id character varying(36) NOT NULL
);


ALTER TABLE public.scope_policy OWNER TO keycloak;

--
-- Name: user_attribute; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.user_attribute (
    name character varying(255) NOT NULL,
    value character varying(255),
    user_id character varying(36) NOT NULL,
    id character varying(36) DEFAULT 'sybase-needs-something-here'::character varying NOT NULL
);


ALTER TABLE public.user_attribute OWNER TO keycloak;

--
-- Name: user_consent; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.user_consent (
    id character varying(36) NOT NULL,
    client_id character varying(255),
    user_id character varying(36) NOT NULL,
    created_date bigint,
    last_updated_date bigint,
    client_storage_provider character varying(36),
    external_client_id character varying(255)
);


ALTER TABLE public.user_consent OWNER TO keycloak;

--
-- Name: user_consent_client_scope; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.user_consent_client_scope (
    user_consent_id character varying(36) NOT NULL,
    scope_id character varying(36) NOT NULL
);


ALTER TABLE public.user_consent_client_scope OWNER TO keycloak;

--
-- Name: user_entity; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.user_entity (
    id character varying(36) NOT NULL,
    email character varying(255),
    email_constraint character varying(255),
    email_verified boolean DEFAULT false NOT NULL,
    enabled boolean DEFAULT false NOT NULL,
    federation_link character varying(255),
    first_name character varying(255),
    last_name character varying(255),
    realm_id character varying(255),
    username character varying(255),
    created_timestamp bigint,
    service_account_client_link character varying(255),
    not_before integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.user_entity OWNER TO keycloak;

--
-- Name: user_federation_config; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.user_federation_config (
    user_federation_provider_id character varying(36) NOT NULL,
    value character varying(255),
    name character varying(255) NOT NULL
);


ALTER TABLE public.user_federation_config OWNER TO keycloak;

--
-- Name: user_federation_mapper; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.user_federation_mapper (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    federation_provider_id character varying(36) NOT NULL,
    federation_mapper_type character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL
);


ALTER TABLE public.user_federation_mapper OWNER TO keycloak;

--
-- Name: user_federation_mapper_config; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.user_federation_mapper_config (
    user_federation_mapper_id character varying(36) NOT NULL,
    value character varying(255),
    name character varying(255) NOT NULL
);


ALTER TABLE public.user_federation_mapper_config OWNER TO keycloak;

--
-- Name: user_federation_provider; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.user_federation_provider (
    id character varying(36) NOT NULL,
    changed_sync_period integer,
    display_name character varying(255),
    full_sync_period integer,
    last_sync integer,
    priority integer,
    provider_name character varying(255),
    realm_id character varying(36)
);


ALTER TABLE public.user_federation_provider OWNER TO keycloak;

--
-- Name: user_group_membership; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.user_group_membership (
    group_id character varying(36) NOT NULL,
    user_id character varying(36) NOT NULL
);


ALTER TABLE public.user_group_membership OWNER TO keycloak;

--
-- Name: user_required_action; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.user_required_action (
    user_id character varying(36) NOT NULL,
    required_action character varying(255) DEFAULT ' '::character varying NOT NULL
);


ALTER TABLE public.user_required_action OWNER TO keycloak;

--
-- Name: user_role_mapping; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.user_role_mapping (
    role_id character varying(255) NOT NULL,
    user_id character varying(36) NOT NULL
);


ALTER TABLE public.user_role_mapping OWNER TO keycloak;

--
-- Name: user_session; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.user_session (
    id character varying(36) NOT NULL,
    auth_method character varying(255),
    ip_address character varying(255),
    last_session_refresh integer,
    login_username character varying(255),
    realm_id character varying(255),
    remember_me boolean DEFAULT false NOT NULL,
    started integer,
    user_id character varying(255),
    user_session_state integer,
    broker_session_id character varying(255),
    broker_user_id character varying(255)
);


ALTER TABLE public.user_session OWNER TO keycloak;

--
-- Name: user_session_note; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.user_session_note (
    user_session character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    value character varying(2048)
);


ALTER TABLE public.user_session_note OWNER TO keycloak;

--
-- Name: username_login_failure; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.username_login_failure (
    realm_id character varying(36) NOT NULL,
    username character varying(255) NOT NULL,
    failed_login_not_before integer,
    last_failure bigint,
    last_ip_failure character varying(255),
    num_failures integer
);


ALTER TABLE public.username_login_failure OWNER TO keycloak;

--
-- Name: web_origins; Type: TABLE; Schema: public; Owner: keycloak
--

CREATE TABLE public.web_origins (
    client_id character varying(36) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE public.web_origins OWNER TO keycloak;

--
-- Data for Name: admin_event_entity; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.admin_event_entity (id, admin_event_time, realm_id, operation_type, auth_realm_id, auth_client_id, auth_user_id, ip_address, resource_path, representation, error, resource_type) FROM stdin;
\.


--
-- Data for Name: associated_policy; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.associated_policy (policy_id, associated_policy_id) FROM stdin;
\.


--
-- Data for Name: authentication_execution; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.authentication_execution (id, alias, authenticator, realm_id, flow_id, requirement, priority, authenticator_flow, auth_flow_id, auth_config) FROM stdin;
370e4909-933a-441b-9d99-dfbf43b2a7b1	\N	auth-cookie	19e687a7-a4bd-4974-9623-d44f5d246eb1	b8e5713a-a3da-4ecd-9dd8-e567ce0b3a5a	2	10	f	\N	\N
a7ca6e8e-7be5-443d-98ef-4c08f4fb8192	\N	auth-spnego	19e687a7-a4bd-4974-9623-d44f5d246eb1	b8e5713a-a3da-4ecd-9dd8-e567ce0b3a5a	3	20	f	\N	\N
fb5e0438-e7c0-4a74-a00e-fdb0dba4f2c0	\N	identity-provider-redirector	19e687a7-a4bd-4974-9623-d44f5d246eb1	b8e5713a-a3da-4ecd-9dd8-e567ce0b3a5a	2	25	f	\N	\N
a2dccf25-3aff-4aac-8a8a-63ec5fdad51b	\N	\N	19e687a7-a4bd-4974-9623-d44f5d246eb1	b8e5713a-a3da-4ecd-9dd8-e567ce0b3a5a	2	30	t	683c5b94-8edb-41b9-86ed-776cb5added7	\N
bdfa9cda-10b1-4a05-be3c-f04834aa2ee7	\N	auth-username-password-form	19e687a7-a4bd-4974-9623-d44f5d246eb1	683c5b94-8edb-41b9-86ed-776cb5added7	0	10	f	\N	\N
3babf698-f63f-4412-afd2-8048f6f43a11	\N	\N	19e687a7-a4bd-4974-9623-d44f5d246eb1	683c5b94-8edb-41b9-86ed-776cb5added7	1	20	t	a15ba0ac-41b4-4fab-b912-a15e165da803	\N
208682d0-28d0-44f3-bba3-c20f6ea6ef59	\N	conditional-user-configured	19e687a7-a4bd-4974-9623-d44f5d246eb1	a15ba0ac-41b4-4fab-b912-a15e165da803	0	10	f	\N	\N
df8d18ec-d316-4076-a037-2c8cc7586027	\N	auth-otp-form	19e687a7-a4bd-4974-9623-d44f5d246eb1	a15ba0ac-41b4-4fab-b912-a15e165da803	0	20	f	\N	\N
63155176-878d-4e27-9c50-ecead2726520	\N	direct-grant-validate-username	19e687a7-a4bd-4974-9623-d44f5d246eb1	2d037f8c-ea48-4173-80f6-c67cd33788a9	0	10	f	\N	\N
8da024b3-5b61-44e9-90ad-320bc3da0316	\N	direct-grant-validate-password	19e687a7-a4bd-4974-9623-d44f5d246eb1	2d037f8c-ea48-4173-80f6-c67cd33788a9	0	20	f	\N	\N
593e3779-ea5b-4c05-aeda-671020b034df	\N	\N	19e687a7-a4bd-4974-9623-d44f5d246eb1	2d037f8c-ea48-4173-80f6-c67cd33788a9	1	30	t	aff8d840-cad0-4621-aee5-2f0a6d3530c5	\N
cb62fd84-4139-46cf-9ca1-921ad6f548d0	\N	conditional-user-configured	19e687a7-a4bd-4974-9623-d44f5d246eb1	aff8d840-cad0-4621-aee5-2f0a6d3530c5	0	10	f	\N	\N
3d56cbe3-e62e-4f11-8f6a-c88662341f2f	\N	direct-grant-validate-otp	19e687a7-a4bd-4974-9623-d44f5d246eb1	aff8d840-cad0-4621-aee5-2f0a6d3530c5	0	20	f	\N	\N
37349657-9c28-4273-b026-fa9cf0bc697f	\N	registration-page-form	19e687a7-a4bd-4974-9623-d44f5d246eb1	f5b0f0fc-55cb-4a45-997e-fa1c0c8b452a	0	10	t	d432ce1c-9e2b-4829-a0d3-cddc0238dedd	\N
c075073b-2add-4268-a963-544aa4c7375d	\N	registration-user-creation	19e687a7-a4bd-4974-9623-d44f5d246eb1	d432ce1c-9e2b-4829-a0d3-cddc0238dedd	0	20	f	\N	\N
f4c60589-152e-4ee0-8f7a-3e198bea0694	\N	registration-profile-action	19e687a7-a4bd-4974-9623-d44f5d246eb1	d432ce1c-9e2b-4829-a0d3-cddc0238dedd	0	40	f	\N	\N
fe35a2a9-5edf-4580-957a-4678dbd84579	\N	registration-password-action	19e687a7-a4bd-4974-9623-d44f5d246eb1	d432ce1c-9e2b-4829-a0d3-cddc0238dedd	0	50	f	\N	\N
95c5e9f7-bef4-480e-954b-f127c16ebfa4	\N	registration-recaptcha-action	19e687a7-a4bd-4974-9623-d44f5d246eb1	d432ce1c-9e2b-4829-a0d3-cddc0238dedd	3	60	f	\N	\N
a47eae09-895b-40ed-8083-12e178242c35	\N	registration-terms-and-conditions	19e687a7-a4bd-4974-9623-d44f5d246eb1	d432ce1c-9e2b-4829-a0d3-cddc0238dedd	3	70	f	\N	\N
eb8299a1-5229-4c63-9b23-73030106a41d	\N	reset-credentials-choose-user	19e687a7-a4bd-4974-9623-d44f5d246eb1	f36063ba-cae3-4ba6-b537-e7820d44088d	0	10	f	\N	\N
8137b0e8-1e03-47bf-96ed-3e0f40ebbc9d	\N	reset-credential-email	19e687a7-a4bd-4974-9623-d44f5d246eb1	f36063ba-cae3-4ba6-b537-e7820d44088d	0	20	f	\N	\N
4b2ab21d-2af6-43e7-b727-8cb44e2e6b93	\N	reset-password	19e687a7-a4bd-4974-9623-d44f5d246eb1	f36063ba-cae3-4ba6-b537-e7820d44088d	0	30	f	\N	\N
f2ee0c9f-349e-4a8e-bdbf-13cc13a3e29d	\N	\N	19e687a7-a4bd-4974-9623-d44f5d246eb1	f36063ba-cae3-4ba6-b537-e7820d44088d	1	40	t	6c4c5592-2bc8-43bd-8c25-80a1f2af3e37	\N
0974f927-0647-4717-8376-1566c24a8f9a	\N	conditional-user-configured	19e687a7-a4bd-4974-9623-d44f5d246eb1	6c4c5592-2bc8-43bd-8c25-80a1f2af3e37	0	10	f	\N	\N
35fc7f3f-430b-4e77-a322-6043a464a90e	\N	reset-otp	19e687a7-a4bd-4974-9623-d44f5d246eb1	6c4c5592-2bc8-43bd-8c25-80a1f2af3e37	0	20	f	\N	\N
544235f6-659d-47f7-a258-04fc3cf36d31	\N	client-secret	19e687a7-a4bd-4974-9623-d44f5d246eb1	f3abbf9a-321e-43b6-add0-f4cc33fc1a68	2	10	f	\N	\N
2ab3f85e-a872-479d-8652-dfd7fa38f9a3	\N	client-jwt	19e687a7-a4bd-4974-9623-d44f5d246eb1	f3abbf9a-321e-43b6-add0-f4cc33fc1a68	2	20	f	\N	\N
f37223cb-49ed-4968-b4c3-3308a469c26a	\N	client-secret-jwt	19e687a7-a4bd-4974-9623-d44f5d246eb1	f3abbf9a-321e-43b6-add0-f4cc33fc1a68	2	30	f	\N	\N
34fc85df-b48b-4288-9054-365f2e059ce9	\N	client-x509	19e687a7-a4bd-4974-9623-d44f5d246eb1	f3abbf9a-321e-43b6-add0-f4cc33fc1a68	2	40	f	\N	\N
e9751d42-a7d0-412c-acc8-dee678a5c52a	\N	idp-review-profile	19e687a7-a4bd-4974-9623-d44f5d246eb1	def07d2e-2c0f-4501-a898-16a21c130bfc	0	10	f	\N	c8bfcc97-0696-4d36-b0e7-435eec5e467f
f4626b81-cdfa-48e6-83ce-1a560dff360c	\N	\N	19e687a7-a4bd-4974-9623-d44f5d246eb1	def07d2e-2c0f-4501-a898-16a21c130bfc	0	20	t	94d62eeb-9695-4187-89a8-d798627e7721	\N
a22ca9c6-d5b2-432a-bcc3-c52a680b1f0b	\N	idp-create-user-if-unique	19e687a7-a4bd-4974-9623-d44f5d246eb1	94d62eeb-9695-4187-89a8-d798627e7721	2	10	f	\N	69fd2fb8-ade4-4cab-b962-d80f13103b11
07f64a90-8ba2-4525-a45d-68fb0e8cf795	\N	\N	19e687a7-a4bd-4974-9623-d44f5d246eb1	94d62eeb-9695-4187-89a8-d798627e7721	2	20	t	19e84e48-68cb-4e47-9538-b6bc5f3d646b	\N
2cb7d035-fc60-4660-b3bf-b8d9c2167232	\N	idp-confirm-link	19e687a7-a4bd-4974-9623-d44f5d246eb1	19e84e48-68cb-4e47-9538-b6bc5f3d646b	0	10	f	\N	\N
106615e5-2114-4213-9f3c-d02eb91d32c9	\N	\N	19e687a7-a4bd-4974-9623-d44f5d246eb1	19e84e48-68cb-4e47-9538-b6bc5f3d646b	0	20	t	e72a92f5-2f38-4c64-9330-b8d91954f261	\N
f92dc45a-3ac3-4db6-bbfe-6d4dd856d66f	\N	idp-email-verification	19e687a7-a4bd-4974-9623-d44f5d246eb1	e72a92f5-2f38-4c64-9330-b8d91954f261	2	10	f	\N	\N
3c6ae071-828a-45b3-930e-5ac48eab766d	\N	\N	19e687a7-a4bd-4974-9623-d44f5d246eb1	e72a92f5-2f38-4c64-9330-b8d91954f261	2	20	t	e294fcbd-90db-41bc-8b7b-9e5c2b70d280	\N
b2d79789-f9eb-407d-9a6a-51ab12f4735c	\N	idp-username-password-form	19e687a7-a4bd-4974-9623-d44f5d246eb1	e294fcbd-90db-41bc-8b7b-9e5c2b70d280	0	10	f	\N	\N
55bf644a-c8c8-464b-97ce-baee05c74af8	\N	\N	19e687a7-a4bd-4974-9623-d44f5d246eb1	e294fcbd-90db-41bc-8b7b-9e5c2b70d280	1	20	t	b903bb9f-a3f4-402f-9814-6e7719de7b17	\N
dd3aa900-0fd0-4163-9e75-0c3d1d5781f0	\N	conditional-user-configured	19e687a7-a4bd-4974-9623-d44f5d246eb1	b903bb9f-a3f4-402f-9814-6e7719de7b17	0	10	f	\N	\N
36473a58-188d-49c6-b879-dd30b25726f4	\N	auth-otp-form	19e687a7-a4bd-4974-9623-d44f5d246eb1	b903bb9f-a3f4-402f-9814-6e7719de7b17	0	20	f	\N	\N
30ae6293-23f4-4b95-bd96-bb5d7749a3d8	\N	http-basic-authenticator	19e687a7-a4bd-4974-9623-d44f5d246eb1	40e5e5b6-6b8f-4ad9-b9fa-8ab4303dcea3	0	10	f	\N	\N
40ccddf9-3ea9-45f7-98f8-dd305876235c	\N	docker-http-basic-authenticator	19e687a7-a4bd-4974-9623-d44f5d246eb1	b9efbeb9-9d2c-4f7e-90c2-91c34d2264ce	0	10	f	\N	\N
891902a5-1866-4c83-ba15-e623d5fc65a4	\N	idp-email-verification	8211ffad-2ece-45e2-be4d-9509ea27c3d5	37df5f04-1368-4a7d-98c6-2867510ef82f	2	10	f	\N	\N
b83cbfb1-4f2d-44a6-b143-8c0126860cbf	\N	\N	8211ffad-2ece-45e2-be4d-9509ea27c3d5	37df5f04-1368-4a7d-98c6-2867510ef82f	2	20	t	aba7d391-0054-4c5b-880b-a940907d18e2	\N
57775fc4-942f-435f-a735-83260604a15a	\N	conditional-user-configured	8211ffad-2ece-45e2-be4d-9509ea27c3d5	2df329ab-e393-4af6-bf18-5e44f2990876	0	10	f	\N	\N
ed87145d-f661-4b75-b9bf-cc27bc820b67	\N	auth-otp-form	8211ffad-2ece-45e2-be4d-9509ea27c3d5	2df329ab-e393-4af6-bf18-5e44f2990876	0	20	f	\N	\N
8ce48c94-ac07-478b-9ad9-45ed68744905	\N	conditional-user-configured	8211ffad-2ece-45e2-be4d-9509ea27c3d5	e89866fb-b02b-485d-911a-2796bff325ea	0	10	f	\N	\N
3071c95a-4e44-4a87-bb40-844624d56327	\N	direct-grant-validate-otp	8211ffad-2ece-45e2-be4d-9509ea27c3d5	e89866fb-b02b-485d-911a-2796bff325ea	0	20	f	\N	\N
0a998130-1ec4-4459-b2ec-572309b4ebd1	\N	conditional-user-configured	8211ffad-2ece-45e2-be4d-9509ea27c3d5	9365d3bc-241a-4c3c-a95f-6edda7e04e6d	0	10	f	\N	\N
51eb58e0-c421-43b1-8bd4-52b83c90e9de	\N	auth-otp-form	8211ffad-2ece-45e2-be4d-9509ea27c3d5	9365d3bc-241a-4c3c-a95f-6edda7e04e6d	0	20	f	\N	\N
3352b5e0-be65-4044-a0d7-ef2503679e29	\N	idp-confirm-link	8211ffad-2ece-45e2-be4d-9509ea27c3d5	9a618e72-496f-46ad-a48b-dc522efb304d	0	10	f	\N	\N
ea74a56e-8ce9-4ba8-b9bd-797b7064f907	\N	\N	8211ffad-2ece-45e2-be4d-9509ea27c3d5	9a618e72-496f-46ad-a48b-dc522efb304d	0	20	t	37df5f04-1368-4a7d-98c6-2867510ef82f	\N
53a3163d-34a6-4c23-9edc-7618f6e1011b	\N	conditional-user-configured	8211ffad-2ece-45e2-be4d-9509ea27c3d5	7ed4e25c-04a7-4880-bcc0-0c36c0c3248e	0	10	f	\N	\N
f033d26b-512b-40e9-b7cf-d54257f12705	\N	reset-otp	8211ffad-2ece-45e2-be4d-9509ea27c3d5	7ed4e25c-04a7-4880-bcc0-0c36c0c3248e	0	20	f	\N	\N
993966a6-bfb1-44e5-9c67-2c588bf56926	\N	conditional-user-configured	8211ffad-2ece-45e2-be4d-9509ea27c3d5	241c9166-fb28-4572-b932-359af0e65354	0	0	f	\N	\N
86378f15-9ad2-4db5-bae3-dba2c248ff21	\N	auth-otp-form	8211ffad-2ece-45e2-be4d-9509ea27c3d5	241c9166-fb28-4572-b932-359af0e65354	2	1	f	\N	\N
c199f9d5-6066-4daf-8136-d1d4ee63e004	\N	webauthn-authenticator	8211ffad-2ece-45e2-be4d-9509ea27c3d5	241c9166-fb28-4572-b932-359af0e65354	2	2	f	\N	\N
b436992b-dd73-46c9-8729-30bb748fa89c	\N	auth-username-password-form	8211ffad-2ece-45e2-be4d-9509ea27c3d5	2ad81ac6-e391-4d7f-91cd-9cd59be62c80	0	0	f	\N	\N
bb527f99-666d-4a65-8cf4-85206831a09a	\N	\N	8211ffad-2ece-45e2-be4d-9509ea27c3d5	2ad81ac6-e391-4d7f-91cd-9cd59be62c80	1	1	t	241c9166-fb28-4572-b932-359af0e65354	\N
c95b6796-fd8f-4298-9a7e-64f302968f33	\N	idp-create-user-if-unique	8211ffad-2ece-45e2-be4d-9509ea27c3d5	10f74bc8-fca4-476c-96ef-9dd849410b88	2	10	f	\N	3ebd377e-1f7b-4d43-86bf-0d2a2d77f70c
ae719c1c-dd77-4ee8-8e6c-757de1767c36	\N	\N	8211ffad-2ece-45e2-be4d-9509ea27c3d5	10f74bc8-fca4-476c-96ef-9dd849410b88	2	20	t	9a618e72-496f-46ad-a48b-dc522efb304d	\N
e0b1827c-37d8-4bf7-b384-f36af75aa18c	\N	idp-username-password-form	8211ffad-2ece-45e2-be4d-9509ea27c3d5	aba7d391-0054-4c5b-880b-a940907d18e2	0	10	f	\N	\N
7507dfe1-7f31-4769-82b0-0510762ffd92	\N	\N	8211ffad-2ece-45e2-be4d-9509ea27c3d5	aba7d391-0054-4c5b-880b-a940907d18e2	1	20	t	9365d3bc-241a-4c3c-a95f-6edda7e04e6d	\N
e925c8ee-9e2e-4407-bfe5-2b86821f45bd	\N	auth-cookie	8211ffad-2ece-45e2-be4d-9509ea27c3d5	b705ebeb-91ec-42b4-8072-c33b24ae8868	2	10	f	\N	\N
6777f30d-8ef1-445e-a887-4e0b9833edb7	\N	auth-spnego	8211ffad-2ece-45e2-be4d-9509ea27c3d5	b705ebeb-91ec-42b4-8072-c33b24ae8868	3	20	f	\N	\N
fb6c85f0-58f1-4065-bff0-6a4fdafcfe46	\N	identity-provider-redirector	8211ffad-2ece-45e2-be4d-9509ea27c3d5	b705ebeb-91ec-42b4-8072-c33b24ae8868	2	25	f	\N	\N
4815d9f4-4fd9-49ae-9a00-928047beeeab	\N	\N	8211ffad-2ece-45e2-be4d-9509ea27c3d5	b705ebeb-91ec-42b4-8072-c33b24ae8868	2	30	t	0ea9bae7-b4e7-44b2-8ea7-c2ae3b32d09a	\N
6466cd43-de5c-43a6-908c-ace71be75d09	\N	webauthn-authenticator-passwordless	8211ffad-2ece-45e2-be4d-9509ea27c3d5	0ea9bae7-b4e7-44b2-8ea7-c2ae3b32d09a	2	10	f	\N	\N
636889ae-c0d9-486b-a864-eb3dd1eef053	\N	\N	8211ffad-2ece-45e2-be4d-9509ea27c3d5	0ea9bae7-b4e7-44b2-8ea7-c2ae3b32d09a	2	14	t	2ad81ac6-e391-4d7f-91cd-9cd59be62c80	\N
61a22c47-4cbb-4325-9afc-d93532566804	\N	auth-cookie	8211ffad-2ece-45e2-be4d-9509ea27c3d5	17f33049-2463-4ed2-ae92-a6fbaabacd5e	2	10	f	\N	\N
b40d6d0f-95f5-430d-a039-0f11ed740e05	\N	auth-spnego	8211ffad-2ece-45e2-be4d-9509ea27c3d5	17f33049-2463-4ed2-ae92-a6fbaabacd5e	3	20	f	\N	\N
c014080b-0fad-4ed2-92ba-7054fd508812	\N	identity-provider-redirector	8211ffad-2ece-45e2-be4d-9509ea27c3d5	17f33049-2463-4ed2-ae92-a6fbaabacd5e	2	25	f	\N	\N
f0c41235-db56-40d9-a32f-06b6ab130f7b	\N	\N	8211ffad-2ece-45e2-be4d-9509ea27c3d5	17f33049-2463-4ed2-ae92-a6fbaabacd5e	2	30	t	c59d455a-b2d1-441e-9198-2666b7c7af3e	\N
b24fb7f6-f368-4ed7-bcbd-3e659fa7bf44	\N	client-secret	8211ffad-2ece-45e2-be4d-9509ea27c3d5	57fc7d69-26a6-4815-95ef-84c4bd3b1f64	2	10	f	\N	\N
636aa012-4e17-4921-b43b-2ad21039982b	\N	client-jwt	8211ffad-2ece-45e2-be4d-9509ea27c3d5	57fc7d69-26a6-4815-95ef-84c4bd3b1f64	2	20	f	\N	\N
80359975-1cba-4de4-b72c-4f27ed0e9114	\N	client-secret-jwt	8211ffad-2ece-45e2-be4d-9509ea27c3d5	57fc7d69-26a6-4815-95ef-84c4bd3b1f64	2	30	f	\N	\N
dc4e7e14-b1e9-43d1-bc89-9a15cab27280	\N	client-x509	8211ffad-2ece-45e2-be4d-9509ea27c3d5	57fc7d69-26a6-4815-95ef-84c4bd3b1f64	2	40	f	\N	\N
a5ea6fcc-1379-4954-b4ad-01e3a3860e57	\N	direct-grant-validate-username	8211ffad-2ece-45e2-be4d-9509ea27c3d5	787d2a18-62ab-4853-a171-e75548103b79	0	10	f	\N	\N
9b2d8977-4dd6-4cbf-8d0b-4f0ca82c1114	\N	direct-grant-validate-password	8211ffad-2ece-45e2-be4d-9509ea27c3d5	787d2a18-62ab-4853-a171-e75548103b79	0	20	f	\N	\N
efdc83d3-c315-4b6c-b5a5-d2caf0e9d94f	\N	\N	8211ffad-2ece-45e2-be4d-9509ea27c3d5	787d2a18-62ab-4853-a171-e75548103b79	1	30	t	e89866fb-b02b-485d-911a-2796bff325ea	\N
cf66eef6-ad88-4bd8-9189-6cf4f04e2540	\N	docker-http-basic-authenticator	8211ffad-2ece-45e2-be4d-9509ea27c3d5	91484abe-cb9b-48e8-b783-159fb070059d	0	10	f	\N	\N
ea321602-eb66-4c84-93a4-5ed1f9dca03d	\N	idp-review-profile	8211ffad-2ece-45e2-be4d-9509ea27c3d5	411f7ab6-eaac-468f-8140-0af1d9ca1eb4	0	10	f	\N	d39a28af-b5f5-4f1e-9b7e-9b7d8770987d
35bb131f-bbd5-468d-8e31-ba5227a49013	\N	\N	8211ffad-2ece-45e2-be4d-9509ea27c3d5	411f7ab6-eaac-468f-8140-0af1d9ca1eb4	0	20	t	10f74bc8-fca4-476c-96ef-9dd849410b88	\N
206187f3-26c4-4ee3-9905-60395690e4fd	\N	auth-username-password-form	8211ffad-2ece-45e2-be4d-9509ea27c3d5	c59d455a-b2d1-441e-9198-2666b7c7af3e	0	10	f	\N	\N
b3ff7640-cd9e-4993-be6f-83d4ef007c2d	\N	\N	8211ffad-2ece-45e2-be4d-9509ea27c3d5	c59d455a-b2d1-441e-9198-2666b7c7af3e	1	20	t	2df329ab-e393-4af6-bf18-5e44f2990876	\N
0d389534-9371-4fac-bcb5-e286a1825ba5	\N	registration-page-form	8211ffad-2ece-45e2-be4d-9509ea27c3d5	83645a93-dfe1-496e-ab3c-387221c38e94	0	10	t	fb1731aa-b333-4f83-be69-eab51972eba2	\N
a773c898-d571-423d-ae4a-182457da741e	\N	registration-user-creation	8211ffad-2ece-45e2-be4d-9509ea27c3d5	fb1731aa-b333-4f83-be69-eab51972eba2	0	20	f	\N	\N
8c03f4f9-7fd7-4627-8cef-16dce90a49e7	\N	registration-profile-action	8211ffad-2ece-45e2-be4d-9509ea27c3d5	fb1731aa-b333-4f83-be69-eab51972eba2	3	40	f	\N	\N
37c4417a-2ae8-4dd5-908e-5ed3754a0e63	\N	registration-password-action	8211ffad-2ece-45e2-be4d-9509ea27c3d5	fb1731aa-b333-4f83-be69-eab51972eba2	0	50	f	\N	\N
06575468-aaca-4ee0-8cda-48ebc6ffd4ba	\N	registration-recaptcha-action	8211ffad-2ece-45e2-be4d-9509ea27c3d5	fb1731aa-b333-4f83-be69-eab51972eba2	3	60	f	\N	\N
0abd654f-e7c4-405d-a68a-f3c914fcbdf1	\N	reset-credentials-choose-user	8211ffad-2ece-45e2-be4d-9509ea27c3d5	42da4271-4d88-4aea-9a8d-f5bca266788e	0	10	f	\N	\N
9a7d5835-10d6-4fc3-8271-570efe10f3a3	\N	reset-credential-email	8211ffad-2ece-45e2-be4d-9509ea27c3d5	42da4271-4d88-4aea-9a8d-f5bca266788e	0	20	f	\N	\N
4fa87a47-689d-455b-a6b9-a7b831c0338b	\N	reset-password	8211ffad-2ece-45e2-be4d-9509ea27c3d5	42da4271-4d88-4aea-9a8d-f5bca266788e	0	30	f	\N	\N
1ed12f9d-aa6f-450e-86f5-fcd6b57ba562	\N	\N	8211ffad-2ece-45e2-be4d-9509ea27c3d5	42da4271-4d88-4aea-9a8d-f5bca266788e	1	40	t	7ed4e25c-04a7-4880-bcc0-0c36c0c3248e	\N
078e225f-13fa-48ce-baa2-83d1ae1edc22	\N	http-basic-authenticator	8211ffad-2ece-45e2-be4d-9509ea27c3d5	13291038-27dc-42a1-b916-17090b8ae34c	0	10	f	\N	\N
\.


--
-- Data for Name: authentication_flow; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.authentication_flow (id, alias, description, realm_id, provider_id, top_level, built_in) FROM stdin;
b8e5713a-a3da-4ecd-9dd8-e567ce0b3a5a	browser	browser based authentication	19e687a7-a4bd-4974-9623-d44f5d246eb1	basic-flow	t	t
683c5b94-8edb-41b9-86ed-776cb5added7	forms	Username, password, otp and other auth forms.	19e687a7-a4bd-4974-9623-d44f5d246eb1	basic-flow	f	t
a15ba0ac-41b4-4fab-b912-a15e165da803	Browser - Conditional OTP	Flow to determine if the OTP is required for the authentication	19e687a7-a4bd-4974-9623-d44f5d246eb1	basic-flow	f	t
2d037f8c-ea48-4173-80f6-c67cd33788a9	direct grant	OpenID Connect Resource Owner Grant	19e687a7-a4bd-4974-9623-d44f5d246eb1	basic-flow	t	t
aff8d840-cad0-4621-aee5-2f0a6d3530c5	Direct Grant - Conditional OTP	Flow to determine if the OTP is required for the authentication	19e687a7-a4bd-4974-9623-d44f5d246eb1	basic-flow	f	t
f5b0f0fc-55cb-4a45-997e-fa1c0c8b452a	registration	registration flow	19e687a7-a4bd-4974-9623-d44f5d246eb1	basic-flow	t	t
d432ce1c-9e2b-4829-a0d3-cddc0238dedd	registration form	registration form	19e687a7-a4bd-4974-9623-d44f5d246eb1	form-flow	f	t
f36063ba-cae3-4ba6-b537-e7820d44088d	reset credentials	Reset credentials for a user if they forgot their password or something	19e687a7-a4bd-4974-9623-d44f5d246eb1	basic-flow	t	t
6c4c5592-2bc8-43bd-8c25-80a1f2af3e37	Reset - Conditional OTP	Flow to determine if the OTP should be reset or not. Set to REQUIRED to force.	19e687a7-a4bd-4974-9623-d44f5d246eb1	basic-flow	f	t
f3abbf9a-321e-43b6-add0-f4cc33fc1a68	clients	Base authentication for clients	19e687a7-a4bd-4974-9623-d44f5d246eb1	client-flow	t	t
def07d2e-2c0f-4501-a898-16a21c130bfc	first broker login	Actions taken after first broker login with identity provider account, which is not yet linked to any Keycloak account	19e687a7-a4bd-4974-9623-d44f5d246eb1	basic-flow	t	t
94d62eeb-9695-4187-89a8-d798627e7721	User creation or linking	Flow for the existing/non-existing user alternatives	19e687a7-a4bd-4974-9623-d44f5d246eb1	basic-flow	f	t
19e84e48-68cb-4e47-9538-b6bc5f3d646b	Handle Existing Account	Handle what to do if there is existing account with same email/username like authenticated identity provider	19e687a7-a4bd-4974-9623-d44f5d246eb1	basic-flow	f	t
e72a92f5-2f38-4c64-9330-b8d91954f261	Account verification options	Method with which to verity the existing account	19e687a7-a4bd-4974-9623-d44f5d246eb1	basic-flow	f	t
e294fcbd-90db-41bc-8b7b-9e5c2b70d280	Verify Existing Account by Re-authentication	Reauthentication of existing account	19e687a7-a4bd-4974-9623-d44f5d246eb1	basic-flow	f	t
b903bb9f-a3f4-402f-9814-6e7719de7b17	First broker login - Conditional OTP	Flow to determine if the OTP is required for the authentication	19e687a7-a4bd-4974-9623-d44f5d246eb1	basic-flow	f	t
40e5e5b6-6b8f-4ad9-b9fa-8ab4303dcea3	saml ecp	SAML ECP Profile Authentication Flow	19e687a7-a4bd-4974-9623-d44f5d246eb1	basic-flow	t	t
b9efbeb9-9d2c-4f7e-90c2-91c34d2264ce	docker auth	Used by Docker clients to authenticate against the IDP	19e687a7-a4bd-4974-9623-d44f5d246eb1	basic-flow	t	t
37df5f04-1368-4a7d-98c6-2867510ef82f	Account verification options	Method with which to verity the existing account	8211ffad-2ece-45e2-be4d-9509ea27c3d5	basic-flow	f	t
2df329ab-e393-4af6-bf18-5e44f2990876	Browser - Conditional OTP	Flow to determine if the OTP is required for the authentication	8211ffad-2ece-45e2-be4d-9509ea27c3d5	basic-flow	f	t
e89866fb-b02b-485d-911a-2796bff325ea	Direct Grant - Conditional OTP	Flow to determine if the OTP is required for the authentication	8211ffad-2ece-45e2-be4d-9509ea27c3d5	basic-flow	f	t
9365d3bc-241a-4c3c-a95f-6edda7e04e6d	First broker login - Conditional OTP	Flow to determine if the OTP is required for the authentication	8211ffad-2ece-45e2-be4d-9509ea27c3d5	basic-flow	f	t
9a618e72-496f-46ad-a48b-dc522efb304d	Handle Existing Account	Handle what to do if there is existing account with same email/username like authenticated identity provider	8211ffad-2ece-45e2-be4d-9509ea27c3d5	basic-flow	f	t
7ed4e25c-04a7-4880-bcc0-0c36c0c3248e	Reset - Conditional OTP	Flow to determine if the OTP should be reset or not. Set to REQUIRED to force.	8211ffad-2ece-45e2-be4d-9509ea27c3d5	basic-flow	f	t
241c9166-fb28-4572-b932-359af0e65354	Two-Factor Auth	OTP or WebAuthn	8211ffad-2ece-45e2-be4d-9509ea27c3d5	basic-flow	f	f
2ad81ac6-e391-4d7f-91cd-9cd59be62c80	User Login		8211ffad-2ece-45e2-be4d-9509ea27c3d5	basic-flow	f	f
10f74bc8-fca4-476c-96ef-9dd849410b88	User creation or linking	Flow for the existing/non-existing user alternatives	8211ffad-2ece-45e2-be4d-9509ea27c3d5	basic-flow	f	t
aba7d391-0054-4c5b-880b-a940907d18e2	Verify Existing Account by Re-authentication	Reauthentication of existing account	8211ffad-2ece-45e2-be4d-9509ea27c3d5	basic-flow	f	t
b705ebeb-91ec-42b4-8072-c33b24ae8868	WebAuthn Browser		8211ffad-2ece-45e2-be4d-9509ea27c3d5	basic-flow	t	f
0ea9bae7-b4e7-44b2-8ea7-c2ae3b32d09a	WebAuthn Browser forms	Username, password, otp and other auth forms.	8211ffad-2ece-45e2-be4d-9509ea27c3d5	basic-flow	f	f
17f33049-2463-4ed2-ae92-a6fbaabacd5e	browser	browser based authentication	8211ffad-2ece-45e2-be4d-9509ea27c3d5	basic-flow	t	t
57fc7d69-26a6-4815-95ef-84c4bd3b1f64	clients	Base authentication for clients	8211ffad-2ece-45e2-be4d-9509ea27c3d5	client-flow	t	t
787d2a18-62ab-4853-a171-e75548103b79	direct grant	OpenID Connect Resource Owner Grant	8211ffad-2ece-45e2-be4d-9509ea27c3d5	basic-flow	t	t
91484abe-cb9b-48e8-b783-159fb070059d	docker auth	Used by Docker clients to authenticate against the IDP	8211ffad-2ece-45e2-be4d-9509ea27c3d5	basic-flow	t	t
411f7ab6-eaac-468f-8140-0af1d9ca1eb4	first broker login	Actions taken after first broker login with identity provider account, which is not yet linked to any Keycloak account	8211ffad-2ece-45e2-be4d-9509ea27c3d5	basic-flow	t	t
c59d455a-b2d1-441e-9198-2666b7c7af3e	forms	Username, password, otp and other auth forms.	8211ffad-2ece-45e2-be4d-9509ea27c3d5	basic-flow	f	t
83645a93-dfe1-496e-ab3c-387221c38e94	registration	registration flow	8211ffad-2ece-45e2-be4d-9509ea27c3d5	basic-flow	t	t
fb1731aa-b333-4f83-be69-eab51972eba2	registration form	registration form	8211ffad-2ece-45e2-be4d-9509ea27c3d5	form-flow	f	t
42da4271-4d88-4aea-9a8d-f5bca266788e	reset credentials	Reset credentials for a user if they forgot their password or something	8211ffad-2ece-45e2-be4d-9509ea27c3d5	basic-flow	t	t
13291038-27dc-42a1-b916-17090b8ae34c	saml ecp	SAML ECP Profile Authentication Flow	8211ffad-2ece-45e2-be4d-9509ea27c3d5	basic-flow	t	t
\.


--
-- Data for Name: authenticator_config; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.authenticator_config (id, alias, realm_id) FROM stdin;
c8bfcc97-0696-4d36-b0e7-435eec5e467f	review profile config	19e687a7-a4bd-4974-9623-d44f5d246eb1
69fd2fb8-ade4-4cab-b962-d80f13103b11	create unique user config	19e687a7-a4bd-4974-9623-d44f5d246eb1
3ebd377e-1f7b-4d43-86bf-0d2a2d77f70c	create unique user config	8211ffad-2ece-45e2-be4d-9509ea27c3d5
d39a28af-b5f5-4f1e-9b7e-9b7d8770987d	review profile config	8211ffad-2ece-45e2-be4d-9509ea27c3d5
\.


--
-- Data for Name: authenticator_config_entry; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.authenticator_config_entry (authenticator_id, value, name) FROM stdin;
69fd2fb8-ade4-4cab-b962-d80f13103b11	false	require.password.update.after.registration
c8bfcc97-0696-4d36-b0e7-435eec5e467f	missing	update.profile.on.first.login
3ebd377e-1f7b-4d43-86bf-0d2a2d77f70c	false	require.password.update.after.registration
d39a28af-b5f5-4f1e-9b7e-9b7d8770987d	missing	update.profile.on.first.login
\.


--
-- Data for Name: broker_link; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.broker_link (identity_provider, storage_provider_id, realm_id, broker_user_id, broker_username, token, user_id) FROM stdin;
\.


--
-- Data for Name: client; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.client (id, enabled, full_scope_allowed, client_id, not_before, public_client, secret, base_url, bearer_only, management_url, surrogate_auth_required, realm_id, protocol, node_rereg_timeout, frontchannel_logout, consent_required, name, service_accounts_enabled, client_authenticator_type, root_url, description, registration_token, standard_flow_enabled, implicit_flow_enabled, direct_access_grants_enabled, always_display_in_console) FROM stdin;
3ff497aa-8999-44cd-af9f-17d098e04a42	t	f	master-realm	0	f	\N	\N	t	\N	f	19e687a7-a4bd-4974-9623-d44f5d246eb1	\N	0	f	f	master Realm	f	client-secret	\N	\N	\N	t	f	f	f
adec04de-0d89-4b02-a17c-85c2a535c226	t	f	account	0	t	\N	/realms/master/account/	f	\N	f	19e687a7-a4bd-4974-9623-d44f5d246eb1	openid-connect	0	f	f	${client_account}	f	client-secret	${authBaseUrl}	\N	\N	t	f	f	f
70fbcc0a-f28d-4e1e-a96f-cf3ce4433149	t	f	account-console	0	t	\N	/realms/master/account/	f	\N	f	19e687a7-a4bd-4974-9623-d44f5d246eb1	openid-connect	0	f	f	${client_account-console}	f	client-secret	${authBaseUrl}	\N	\N	t	f	f	f
add41af2-b51e-4410-ac33-9419dc6a000c	t	f	broker	0	f	\N	\N	t	\N	f	19e687a7-a4bd-4974-9623-d44f5d246eb1	openid-connect	0	f	f	${client_broker}	f	client-secret	\N	\N	\N	t	f	f	f
79c8f459-6cb5-43cf-9527-51d3489cf8ac	t	f	security-admin-console	0	t	\N	/admin/master/console/	f	\N	f	19e687a7-a4bd-4974-9623-d44f5d246eb1	openid-connect	0	f	f	${client_security-admin-console}	f	client-secret	${authAdminUrl}	\N	\N	t	f	f	f
f543ee32-5347-4754-86c2-6772c9c0d044	t	f	admin-cli	0	t	\N	\N	f	\N	f	19e687a7-a4bd-4974-9623-d44f5d246eb1	openid-connect	0	f	f	${client_admin-cli}	f	client-secret	\N	\N	\N	f	f	t	f
e0df3a5b-d8f3-4641-af97-e2649ab5b9ab	t	f	fragjetzt-realm	0	f	\N	\N	t	\N	f	19e687a7-a4bd-4974-9623-d44f5d246eb1	\N	0	f	f	fragjetzt Realm	f	client-secret	\N	\N	\N	t	f	f	f
a6e6b6b5-1888-44a4-892d-202b54ae2459	t	f	account	0	t	\N	/realms/fragjetzt/account/	f	\N	f	8211ffad-2ece-45e2-be4d-9509ea27c3d5	openid-connect	0	f	f	${client_account}	f	client-secret	${authBaseUrl}	\N	\N	t	f	f	f
ad527668-535b-41b0-bfcd-3bbb4a3e684e	t	f	account-console	0	t	\N	/realms/fragjetzt/account/	f	\N	f	8211ffad-2ece-45e2-be4d-9509ea27c3d5	openid-connect	0	f	f	${client_account-console}	f	client-secret	${authBaseUrl}	\N	\N	t	f	f	f
a76d389d-ba3a-4df4-8aaa-0fde29f07482	t	f	admin-cli	0	t	\N	\N	f	\N	f	8211ffad-2ece-45e2-be4d-9509ea27c3d5	openid-connect	0	f	f	${client_admin-cli}	f	client-secret	\N	\N	\N	f	f	t	f
2f9dc197-7822-4483-8aae-99f87b573f22	t	f	broker	0	f	\N	\N	t	\N	f	8211ffad-2ece-45e2-be4d-9509ea27c3d5	openid-connect	0	f	f	${client_broker}	f	client-secret	\N	\N	\N	t	f	f	f
472187cb-354b-42de-acad-b0d8f8a9c031	t	t	frag.jetzt-frontend	0	t	\N	http://localhost:4200/home	f	http://localhost:4200	f	8211ffad-2ece-45e2-be4d-9509ea27c3d5	openid-connect	-1	t	f	frag.jetzt Frontend	f	client-secret	http://localhost:4200	Allows the Frontend to access Keycloak	\N	t	f	t	f
b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	t	f	realm-management	0	f	\N	\N	t	\N	f	8211ffad-2ece-45e2-be4d-9509ea27c3d5	openid-connect	0	f	f	${client_realm-management}	f	client-secret	\N	\N	\N	t	f	f	f
f41d3566-a7d2-4326-8425-4c9ce085d282	t	f	security-admin-console	0	t	\N	/admin/fragjetzt/console/	f	\N	f	8211ffad-2ece-45e2-be4d-9509ea27c3d5	openid-connect	0	f	f	${client_security-admin-console}	f	client-secret	${authAdminUrl}	\N	\N	t	f	f	f
\.


--
-- Data for Name: client_attributes; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.client_attributes (client_id, name, value) FROM stdin;
adec04de-0d89-4b02-a17c-85c2a535c226	post.logout.redirect.uris	+
70fbcc0a-f28d-4e1e-a96f-cf3ce4433149	post.logout.redirect.uris	+
70fbcc0a-f28d-4e1e-a96f-cf3ce4433149	pkce.code.challenge.method	S256
79c8f459-6cb5-43cf-9527-51d3489cf8ac	post.logout.redirect.uris	+
79c8f459-6cb5-43cf-9527-51d3489cf8ac	pkce.code.challenge.method	S256
a6e6b6b5-1888-44a4-892d-202b54ae2459	post.logout.redirect.uris	+
ad527668-535b-41b0-bfcd-3bbb4a3e684e	post.logout.redirect.uris	+
ad527668-535b-41b0-bfcd-3bbb4a3e684e	pkce.code.challenge.method	S256
a76d389d-ba3a-4df4-8aaa-0fde29f07482	post.logout.redirect.uris	+
2f9dc197-7822-4483-8aae-99f87b573f22	post.logout.redirect.uris	+
472187cb-354b-42de-acad-b0d8f8a9c031	oidc.ciba.grant.enabled	false
472187cb-354b-42de-acad-b0d8f8a9c031	client.secret.creation.time	1692124341
472187cb-354b-42de-acad-b0d8f8a9c031	backchannel.logout.session.required	true
472187cb-354b-42de-acad-b0d8f8a9c031	post.logout.redirect.uris	http://localhost:4200/home
472187cb-354b-42de-acad-b0d8f8a9c031	display.on.consent.screen	false
472187cb-354b-42de-acad-b0d8f8a9c031	oauth2.device.authorization.grant.enabled	false
472187cb-354b-42de-acad-b0d8f8a9c031	backchannel.logout.revoke.offline.tokens	false
b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	post.logout.redirect.uris	+
f41d3566-a7d2-4326-8425-4c9ce085d282	post.logout.redirect.uris	+
f41d3566-a7d2-4326-8425-4c9ce085d282	pkce.code.challenge.method	S256
\.


--
-- Data for Name: client_auth_flow_bindings; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.client_auth_flow_bindings (client_id, flow_id, binding_name) FROM stdin;
\.


--
-- Data for Name: client_initial_access; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.client_initial_access (id, realm_id, "timestamp", expiration, count, remaining_count) FROM stdin;
\.


--
-- Data for Name: client_node_registrations; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.client_node_registrations (client_id, value, name) FROM stdin;
\.


--
-- Data for Name: client_scope; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.client_scope (id, name, realm_id, description, protocol) FROM stdin;
d82f684e-5e80-4f6d-896c-0d5815ed9036	offline_access	19e687a7-a4bd-4974-9623-d44f5d246eb1	OpenID Connect built-in scope: offline_access	openid-connect
1d1514b7-0e86-4d1b-abce-a753327a9a3e	role_list	19e687a7-a4bd-4974-9623-d44f5d246eb1	SAML role list	saml
894ea45c-2e6b-4ec7-af1d-67dfa311ca84	profile	19e687a7-a4bd-4974-9623-d44f5d246eb1	OpenID Connect built-in scope: profile	openid-connect
af34023f-5047-4ba8-97af-a34962abaaa2	email	19e687a7-a4bd-4974-9623-d44f5d246eb1	OpenID Connect built-in scope: email	openid-connect
47e6c487-6677-413a-a4da-9a319c3cde69	address	19e687a7-a4bd-4974-9623-d44f5d246eb1	OpenID Connect built-in scope: address	openid-connect
42ebb2c1-c204-4d65-8037-6024d7a86c51	phone	19e687a7-a4bd-4974-9623-d44f5d246eb1	OpenID Connect built-in scope: phone	openid-connect
2159cdef-1e6e-4042-9bc4-e27d25500d7b	roles	19e687a7-a4bd-4974-9623-d44f5d246eb1	OpenID Connect scope for add user roles to the access token	openid-connect
660bd312-8871-4451-84a1-c7368b29a90a	web-origins	19e687a7-a4bd-4974-9623-d44f5d246eb1	OpenID Connect scope for add allowed web origins to the access token	openid-connect
1fffa51b-bddc-42fa-b194-8b7b297ee10e	microprofile-jwt	19e687a7-a4bd-4974-9623-d44f5d246eb1	Microprofile - JWT built-in scope	openid-connect
5570a29b-70d4-4a4e-9cb3-dedee9ce6f8a	acr	19e687a7-a4bd-4974-9623-d44f5d246eb1	OpenID Connect scope for add acr (authentication context class reference) to the token	openid-connect
0752ff44-11f5-4b5b-bde5-3d7be9386298	role_list	8211ffad-2ece-45e2-be4d-9509ea27c3d5	SAML role list	saml
0cd91965-a09c-4bda-a42a-fc9cb861eedf	address	8211ffad-2ece-45e2-be4d-9509ea27c3d5	OpenID Connect built-in scope: address	openid-connect
566a6009-e84e-4929-9e04-82514fd7d9ad	acr	8211ffad-2ece-45e2-be4d-9509ea27c3d5	OpenID Connect scope for add acr (authentication context class reference) to the token	openid-connect
7eb9b141-bc61-44b4-88ef-100e4a24eeb4	phone	8211ffad-2ece-45e2-be4d-9509ea27c3d5	OpenID Connect built-in scope: phone	openid-connect
0a3e7bd4-fb95-4fdc-8285-813707299d9b	email	8211ffad-2ece-45e2-be4d-9509ea27c3d5	OpenID Connect built-in scope: email	openid-connect
11429889-deb1-4e5d-9b2c-06e8070b0f68	roles	8211ffad-2ece-45e2-be4d-9509ea27c3d5	OpenID Connect scope for add user roles to the access token	openid-connect
d4c2adb0-6164-480a-8740-e84b18f86045	web-origins	8211ffad-2ece-45e2-be4d-9509ea27c3d5	OpenID Connect scope for add allowed web origins to the access token	openid-connect
df584f35-0de0-4de7-939d-635bd41a9229	profile	8211ffad-2ece-45e2-be4d-9509ea27c3d5	OpenID Connect built-in scope: profile	openid-connect
6be0da5b-3e6e-4599-86d0-6aa51519f8df	microprofile-jwt	8211ffad-2ece-45e2-be4d-9509ea27c3d5	Microprofile - JWT built-in scope	openid-connect
4545cb6b-9d1d-477e-a19f-0c34a0a2f9fa	offline_access	8211ffad-2ece-45e2-be4d-9509ea27c3d5	OpenID Connect built-in scope: offline_access	openid-connect
\.


--
-- Data for Name: client_scope_attributes; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.client_scope_attributes (scope_id, value, name) FROM stdin;
d82f684e-5e80-4f6d-896c-0d5815ed9036	true	display.on.consent.screen
d82f684e-5e80-4f6d-896c-0d5815ed9036	${offlineAccessScopeConsentText}	consent.screen.text
1d1514b7-0e86-4d1b-abce-a753327a9a3e	true	display.on.consent.screen
1d1514b7-0e86-4d1b-abce-a753327a9a3e	${samlRoleListScopeConsentText}	consent.screen.text
894ea45c-2e6b-4ec7-af1d-67dfa311ca84	true	display.on.consent.screen
894ea45c-2e6b-4ec7-af1d-67dfa311ca84	${profileScopeConsentText}	consent.screen.text
894ea45c-2e6b-4ec7-af1d-67dfa311ca84	true	include.in.token.scope
af34023f-5047-4ba8-97af-a34962abaaa2	true	display.on.consent.screen
af34023f-5047-4ba8-97af-a34962abaaa2	${emailScopeConsentText}	consent.screen.text
af34023f-5047-4ba8-97af-a34962abaaa2	true	include.in.token.scope
47e6c487-6677-413a-a4da-9a319c3cde69	true	display.on.consent.screen
47e6c487-6677-413a-a4da-9a319c3cde69	${addressScopeConsentText}	consent.screen.text
47e6c487-6677-413a-a4da-9a319c3cde69	true	include.in.token.scope
42ebb2c1-c204-4d65-8037-6024d7a86c51	true	display.on.consent.screen
42ebb2c1-c204-4d65-8037-6024d7a86c51	${phoneScopeConsentText}	consent.screen.text
42ebb2c1-c204-4d65-8037-6024d7a86c51	true	include.in.token.scope
2159cdef-1e6e-4042-9bc4-e27d25500d7b	true	display.on.consent.screen
2159cdef-1e6e-4042-9bc4-e27d25500d7b	${rolesScopeConsentText}	consent.screen.text
2159cdef-1e6e-4042-9bc4-e27d25500d7b	false	include.in.token.scope
660bd312-8871-4451-84a1-c7368b29a90a	false	display.on.consent.screen
660bd312-8871-4451-84a1-c7368b29a90a		consent.screen.text
660bd312-8871-4451-84a1-c7368b29a90a	false	include.in.token.scope
1fffa51b-bddc-42fa-b194-8b7b297ee10e	false	display.on.consent.screen
1fffa51b-bddc-42fa-b194-8b7b297ee10e	true	include.in.token.scope
5570a29b-70d4-4a4e-9cb3-dedee9ce6f8a	false	display.on.consent.screen
5570a29b-70d4-4a4e-9cb3-dedee9ce6f8a	false	include.in.token.scope
0752ff44-11f5-4b5b-bde5-3d7be9386298	${samlRoleListScopeConsentText}	consent.screen.text
0752ff44-11f5-4b5b-bde5-3d7be9386298	true	display.on.consent.screen
0cd91965-a09c-4bda-a42a-fc9cb861eedf	true	include.in.token.scope
0cd91965-a09c-4bda-a42a-fc9cb861eedf	true	display.on.consent.screen
0cd91965-a09c-4bda-a42a-fc9cb861eedf	${addressScopeConsentText}	consent.screen.text
566a6009-e84e-4929-9e04-82514fd7d9ad	false	include.in.token.scope
566a6009-e84e-4929-9e04-82514fd7d9ad	false	display.on.consent.screen
7eb9b141-bc61-44b4-88ef-100e4a24eeb4	true	include.in.token.scope
7eb9b141-bc61-44b4-88ef-100e4a24eeb4	true	display.on.consent.screen
7eb9b141-bc61-44b4-88ef-100e4a24eeb4	${phoneScopeConsentText}	consent.screen.text
0a3e7bd4-fb95-4fdc-8285-813707299d9b	true	include.in.token.scope
0a3e7bd4-fb95-4fdc-8285-813707299d9b	true	display.on.consent.screen
0a3e7bd4-fb95-4fdc-8285-813707299d9b	${emailScopeConsentText}	consent.screen.text
11429889-deb1-4e5d-9b2c-06e8070b0f68	false	include.in.token.scope
11429889-deb1-4e5d-9b2c-06e8070b0f68	true	display.on.consent.screen
11429889-deb1-4e5d-9b2c-06e8070b0f68	${rolesScopeConsentText}	consent.screen.text
d4c2adb0-6164-480a-8740-e84b18f86045	false	include.in.token.scope
d4c2adb0-6164-480a-8740-e84b18f86045	false	display.on.consent.screen
d4c2adb0-6164-480a-8740-e84b18f86045		consent.screen.text
df584f35-0de0-4de7-939d-635bd41a9229	true	include.in.token.scope
df584f35-0de0-4de7-939d-635bd41a9229	true	display.on.consent.screen
df584f35-0de0-4de7-939d-635bd41a9229	${profileScopeConsentText}	consent.screen.text
6be0da5b-3e6e-4599-86d0-6aa51519f8df	true	include.in.token.scope
6be0da5b-3e6e-4599-86d0-6aa51519f8df	false	display.on.consent.screen
4545cb6b-9d1d-477e-a19f-0c34a0a2f9fa	${offlineAccessScopeConsentText}	consent.screen.text
4545cb6b-9d1d-477e-a19f-0c34a0a2f9fa	true	display.on.consent.screen
\.


--
-- Data for Name: client_scope_client; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.client_scope_client (client_id, scope_id, default_scope) FROM stdin;
adec04de-0d89-4b02-a17c-85c2a535c226	af34023f-5047-4ba8-97af-a34962abaaa2	t
adec04de-0d89-4b02-a17c-85c2a535c226	894ea45c-2e6b-4ec7-af1d-67dfa311ca84	t
adec04de-0d89-4b02-a17c-85c2a535c226	5570a29b-70d4-4a4e-9cb3-dedee9ce6f8a	t
adec04de-0d89-4b02-a17c-85c2a535c226	2159cdef-1e6e-4042-9bc4-e27d25500d7b	t
adec04de-0d89-4b02-a17c-85c2a535c226	660bd312-8871-4451-84a1-c7368b29a90a	t
adec04de-0d89-4b02-a17c-85c2a535c226	d82f684e-5e80-4f6d-896c-0d5815ed9036	f
adec04de-0d89-4b02-a17c-85c2a535c226	47e6c487-6677-413a-a4da-9a319c3cde69	f
adec04de-0d89-4b02-a17c-85c2a535c226	1fffa51b-bddc-42fa-b194-8b7b297ee10e	f
adec04de-0d89-4b02-a17c-85c2a535c226	42ebb2c1-c204-4d65-8037-6024d7a86c51	f
70fbcc0a-f28d-4e1e-a96f-cf3ce4433149	af34023f-5047-4ba8-97af-a34962abaaa2	t
70fbcc0a-f28d-4e1e-a96f-cf3ce4433149	894ea45c-2e6b-4ec7-af1d-67dfa311ca84	t
70fbcc0a-f28d-4e1e-a96f-cf3ce4433149	5570a29b-70d4-4a4e-9cb3-dedee9ce6f8a	t
70fbcc0a-f28d-4e1e-a96f-cf3ce4433149	2159cdef-1e6e-4042-9bc4-e27d25500d7b	t
70fbcc0a-f28d-4e1e-a96f-cf3ce4433149	660bd312-8871-4451-84a1-c7368b29a90a	t
70fbcc0a-f28d-4e1e-a96f-cf3ce4433149	d82f684e-5e80-4f6d-896c-0d5815ed9036	f
70fbcc0a-f28d-4e1e-a96f-cf3ce4433149	47e6c487-6677-413a-a4da-9a319c3cde69	f
70fbcc0a-f28d-4e1e-a96f-cf3ce4433149	1fffa51b-bddc-42fa-b194-8b7b297ee10e	f
70fbcc0a-f28d-4e1e-a96f-cf3ce4433149	42ebb2c1-c204-4d65-8037-6024d7a86c51	f
f543ee32-5347-4754-86c2-6772c9c0d044	af34023f-5047-4ba8-97af-a34962abaaa2	t
f543ee32-5347-4754-86c2-6772c9c0d044	894ea45c-2e6b-4ec7-af1d-67dfa311ca84	t
f543ee32-5347-4754-86c2-6772c9c0d044	5570a29b-70d4-4a4e-9cb3-dedee9ce6f8a	t
f543ee32-5347-4754-86c2-6772c9c0d044	2159cdef-1e6e-4042-9bc4-e27d25500d7b	t
f543ee32-5347-4754-86c2-6772c9c0d044	660bd312-8871-4451-84a1-c7368b29a90a	t
f543ee32-5347-4754-86c2-6772c9c0d044	d82f684e-5e80-4f6d-896c-0d5815ed9036	f
f543ee32-5347-4754-86c2-6772c9c0d044	47e6c487-6677-413a-a4da-9a319c3cde69	f
f543ee32-5347-4754-86c2-6772c9c0d044	1fffa51b-bddc-42fa-b194-8b7b297ee10e	f
f543ee32-5347-4754-86c2-6772c9c0d044	42ebb2c1-c204-4d65-8037-6024d7a86c51	f
add41af2-b51e-4410-ac33-9419dc6a000c	af34023f-5047-4ba8-97af-a34962abaaa2	t
add41af2-b51e-4410-ac33-9419dc6a000c	894ea45c-2e6b-4ec7-af1d-67dfa311ca84	t
add41af2-b51e-4410-ac33-9419dc6a000c	5570a29b-70d4-4a4e-9cb3-dedee9ce6f8a	t
add41af2-b51e-4410-ac33-9419dc6a000c	2159cdef-1e6e-4042-9bc4-e27d25500d7b	t
add41af2-b51e-4410-ac33-9419dc6a000c	660bd312-8871-4451-84a1-c7368b29a90a	t
add41af2-b51e-4410-ac33-9419dc6a000c	d82f684e-5e80-4f6d-896c-0d5815ed9036	f
add41af2-b51e-4410-ac33-9419dc6a000c	47e6c487-6677-413a-a4da-9a319c3cde69	f
add41af2-b51e-4410-ac33-9419dc6a000c	1fffa51b-bddc-42fa-b194-8b7b297ee10e	f
add41af2-b51e-4410-ac33-9419dc6a000c	42ebb2c1-c204-4d65-8037-6024d7a86c51	f
3ff497aa-8999-44cd-af9f-17d098e04a42	af34023f-5047-4ba8-97af-a34962abaaa2	t
3ff497aa-8999-44cd-af9f-17d098e04a42	894ea45c-2e6b-4ec7-af1d-67dfa311ca84	t
3ff497aa-8999-44cd-af9f-17d098e04a42	5570a29b-70d4-4a4e-9cb3-dedee9ce6f8a	t
3ff497aa-8999-44cd-af9f-17d098e04a42	2159cdef-1e6e-4042-9bc4-e27d25500d7b	t
3ff497aa-8999-44cd-af9f-17d098e04a42	660bd312-8871-4451-84a1-c7368b29a90a	t
3ff497aa-8999-44cd-af9f-17d098e04a42	d82f684e-5e80-4f6d-896c-0d5815ed9036	f
3ff497aa-8999-44cd-af9f-17d098e04a42	47e6c487-6677-413a-a4da-9a319c3cde69	f
3ff497aa-8999-44cd-af9f-17d098e04a42	1fffa51b-bddc-42fa-b194-8b7b297ee10e	f
3ff497aa-8999-44cd-af9f-17d098e04a42	42ebb2c1-c204-4d65-8037-6024d7a86c51	f
79c8f459-6cb5-43cf-9527-51d3489cf8ac	af34023f-5047-4ba8-97af-a34962abaaa2	t
79c8f459-6cb5-43cf-9527-51d3489cf8ac	894ea45c-2e6b-4ec7-af1d-67dfa311ca84	t
79c8f459-6cb5-43cf-9527-51d3489cf8ac	5570a29b-70d4-4a4e-9cb3-dedee9ce6f8a	t
79c8f459-6cb5-43cf-9527-51d3489cf8ac	2159cdef-1e6e-4042-9bc4-e27d25500d7b	t
79c8f459-6cb5-43cf-9527-51d3489cf8ac	660bd312-8871-4451-84a1-c7368b29a90a	t
79c8f459-6cb5-43cf-9527-51d3489cf8ac	d82f684e-5e80-4f6d-896c-0d5815ed9036	f
79c8f459-6cb5-43cf-9527-51d3489cf8ac	47e6c487-6677-413a-a4da-9a319c3cde69	f
79c8f459-6cb5-43cf-9527-51d3489cf8ac	1fffa51b-bddc-42fa-b194-8b7b297ee10e	f
79c8f459-6cb5-43cf-9527-51d3489cf8ac	42ebb2c1-c204-4d65-8037-6024d7a86c51	f
a6e6b6b5-1888-44a4-892d-202b54ae2459	d4c2adb0-6164-480a-8740-e84b18f86045	t
a6e6b6b5-1888-44a4-892d-202b54ae2459	566a6009-e84e-4929-9e04-82514fd7d9ad	t
a6e6b6b5-1888-44a4-892d-202b54ae2459	11429889-deb1-4e5d-9b2c-06e8070b0f68	t
a6e6b6b5-1888-44a4-892d-202b54ae2459	df584f35-0de0-4de7-939d-635bd41a9229	t
a6e6b6b5-1888-44a4-892d-202b54ae2459	0a3e7bd4-fb95-4fdc-8285-813707299d9b	t
a6e6b6b5-1888-44a4-892d-202b54ae2459	0cd91965-a09c-4bda-a42a-fc9cb861eedf	f
a6e6b6b5-1888-44a4-892d-202b54ae2459	7eb9b141-bc61-44b4-88ef-100e4a24eeb4	f
a6e6b6b5-1888-44a4-892d-202b54ae2459	4545cb6b-9d1d-477e-a19f-0c34a0a2f9fa	f
a6e6b6b5-1888-44a4-892d-202b54ae2459	6be0da5b-3e6e-4599-86d0-6aa51519f8df	f
ad527668-535b-41b0-bfcd-3bbb4a3e684e	d4c2adb0-6164-480a-8740-e84b18f86045	t
ad527668-535b-41b0-bfcd-3bbb4a3e684e	566a6009-e84e-4929-9e04-82514fd7d9ad	t
ad527668-535b-41b0-bfcd-3bbb4a3e684e	11429889-deb1-4e5d-9b2c-06e8070b0f68	t
ad527668-535b-41b0-bfcd-3bbb4a3e684e	df584f35-0de0-4de7-939d-635bd41a9229	t
ad527668-535b-41b0-bfcd-3bbb4a3e684e	0a3e7bd4-fb95-4fdc-8285-813707299d9b	t
ad527668-535b-41b0-bfcd-3bbb4a3e684e	0cd91965-a09c-4bda-a42a-fc9cb861eedf	f
ad527668-535b-41b0-bfcd-3bbb4a3e684e	7eb9b141-bc61-44b4-88ef-100e4a24eeb4	f
ad527668-535b-41b0-bfcd-3bbb4a3e684e	4545cb6b-9d1d-477e-a19f-0c34a0a2f9fa	f
ad527668-535b-41b0-bfcd-3bbb4a3e684e	6be0da5b-3e6e-4599-86d0-6aa51519f8df	f
a76d389d-ba3a-4df4-8aaa-0fde29f07482	d4c2adb0-6164-480a-8740-e84b18f86045	t
a76d389d-ba3a-4df4-8aaa-0fde29f07482	566a6009-e84e-4929-9e04-82514fd7d9ad	t
a76d389d-ba3a-4df4-8aaa-0fde29f07482	11429889-deb1-4e5d-9b2c-06e8070b0f68	t
a76d389d-ba3a-4df4-8aaa-0fde29f07482	df584f35-0de0-4de7-939d-635bd41a9229	t
a76d389d-ba3a-4df4-8aaa-0fde29f07482	0a3e7bd4-fb95-4fdc-8285-813707299d9b	t
a76d389d-ba3a-4df4-8aaa-0fde29f07482	0cd91965-a09c-4bda-a42a-fc9cb861eedf	f
a76d389d-ba3a-4df4-8aaa-0fde29f07482	7eb9b141-bc61-44b4-88ef-100e4a24eeb4	f
a76d389d-ba3a-4df4-8aaa-0fde29f07482	4545cb6b-9d1d-477e-a19f-0c34a0a2f9fa	f
a76d389d-ba3a-4df4-8aaa-0fde29f07482	6be0da5b-3e6e-4599-86d0-6aa51519f8df	f
2f9dc197-7822-4483-8aae-99f87b573f22	d4c2adb0-6164-480a-8740-e84b18f86045	t
2f9dc197-7822-4483-8aae-99f87b573f22	566a6009-e84e-4929-9e04-82514fd7d9ad	t
2f9dc197-7822-4483-8aae-99f87b573f22	11429889-deb1-4e5d-9b2c-06e8070b0f68	t
2f9dc197-7822-4483-8aae-99f87b573f22	df584f35-0de0-4de7-939d-635bd41a9229	t
2f9dc197-7822-4483-8aae-99f87b573f22	0a3e7bd4-fb95-4fdc-8285-813707299d9b	t
2f9dc197-7822-4483-8aae-99f87b573f22	0cd91965-a09c-4bda-a42a-fc9cb861eedf	f
2f9dc197-7822-4483-8aae-99f87b573f22	7eb9b141-bc61-44b4-88ef-100e4a24eeb4	f
2f9dc197-7822-4483-8aae-99f87b573f22	4545cb6b-9d1d-477e-a19f-0c34a0a2f9fa	f
2f9dc197-7822-4483-8aae-99f87b573f22	6be0da5b-3e6e-4599-86d0-6aa51519f8df	f
472187cb-354b-42de-acad-b0d8f8a9c031	d4c2adb0-6164-480a-8740-e84b18f86045	t
472187cb-354b-42de-acad-b0d8f8a9c031	566a6009-e84e-4929-9e04-82514fd7d9ad	t
472187cb-354b-42de-acad-b0d8f8a9c031	11429889-deb1-4e5d-9b2c-06e8070b0f68	t
472187cb-354b-42de-acad-b0d8f8a9c031	df584f35-0de0-4de7-939d-635bd41a9229	t
472187cb-354b-42de-acad-b0d8f8a9c031	0a3e7bd4-fb95-4fdc-8285-813707299d9b	t
472187cb-354b-42de-acad-b0d8f8a9c031	0cd91965-a09c-4bda-a42a-fc9cb861eedf	f
472187cb-354b-42de-acad-b0d8f8a9c031	7eb9b141-bc61-44b4-88ef-100e4a24eeb4	f
472187cb-354b-42de-acad-b0d8f8a9c031	4545cb6b-9d1d-477e-a19f-0c34a0a2f9fa	f
472187cb-354b-42de-acad-b0d8f8a9c031	6be0da5b-3e6e-4599-86d0-6aa51519f8df	f
b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	d4c2adb0-6164-480a-8740-e84b18f86045	t
b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	566a6009-e84e-4929-9e04-82514fd7d9ad	t
b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	11429889-deb1-4e5d-9b2c-06e8070b0f68	t
b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	df584f35-0de0-4de7-939d-635bd41a9229	t
b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	0a3e7bd4-fb95-4fdc-8285-813707299d9b	t
b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	0cd91965-a09c-4bda-a42a-fc9cb861eedf	f
b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	7eb9b141-bc61-44b4-88ef-100e4a24eeb4	f
b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	4545cb6b-9d1d-477e-a19f-0c34a0a2f9fa	f
b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	6be0da5b-3e6e-4599-86d0-6aa51519f8df	f
f41d3566-a7d2-4326-8425-4c9ce085d282	d4c2adb0-6164-480a-8740-e84b18f86045	t
f41d3566-a7d2-4326-8425-4c9ce085d282	566a6009-e84e-4929-9e04-82514fd7d9ad	t
f41d3566-a7d2-4326-8425-4c9ce085d282	11429889-deb1-4e5d-9b2c-06e8070b0f68	t
f41d3566-a7d2-4326-8425-4c9ce085d282	df584f35-0de0-4de7-939d-635bd41a9229	t
f41d3566-a7d2-4326-8425-4c9ce085d282	0a3e7bd4-fb95-4fdc-8285-813707299d9b	t
f41d3566-a7d2-4326-8425-4c9ce085d282	0cd91965-a09c-4bda-a42a-fc9cb861eedf	f
f41d3566-a7d2-4326-8425-4c9ce085d282	7eb9b141-bc61-44b4-88ef-100e4a24eeb4	f
f41d3566-a7d2-4326-8425-4c9ce085d282	4545cb6b-9d1d-477e-a19f-0c34a0a2f9fa	f
f41d3566-a7d2-4326-8425-4c9ce085d282	6be0da5b-3e6e-4599-86d0-6aa51519f8df	f
\.


--
-- Data for Name: client_scope_role_mapping; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.client_scope_role_mapping (scope_id, role_id) FROM stdin;
d82f684e-5e80-4f6d-896c-0d5815ed9036	65fbf254-0242-466b-bcb0-4eca469ce313
4545cb6b-9d1d-477e-a19f-0c34a0a2f9fa	e35019e2-a64f-4807-b703-6eaa9ae264eb
\.


--
-- Data for Name: client_session; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.client_session (id, client_id, redirect_uri, state, "timestamp", session_id, auth_method, realm_id, auth_user_id, current_action) FROM stdin;
\.


--
-- Data for Name: client_session_auth_status; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.client_session_auth_status (authenticator, status, client_session) FROM stdin;
\.


--
-- Data for Name: client_session_note; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.client_session_note (name, value, client_session) FROM stdin;
\.


--
-- Data for Name: client_session_prot_mapper; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.client_session_prot_mapper (protocol_mapper_id, client_session) FROM stdin;
\.


--
-- Data for Name: client_session_role; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.client_session_role (role_id, client_session) FROM stdin;
\.


--
-- Data for Name: client_user_session_note; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.client_user_session_note (name, value, client_session) FROM stdin;
\.


--
-- Data for Name: component; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.component (id, name, parent_id, provider_id, provider_type, realm_id, sub_type) FROM stdin;
8e21d1f0-250f-4b21-917c-e287453f1c2e	Trusted Hosts	19e687a7-a4bd-4974-9623-d44f5d246eb1	trusted-hosts	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	19e687a7-a4bd-4974-9623-d44f5d246eb1	anonymous
1f140396-5fe7-43e3-b2ff-a5315e9f7278	Consent Required	19e687a7-a4bd-4974-9623-d44f5d246eb1	consent-required	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	19e687a7-a4bd-4974-9623-d44f5d246eb1	anonymous
0448c576-bd5c-489b-a550-5c206339dfc0	Full Scope Disabled	19e687a7-a4bd-4974-9623-d44f5d246eb1	scope	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	19e687a7-a4bd-4974-9623-d44f5d246eb1	anonymous
ff303b9f-ccb5-4b72-ba5a-06965ec54432	Max Clients Limit	19e687a7-a4bd-4974-9623-d44f5d246eb1	max-clients	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	19e687a7-a4bd-4974-9623-d44f5d246eb1	anonymous
b8cfec13-ae4a-44ac-bf93-709548579e06	Allowed Protocol Mapper Types	19e687a7-a4bd-4974-9623-d44f5d246eb1	allowed-protocol-mappers	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	19e687a7-a4bd-4974-9623-d44f5d246eb1	anonymous
a74f0aa2-57de-4841-a94f-d61050415c5a	Allowed Client Scopes	19e687a7-a4bd-4974-9623-d44f5d246eb1	allowed-client-templates	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	19e687a7-a4bd-4974-9623-d44f5d246eb1	anonymous
1b794f40-f758-4b54-b9d5-f5a11db9d2f3	Allowed Protocol Mapper Types	19e687a7-a4bd-4974-9623-d44f5d246eb1	allowed-protocol-mappers	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	19e687a7-a4bd-4974-9623-d44f5d246eb1	authenticated
e4d2e3e2-1858-49b5-a3c0-a0cdf6e0a732	Allowed Client Scopes	19e687a7-a4bd-4974-9623-d44f5d246eb1	allowed-client-templates	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	19e687a7-a4bd-4974-9623-d44f5d246eb1	authenticated
3dcc7eed-6360-4ec1-9aba-d6a261ff3a75	rsa-generated	19e687a7-a4bd-4974-9623-d44f5d246eb1	rsa-generated	org.keycloak.keys.KeyProvider	19e687a7-a4bd-4974-9623-d44f5d246eb1	\N
9b3dfcca-8de0-4d75-9c04-c3d3cf55c1bd	rsa-enc-generated	19e687a7-a4bd-4974-9623-d44f5d246eb1	rsa-enc-generated	org.keycloak.keys.KeyProvider	19e687a7-a4bd-4974-9623-d44f5d246eb1	\N
1d56a762-e140-4286-bc30-d2083c12c256	hmac-generated	19e687a7-a4bd-4974-9623-d44f5d246eb1	hmac-generated	org.keycloak.keys.KeyProvider	19e687a7-a4bd-4974-9623-d44f5d246eb1	\N
25dd2dde-87f8-44ea-a4fe-3480476e71d6	aes-generated	19e687a7-a4bd-4974-9623-d44f5d246eb1	aes-generated	org.keycloak.keys.KeyProvider	19e687a7-a4bd-4974-9623-d44f5d246eb1	\N
56fb989a-9cd1-4e6a-a1a4-cb88cbaefe73	Full Scope Disabled	8211ffad-2ece-45e2-be4d-9509ea27c3d5	scope	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	8211ffad-2ece-45e2-be4d-9509ea27c3d5	anonymous
ca5af8ce-414b-48a2-b0b5-f63f5bc1cd3c	Consent Required	8211ffad-2ece-45e2-be4d-9509ea27c3d5	consent-required	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	8211ffad-2ece-45e2-be4d-9509ea27c3d5	anonymous
99942efb-3166-47b1-8a8b-2b1ffc61ea66	Max Clients Limit	8211ffad-2ece-45e2-be4d-9509ea27c3d5	max-clients	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	8211ffad-2ece-45e2-be4d-9509ea27c3d5	anonymous
9935693f-305a-4d78-8562-792e84441351	Allowed Protocol Mapper Types	8211ffad-2ece-45e2-be4d-9509ea27c3d5	allowed-protocol-mappers	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	8211ffad-2ece-45e2-be4d-9509ea27c3d5	anonymous
f0f6388b-5681-4667-a763-115bd9882a83	Trusted Hosts	8211ffad-2ece-45e2-be4d-9509ea27c3d5	trusted-hosts	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	8211ffad-2ece-45e2-be4d-9509ea27c3d5	anonymous
c51d0c12-5000-4522-b90c-fd153f94c350	Allowed Client Scopes	8211ffad-2ece-45e2-be4d-9509ea27c3d5	allowed-client-templates	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	8211ffad-2ece-45e2-be4d-9509ea27c3d5	anonymous
ff48ea3c-5780-44d3-b021-02344d57d139	Allowed Protocol Mapper Types	8211ffad-2ece-45e2-be4d-9509ea27c3d5	allowed-protocol-mappers	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	8211ffad-2ece-45e2-be4d-9509ea27c3d5	authenticated
265f9c0a-5f14-40f9-852d-7653a774a4cf	Allowed Client Scopes	8211ffad-2ece-45e2-be4d-9509ea27c3d5	allowed-client-templates	org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy	8211ffad-2ece-45e2-be4d-9509ea27c3d5	authenticated
5e986667-ad45-44d2-9065-2014ab087e09	rsa-enc-generated	8211ffad-2ece-45e2-be4d-9509ea27c3d5	rsa-enc-generated	org.keycloak.keys.KeyProvider	8211ffad-2ece-45e2-be4d-9509ea27c3d5	\N
9515bc53-c820-4ed5-b67b-4325c372eb35	hmac-generated	8211ffad-2ece-45e2-be4d-9509ea27c3d5	hmac-generated	org.keycloak.keys.KeyProvider	8211ffad-2ece-45e2-be4d-9509ea27c3d5	\N
bd011d5f-75fc-4192-a043-ff6acccdc16b	aes-generated	8211ffad-2ece-45e2-be4d-9509ea27c3d5	aes-generated	org.keycloak.keys.KeyProvider	8211ffad-2ece-45e2-be4d-9509ea27c3d5	\N
5f4fc823-3e0b-4089-9305-235674dc32eb	rsa-generated	8211ffad-2ece-45e2-be4d-9509ea27c3d5	rsa-generated	org.keycloak.keys.KeyProvider	8211ffad-2ece-45e2-be4d-9509ea27c3d5	\N
\.


--
-- Data for Name: component_config; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.component_config (id, component_id, name, value) FROM stdin;
ced6dd12-296a-4c28-aa45-5dbd73710433	8e21d1f0-250f-4b21-917c-e287453f1c2e	client-uris-must-match	true
14a6ad62-c456-4da7-8770-6b57d82b644f	8e21d1f0-250f-4b21-917c-e287453f1c2e	host-sending-registration-request-must-match	true
d2da5610-df62-46af-837c-972d513d7461	e4d2e3e2-1858-49b5-a3c0-a0cdf6e0a732	allow-default-scopes	true
df1a5ab3-3193-412d-8a88-84ecdd182878	b8cfec13-ae4a-44ac-bf93-709548579e06	allowed-protocol-mapper-types	saml-user-attribute-mapper
d7a8ec2f-a7e2-431d-8ef2-726ce3739c47	b8cfec13-ae4a-44ac-bf93-709548579e06	allowed-protocol-mapper-types	saml-role-list-mapper
faf0c799-51e5-48a3-8f37-4a918be22dc4	b8cfec13-ae4a-44ac-bf93-709548579e06	allowed-protocol-mapper-types	oidc-usermodel-property-mapper
5b95c473-93a0-4007-8ed8-b625f30d2538	b8cfec13-ae4a-44ac-bf93-709548579e06	allowed-protocol-mapper-types	oidc-address-mapper
638120db-ed18-44e5-9dea-26ea36a123e2	b8cfec13-ae4a-44ac-bf93-709548579e06	allowed-protocol-mapper-types	oidc-full-name-mapper
654b0e94-feab-4eff-9425-dfc7095fb08c	b8cfec13-ae4a-44ac-bf93-709548579e06	allowed-protocol-mapper-types	oidc-usermodel-attribute-mapper
3c68212f-0709-4c21-8fa4-8a13914314d6	b8cfec13-ae4a-44ac-bf93-709548579e06	allowed-protocol-mapper-types	oidc-sha256-pairwise-sub-mapper
824f434f-0e8c-4fce-b853-20eece6b2d2b	b8cfec13-ae4a-44ac-bf93-709548579e06	allowed-protocol-mapper-types	saml-user-property-mapper
fee66024-5378-4f2c-b067-e52f93fea1f3	ff303b9f-ccb5-4b72-ba5a-06965ec54432	max-clients	200
e757d4b0-6081-48be-af09-efbad622c4cb	a74f0aa2-57de-4841-a94f-d61050415c5a	allow-default-scopes	true
7435ef10-4725-489c-bb99-14ce476a06f7	1b794f40-f758-4b54-b9d5-f5a11db9d2f3	allowed-protocol-mapper-types	saml-role-list-mapper
17a047e7-c69b-416b-afd7-33e8bc802b9b	1b794f40-f758-4b54-b9d5-f5a11db9d2f3	allowed-protocol-mapper-types	saml-user-property-mapper
b9190804-fc2c-48e0-b1cf-44b265ea129f	1b794f40-f758-4b54-b9d5-f5a11db9d2f3	allowed-protocol-mapper-types	oidc-full-name-mapper
8b02ba97-4a46-405c-8c2e-4d8e91a8c386	1b794f40-f758-4b54-b9d5-f5a11db9d2f3	allowed-protocol-mapper-types	oidc-sha256-pairwise-sub-mapper
ab6b2e81-5573-48b7-b4eb-10953388056e	1b794f40-f758-4b54-b9d5-f5a11db9d2f3	allowed-protocol-mapper-types	oidc-usermodel-property-mapper
5c08c490-3c4b-480f-8cc1-0ac2896983f2	1b794f40-f758-4b54-b9d5-f5a11db9d2f3	allowed-protocol-mapper-types	oidc-address-mapper
f09cfa98-cf26-46b2-8121-8e385f4bd82b	1b794f40-f758-4b54-b9d5-f5a11db9d2f3	allowed-protocol-mapper-types	oidc-usermodel-attribute-mapper
fda4b714-45b9-43a7-b194-5bbcfbf14d98	1b794f40-f758-4b54-b9d5-f5a11db9d2f3	allowed-protocol-mapper-types	saml-user-attribute-mapper
7f777f66-e3b5-4f1e-a866-b23352304733	25dd2dde-87f8-44ea-a4fe-3480476e71d6	kid	bb5e398a-9144-43b2-8e62-9121617e0f27
81d4b0e4-1b7d-4317-b16a-013a2759cce4	25dd2dde-87f8-44ea-a4fe-3480476e71d6	secret	ud7WGLiRIWRcCh3kjRsb3w
c9103939-9b9f-47b7-89e7-5229abcd94fc	25dd2dde-87f8-44ea-a4fe-3480476e71d6	priority	100
8de15c97-0bd5-4325-aa96-51bf26815182	1d56a762-e140-4286-bc30-d2083c12c256	kid	b502f52f-99b6-4e72-b062-ff98e9ffe1ea
24a6f52b-496e-4763-8ff6-031c81ebb4f4	1d56a762-e140-4286-bc30-d2083c12c256	priority	100
f4e8997c-8686-4ae3-8b18-7a4152560385	1d56a762-e140-4286-bc30-d2083c12c256	secret	OxZL30PSH9ldgkosq6pll5enf6Ngv6G8tzYQ4T62Qjb6_vmJIxiKBcT6P3s0jGoJo80W0xvvxq0r2GDdSH-rJQ
3666ce19-6b5e-4d7f-922d-cdb4c0ff6a10	1d56a762-e140-4286-bc30-d2083c12c256	algorithm	HS256
f63ceeb8-054c-4448-81ec-8cd34b9587d2	3dcc7eed-6360-4ec1-9aba-d6a261ff3a75	keyUse	SIG
212e4e3b-dff2-4144-b357-c076d941df65	3dcc7eed-6360-4ec1-9aba-d6a261ff3a75	priority	100
4627a058-49cc-44be-bc62-2c649270d455	3dcc7eed-6360-4ec1-9aba-d6a261ff3a75	certificate	MIICmzCCAYMCBgGOzGvGZDANBgkqhkiG9w0BAQsFADARMQ8wDQYDVQQDDAZtYXN0ZXIwHhcNMjQwNDExMDkwODE5WhcNMzQwNDExMDkwOTU5WjARMQ8wDQYDVQQDDAZtYXN0ZXIwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQC1vvEBvHeCndEuprTJk4YpQeWxOkHVM2y6NG6sIS3PAPAV3kuggTyEx0GJA8jP1gTGrtZD4kMPSFagNVwU1RlsiqY6U9kDkfJYCzzHJtTEgXl0feX+bOmGe/wbHmxyLPpToEWxD2RCNsohLCzyDXB00nwO9fzj4lL+wyfHYp3tDgRAtx4kPh+u8+c1ea0cIquUKAaRbRb+H0DYN8R0razKCCS7CoSjbJlxzvO9mLMY1G3OWiRcCZ7WULyZZL7niOAgeOuOz4tf4FyD4OHbVcsyKGmVpkD7MuCdO7uooXtGLBL6iF01/aEwHaH/kgrO0rmV9GSA/koDmSNBgSsTH26tAgMBAAEwDQYJKoZIhvcNAQELBQADggEBAB3rOQ7CMItSRebzW68oSAss2h6/0jCgy6fOAVccfdHXVtaK18qb3RAvwSOTZfG0IHXzx0ZBOD1YJVW70z3I7Hf2i9Y5V47Ff7k8ptl7DW/U/5fWtpY72jRtbX1v/HujcXsVoXcES/rIDmiIUWj52y6R8A0W1rykQEnh4aq3E9zglbGtJceXlZ4gQ2R6BrZfK4YLpKyDeztyX3TxAs3cENxI6kwx/kzeYw7cPtz1Lfbt79N5qMfQ4epuCg5uaaVtE/GRa2u+wBHIoMCGhWQ822VS0T9yo2uecRb5y14cBNSvRKDQPCCgiPA56E0Zvc6gGzg89idyVddPQktOYsxEMVk=
93c38a6a-df30-43a7-9bf1-58bae4c7d0b7	3dcc7eed-6360-4ec1-9aba-d6a261ff3a75	privateKey	MIIEowIBAAKCAQEAtb7xAbx3gp3RLqa0yZOGKUHlsTpB1TNsujRurCEtzwDwFd5LoIE8hMdBiQPIz9YExq7WQ+JDD0hWoDVcFNUZbIqmOlPZA5HyWAs8xybUxIF5dH3l/mzphnv8Gx5sciz6U6BFsQ9kQjbKISws8g1wdNJ8DvX84+JS/sMnx2Kd7Q4EQLceJD4frvPnNXmtHCKrlCgGkW0W/h9A2DfEdK2syggkuwqEo2yZcc7zvZizGNRtzlokXAme1lC8mWS+54jgIHjrjs+LX+Bcg+Dh21XLMihplaZA+zLgnTu7qKF7RiwS+ohdNf2hMB2h/5IKztK5lfRkgP5KA5kjQYErEx9urQIDAQABAoIBAFV/nmQ5M6WcNZBzN0BO6MHW8MXHiLnR59fPG7NZJnIn2EoJWd2Hfp9eF1vsDleLsJKBJGYp9m8lki7q+0cHmt6/eQSedkxiJ7Y4jx6HC3foCul84qomtVMhV4+wZ8bsltSpZK+vdHRbzwn8/KXgIaOrc1YcbZwM2AtmZnu7Tlz6OnhdNBnKYQ6UvZNffOcmFDwuzKePf0wN7GiFFaXZgcCsM6vWV8QeisAjJMfhby8cbyaJdCgk6wGG40LXnbNWrC07Oi4+Ulcrl1ktbcq4Op3Kwmh2qBy+6GYKpGfu6hgnJ7EV6hzca6F27aLjiA9Qbt2WUB+V0r9oph1gWw9XO0cCgYEA+XUW815uNnjmQ2rMp34/YZtOpv2HvQWvNZajQEV9QcgS77dXayYhIxVdOhuTRO1bMr92ogSAbZzFN6MsuiKpSCrDsUYSIbzN7KcFII/1zfi7r7BT6IrUXNE9GeuHZlwb3WAwvTgNbH9O0DijWpCG5/FVw58iX8W9S9ahZAK/2ysCgYEAuoM43jxNgq4m8o2kBdkd2rDlKEYNxh5XVlpqdkjtuDUnOnxxwBx6W3TWJ/56gBi8Xb8CLSfnB0wBJeXMu7x861K/wde1fY2MzQNPOSqK+kFyZ3DGD9rhuZl+HYavxa6c0lINKohxqTwiqbCA2cMPcnOywznq8QcdZ8s1ADCvEYcCgYA8ebZmqa3uZsDr9yhGMsUK1Kvsr+oIWShwxWQuYKpNGEP++WOoBkZiSvue6rB5NVG2q9QG0zCPYkItitF9DoobhMKMZ4BdzXfet1721e+xT2uw2/57kayKE+/MKN1aY1oeAQPP25blhreEozZ27jLGaCtykVG0BiwuBop5xHDeoQKBgQCioqglCZphGHD30lCzlMZ2mwfH3YZHOs/XidIJwMAhf5/sXPVnmq2LME9J4pp5NpiSemtwaroG39ykakTjc5HONYLmia1FhPfBEWC0rYcwf389+dWLn8Zy7AhPwNFm5EL1Fv1KGF5cPwbOxw0kVfYTTBYWLFvUvuqCRmy59LnD4wKBgEpCk1RmtrzdvjbddaWyJGhcbh9NOHeAETgk2ASWpw39WO1F1kkmktdRVR/XGFir08VmoITF5xxyB4+6jUU5nR6hAvD/Al/PIt3rOxrXmyQH28zE8CGaEhANMcmDxB8kKZlcILho7lcLAC8ERDjAVIkr4Jd9xoAWge2WaUlK0sfk
d00e1ecd-35d7-468c-a1f9-e053d9947cbe	9b3dfcca-8de0-4d75-9c04-c3d3cf55c1bd	certificate	MIICmzCCAYMCBgGOzGvG1TANBgkqhkiG9w0BAQsFADARMQ8wDQYDVQQDDAZtYXN0ZXIwHhcNMjQwNDExMDkwODE5WhcNMzQwNDExMDkwOTU5WjARMQ8wDQYDVQQDDAZtYXN0ZXIwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDZ9wp4fcCrT/vNPYbEae0miOFj/cYmxlEyJYai0pcjc4KS0rRvAM5GUJKg0oNYWMLOWXg8uhbSv65U20nvxgLo2ko7rH7WVdHaBsFvldP8vaKCYb2pbWO9uZWn8D5JF4FeAOh08GwO8+hHCy14+6hSib94He0Xijq6Xh7eK8HpCAfZXrjM5phN3jcXNNYfUy71DmMoDwxud/KMAiGUXvbccznd6tlcOGKkFmCvrTmhe320G/tChl86488FmAzVFkS5TxsfRvXh89GF7Jj9tFULY3slJegx57GzmqhkQ3MIUMq64w5yFJI06wxWZYr7NlT9mEsWhhGT7iyRYqMucdJfAgMBAAEwDQYJKoZIhvcNAQELBQADggEBAFzUV1pVVFgCxQAOo5Q+JsNyUDNeB9B+fFUaWG7Oc7UdZ1uwIhWRrUK+O+LHP07SR2y1GtL32TyKo1xCI2/xMXooXJzSAe864SopQ9p8a9Asdfv9H9kH2sxN6q7aJm5mYUCtu/+Vj/xXH3w8OictdN2bxMIim+TZ0QeYytnSSvwbc0OXq2s4D6D202+OCtfG4BOTTyVyoHK6SQ5Wpo+i5Zh7F6UDtgQ8H3YAIQjNvRQ+NlPsttJmbzpVH+q5p0BfT7K+3pGfCq/eV4LU7LMwHjHVLuUpf+9pqYdPwavLE77/2qW7JBsh6XupLnirHv2C9LJbGaVg+zrDtYpDNQgdltg=
1af42788-517c-4f2b-a327-d09f1ca18ef7	9b3dfcca-8de0-4d75-9c04-c3d3cf55c1bd	priority	100
173dbf2d-d55c-4792-8f89-0c016c541213	9b3dfcca-8de0-4d75-9c04-c3d3cf55c1bd	keyUse	ENC
e5d63a44-f986-4ef5-8859-e1f66b2e4d1d	9b3dfcca-8de0-4d75-9c04-c3d3cf55c1bd	algorithm	RSA-OAEP
81b9ce57-5257-4e0f-af65-73106951a0a0	9b3dfcca-8de0-4d75-9c04-c3d3cf55c1bd	privateKey	MIIEpAIBAAKCAQEA2fcKeH3Aq0/7zT2GxGntJojhY/3GJsZRMiWGotKXI3OCktK0bwDORlCSoNKDWFjCzll4PLoW0r+uVNtJ78YC6NpKO6x+1lXR2gbBb5XT/L2igmG9qW1jvbmVp/A+SReBXgDodPBsDvPoRwstePuoUom/eB3tF4o6ul4e3ivB6QgH2V64zOaYTd43FzTWH1Mu9Q5jKA8MbnfyjAIhlF723HM53erZXDhipBZgr605oXt9tBv7QoZfOuPPBZgM1RZEuU8bH0b14fPRheyY/bRVC2N7JSXoMeexs5qoZENzCFDKuuMOchSSNOsMVmWK+zZU/ZhLFoYRk+4skWKjLnHSXwIDAQABAoIBACNlunZr8boV5g45njp6phAFLcQShrdtqz+0wW3i5mGpqicUVJzW/tODf/SMi4EcVR1e1qcsuh/QD6Hp76giulPnfiO9H+dzY35VbNz6x5plj6U99f0Jw1IgorHLKHUFU2CZTtG4Pnbv3HqL0amIBm4fvw+6SHKeDms4ICozkAJlxdIDKTZJruyYI6q466Pa2a5XTjceew9cLKA4LHwPiirlHCCiW23wZoNF7vX9BITWlnhlL9j40kJDFNc95ib3sDHMSPwTn96spjaKg5p4juY/UiyUO9NLgO1OpCiILLmPglDJlRk/8/8k1p8/HZyKM3dbI1cH5jG1Vs3G5Rw7YsECgYEA+HSU2akF/JLLcXvGUBv/c9SpbcBM5QTP4qp3aidb1MsUTq9HXVuxdR6TslGNEaCZkhPiDMx3zNwWWJ6N644KhYa1Jop9Q/pNNLZ9+pKiJKNvUJwqmwoSo6S/7bjydrMdMXN/WBvLqCPqX8+E2/X3k2xkiinDgHxIZztZxyd2XUECgYEA4JVvqdwoat1Lkh32EcUxxXLdSEgdsgGBYpoQsNZyrF/G4As1fAj3OneFXIVvzbZEfoKIeYYSeIcC0U2R1/Fy+nj7HmFvpVWCJqIjjMqxn5qNTSlpzqDOjyzOh/gnVO0ljgXgKDtkb+IF6T0TzsZTt9kxu4JEhg+9x4nDiJB5J58CgYAq0u1VFu0RO7wVM2BbLng5Xs5VPHAEiW/LgdZTVbVeWCKH7W4/iS5tIXgTpDrMbwIJS6QK0LNVfBnd7HZJw5HMsgtHOVed/+aOz2Wnat7HsePd4Qz27CWKTztFnjssenLtsxqf9SqFMKuVGA+uWhbyMhyiM9CetM1F/dTbntGoAQKBgQDUcwjKuoVRcj4G32jjrpNCoUJLGk1dEqd+yxd28aZRAdl0H6Zcy250VJRHzcLR6tYohvrrWrUmZzF3JdYNXU7TOpbzSmD/GkCgo0jV6vjUbrO0hrSczuoX7t8BKPKsGBoqm8xMwsJkRIAq/6g5NswDYub05mpEF6gDloLuMR0rKQKBgQDbNd67JEebc+moPILDYiyRJoJ4n6PSFz/cqASAykBrx5w1z4Kch2qQ3tqFZivc4/pw49YPWICitu9u0zZWTiFGYXnyLrsBfAW0C4+E+/tcp+6l13hMEKEymYhVAqqyxRcyeUZEj9Z7tpzxK64NMfvVmcUKRvvhydHRUKh8FP8asQ==
97e5b279-424e-47b0-b7d5-3fc2eeece210	bd011d5f-75fc-4192-a043-ff6acccdc16b	secret	tsLJABDdXKT63l0CQ6dMLQ
13d5f6d5-5efb-4332-b5bf-85d13f4af2dc	bd011d5f-75fc-4192-a043-ff6acccdc16b	kid	7464b686-70ed-4fce-b6ae-c1e41f9268ba
7b16714b-b1f7-47b3-9b24-007eef9f656b	bd011d5f-75fc-4192-a043-ff6acccdc16b	priority	100
debab664-d795-401e-9d5d-78466b8f7203	5f4fc823-3e0b-4089-9305-235674dc32eb	privateKey	MIIEogIBAAKCAQEAxhZo+MurxMK+8sOPyL3Wz4Ef5jZgx3f0jzjNenZzPidfI5pj/Q1eyrTF4cPSHBgOcx0PEvghXb6VgLmaIyoO6UnbTdSHShujSD+vO8vaph5vYXbX8ZqmuzXkt29XYCKa1kihUvI0w5pAhGiimAaV9oRf96zumMawGIGdAA3826tQiGhPIzULFem6eMxUXZqLwYwvIbDsMIBDEwSY3SkcOZV02MS21yHNa345BIJw/rEZ7IRFZq53I6P9gztZdDvesLsR9qF1Z+AC2WcB2lTgB9GMrdN98IIyBH4sJAIIbsOqa62Gl6SBuWVbOJehckQZ08ozWzlmCKPTLD55AgRxaQIDAQABAoIBAA9HyEY08+WkPjWc51kNZ+Yu6EaKeFG6y6dGsvcY7w/i2qB09vSc1OyfIEqnn+wh4O95VOHaOTyxiHfbGWxQAMF3PI8W727EjgZzPzHGKnI5E895avQMwYLch+2sjZlxYe9Yv/UVkQNrwUFVjwFaNLRFBuAku20WmpvxV6qa6aAKRrqGwGYONCWwmBJwXkUU4rNC8pMuk1u+OXSQkLqVGZgdVFGnfPfTNOm1vBV6ZrXua9F6I/DaLk5Ka6uuZQRNrOHf/wB9y6WecwyGDqz958EJ5wEJrgBMAZ8PW5NGlLhcmXrJKwTLVbFB2Cn/+hDasX+bbSyKtvgHt+pjqiIVVF8CgYEA9HGYi26gpcYxsveULI1f9soPh/uPuiT5iSX13XWrAn/uaoqVhx9CliJV0qUZANl/dtRSFnSiVPxSPJu2TbD8dBWj+4hphhrYmU4XTIWzFldu07zSPdHk7ZaobbUJUJiqxlZaHT2xt5uQikl6xx89GYsp018XFF7A4qzQOlp0NHMCgYEAz3PIp1Ult2AjfrrmIwfYwq9ybq5dBauJtDB0oEE+inwoGCfcdpH16j6RJ6WYxmPggKP77dNUT0x54tOdnuGR8mOL6mBNpsQzTo43uJ6cIRfHxKrJPjJP5Y44DPDZk3S0ToevIG9GPdHL7uTtZtplCw3qZLIh41YlrtcbLA7Q57MCgYB3XOkCXRjxGiC2pZKjVr2fgvemjT6XxOpdAD5Yz9abt+CyAc4WVztqmIWUAK5+7bYCTpdvA7EpErUNh3Oz6FK+VLroXqo4o+9UnP9fjIHwzOz/U9rN+SpyofjI6XgM9R+fDhee47teTwZ7oHnlteleCch4wJxChCpoBb8AX58S5QKBgEMkSy31PfbCs9pBbVirInmoRRYTcuHx6eiawjRXyYESu/tNc2ogxYGh7GLzkqWrD68y48fOXPwpWU2ThfE1OoJDu7YIozk1SRHEr74d/5QqWSqKyAVMTVWTmQp/LTwvSmgcPA74VLiPXTyddUSTrJ5OS615hcRJTnru/cldpjlHAoGAd9w8hjVciBswmIO3Mj79wg8P88ttlgGGZPQY+X52+7pXjZcw/qcgBioYoDh5Fx0F6xmPrJ51BaD5ykYym7t2mnTRsJz1qeO2bWCM0WfU0fEcw2HV+uMfRrA/n8JbQbORZEodz36Gdfi6OFz3ADAQxnvm60JVGqTcgUYEVIZ+xu0=
f7d7db89-f4d0-4789-a488-23e0109a9a8e	5f4fc823-3e0b-4089-9305-235674dc32eb	priority	100
dbbbcf29-19d3-425f-8a1e-1fae18244479	5f4fc823-3e0b-4089-9305-235674dc32eb	certificate	MIICoTCCAYkCBgGOzGvSKjANBgkqhkiG9w0BAQsFADAUMRIwEAYDVQQDDAlmcmFnamV0enQwHhcNMjQwNDExMDkwODIyWhcNMzQwNDExMDkxMDAyWjAUMRIwEAYDVQQDDAlmcmFnamV0enQwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDGFmj4y6vEwr7yw4/IvdbPgR/mNmDHd/SPOM16dnM+J18jmmP9DV7KtMXhw9IcGA5zHQ8S+CFdvpWAuZojKg7pSdtN1IdKG6NIP687y9qmHm9hdtfxmqa7NeS3b1dgIprWSKFS8jTDmkCEaKKYBpX2hF/3rO6YxrAYgZ0ADfzbq1CIaE8jNQsV6bp4zFRdmovBjC8hsOwwgEMTBJjdKRw5lXTYxLbXIc1rfjkEgnD+sRnshEVmrncjo/2DO1l0O96wuxH2oXVn4ALZZwHaVOAH0Yyt033wgjIEfiwkAghuw6prrYaXpIG5ZVs4l6FyRBnTyjNbOWYIo9MsPnkCBHFpAgMBAAEwDQYJKoZIhvcNAQELBQADggEBAELN5Pe3HcA4OIABkKwXUqW7y0NCvZHxduG8TsLBK3AUrZFN6C57mXQuT2zLb6Jh3mpyfYEeep1aEATvJBprtRuUSIz35R7TgT0HxaVzL5UgDFud8aX9PeD0wxdmFgau9W13Mt752rwXW4Q+5BP/ArrzYcgI6SmLdU2tDxQ+D2lGRXW5nvXYe9qUIoNuZ2aVY/9OR8wpevTd9BggC0sAyayqIaTglCKnpfgouddJ4K8T87AH8vLzv0TspuVqFMX6abxpeBqM7BRcTbaS9MI7dZhpAz4zzC2G7OaarKMQsrxPrK0fdQ+AwczOOhWK+7ZEuBclIW+1rcCUIDWglttEOJo=
8d6cfb06-bce4-4664-9ee6-d59f38ad3547	f0f6388b-5681-4667-a763-115bd9882a83	client-uris-must-match	true
06d86576-6dc9-4597-bd12-160c6afa85cc	f0f6388b-5681-4667-a763-115bd9882a83	host-sending-registration-request-must-match	true
f3cca2ec-caf1-48a2-a936-afb8b58bda6e	99942efb-3166-47b1-8a8b-2b1ffc61ea66	max-clients	200
b1c1595c-ca6c-450c-91c3-b633577426f8	5e986667-ad45-44d2-9065-2014ab087e09	privateKey	MIIEowIBAAKCAQEA2V/N8ojKWY5cLOR3LRtHKd0tmB7JNYJw7njYc61PTpfsMpYViL/2Lha1QSSAqL9+Vk8/w87Wr5VZS4MALrTf3d1i+Y/y7ug0/n4A8xtQ7i5b8p1GGebxoXNxHC6146bTtKth1/VonmQdDMXBrqc65LkL2LY8/SSX8f9bc2D4rv+d5m5c6NSlel5MbSJH0t5p8ROAvyRTUoDlpGgVHFRuSP4Stq+NCqsVa4Ac0UxbY5vVp95kNUs8QokPgkAg26Np8aWHAkBCZlDBXDsycg5Q0L+Xe3Svx6BAdOG68pt52a0uBNrY2lx7IhokaaM+w/xrxqP5VGcN4uPwz8up8gpi0QIDAQABAoIBADMhzhgXBUI7soL82uvJVtwKIwd9dmcX3+i4lHHhep5KLk1X8IjSXt5IiSpho8IOCXHUAnPU27tBrleM4SYbde2Cfh2mNsbr6EtU6HRHIKpmTUqspCjDomMP/LNcaXy7aWE+Ty21lH5tnke63OxwefIDdT3s4DvLt9I89w4U6WqAV3EOUtdEze4SpSnhq1OVRGfk0QqiF9Nt+HfcAerE1BTcx0555DIZCIAQftQOzgTFg/MuW05b+J9YJKv5Qq3CDD9/ZIUd7PfGUzmnrBFY3f5e7Q53zwJH8Gc120yL9tWGvFCkymy3774mJMkb8rVoLTUwASi/qaL5wHI60rrO8IECgYEA7MlcM3V4pjN5A9c6t3uAZNR8I/0fPcerOFGt4UjVuL5oOZwwgDKd5ct4kL+GwnOHwIiK++JvxO6AbQK8EQAYBU8B3Q98nSrKJkBASoVMgcmBlC6LhMwJHM/A9L7iSgp9vEnQg+23C5sc+e1OypOjXCAnd94dl1RBup3PBzE15V0CgYEA6wMz10HQBVcX+mUrGPSmjkJM0YqR3P1jGLN//C7IdZzneyAOWZB97oLJQmU9houV/zDS1baBi1VZeR0b7kNamq3e+wtM+gdC75oBdBBHVTfqht08o5MwRQEWotMFhM7Tv3Ukt70uECxrx8qQ1jV7KO93fCK2GA7G0joJTpD6CAUCgYBVzysQovOUF3c1gxwOPj1rTO2oBST7OI6+HCuNRt3c03Pl3MpeyvkZu8D3g9EMSt3ZUiI74fVrQptb5e/NoWETG2yrdd9M/aLAdexWlTQCnMnOr4d0GpT60fc1Oq+68U5bm8xq4wzAsfewQuWY2CQNUMG82MuUeb5QKLxL+PN1KQKBgG0GEE+k/V7s13fW5RZvsLDAOyjeUuqlNgqogxA3l9FQF+b7qQyNxo4WaE1/7/nx2YUIXla8kv5Teu1/voxa/LZTQOVPicdYBQpodJImVyFGjmtASThNORBZ/+lLYf0JghwIuR7wCWWBZgLKi0SEjP0hklmpNLaWbE/skpK7GPMNAoGBAMDM8oxAwM2BYlBgG0rxMDA0mH/KBeS9eWLBedqjQmLQx5wZLVBeKeYufbwD0nASLjPwE/kR97jRxNgsxELqYBzP0+iQB5/nAV9UtDVnmZS3RXe1adCaBKg8ESfehoE7k4FUkBkOIXvXVLqtjCByDMaK7YujHmJOt+IG72AAQ6DT
06690cd2-da6c-4f61-b070-2f53688f920d	5e986667-ad45-44d2-9065-2014ab087e09	certificate	MIICoTCCAYkCBgGOzGvR4zANBgkqhkiG9w0BAQsFADAUMRIwEAYDVQQDDAlmcmFnamV0enQwHhcNMjQwNDExMDkwODIxWhcNMzQwNDExMDkxMDAxWjAUMRIwEAYDVQQDDAlmcmFnamV0enQwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDZX83yiMpZjlws5HctG0cp3S2YHsk1gnDueNhzrU9Ol+wylhWIv/YuFrVBJICov35WTz/DztavlVlLgwAutN/d3WL5j/Lu6DT+fgDzG1DuLlvynUYZ5vGhc3EcLrXjptO0q2HX9WieZB0MxcGupzrkuQvYtjz9JJfx/1tzYPiu/53mblzo1KV6XkxtIkfS3mnxE4C/JFNSgOWkaBUcVG5I/hK2r40KqxVrgBzRTFtjm9Wn3mQ1SzxCiQ+CQCDbo2nxpYcCQEJmUMFcOzJyDlDQv5d7dK/HoEB04brym3nZrS4E2tjaXHsiGiRpoz7D/GvGo/lUZw3i4/DPy6nyCmLRAgMBAAEwDQYJKoZIhvcNAQELBQADggEBADmpGDZdqnu0/zt4NtademVLAsj8wrzt2XrhbFVdBModk/4NEjwc1H6dyq16LeOvO/XWg2r1/HCPNZiN2rKipBwx9Igw6UsrPrIFFtIinTDo50h29RTRin/6luSeK9SZdp9crCVrVE+VStq9gQeyJtBTExe1pRNf8mfBTZ4Pv2Rlc9zct0xv1B3C0ienlYWW06tUrBUYYYzVCBk1YpXvD6N0vhjtvoIIJqMcXH74o+6dE21Q8fgCmCdEG8LhVxf96HFFuSrCsyvwLyVd3vLXTeyqrL46OEZTNxf9MPvU0zNVuyzBUpARSGbVn1eKXETJO5jMvjhgGUOcz7ot0iA4tG0=
fa99c38e-3bca-449c-a0ae-6f2e9055a9e0	5e986667-ad45-44d2-9065-2014ab087e09	algorithm	RSA-OAEP
dfdcc5ce-0667-4f84-b9f6-933a69f83391	5e986667-ad45-44d2-9065-2014ab087e09	priority	100
e9656b21-43e9-4e5a-8efb-062f66f83ebd	9935693f-305a-4d78-8562-792e84441351	allowed-protocol-mapper-types	oidc-usermodel-attribute-mapper
4db605a6-f8b8-4095-b582-eca1c470ffb8	9935693f-305a-4d78-8562-792e84441351	allowed-protocol-mapper-types	oidc-address-mapper
1c25f6f1-2c15-46dc-a041-fc8f8f98a808	9935693f-305a-4d78-8562-792e84441351	allowed-protocol-mapper-types	saml-user-attribute-mapper
09b5d5a7-c99a-4941-9875-f007679f0c23	9935693f-305a-4d78-8562-792e84441351	allowed-protocol-mapper-types	saml-user-property-mapper
db73f88c-d7f5-4c0e-ba27-b0017ddd8f03	9935693f-305a-4d78-8562-792e84441351	allowed-protocol-mapper-types	oidc-usermodel-property-mapper
299d6a3d-512e-43a5-b0b4-fc764069b04e	9935693f-305a-4d78-8562-792e84441351	allowed-protocol-mapper-types	saml-role-list-mapper
c6023fb6-b015-4513-a399-cf07ab6ab15c	9935693f-305a-4d78-8562-792e84441351	allowed-protocol-mapper-types	oidc-sha256-pairwise-sub-mapper
ff928351-8443-4cfb-a70e-2f05d38efc93	9935693f-305a-4d78-8562-792e84441351	allowed-protocol-mapper-types	oidc-full-name-mapper
47628353-4fbc-4db0-b207-8a3db3bfa97a	c51d0c12-5000-4522-b90c-fd153f94c350	allow-default-scopes	true
62a61f36-eef9-4d20-b9ae-cf30995bb3c3	ff48ea3c-5780-44d3-b021-02344d57d139	allowed-protocol-mapper-types	oidc-address-mapper
668c061c-e58c-497a-bcbb-5808393c10bc	ff48ea3c-5780-44d3-b021-02344d57d139	allowed-protocol-mapper-types	oidc-full-name-mapper
ad1670c9-7ba5-479e-ab4b-34fefe09db8d	ff48ea3c-5780-44d3-b021-02344d57d139	allowed-protocol-mapper-types	oidc-sha256-pairwise-sub-mapper
8feeeea9-46a9-42a5-871c-cf8f5967cdde	ff48ea3c-5780-44d3-b021-02344d57d139	allowed-protocol-mapper-types	saml-user-property-mapper
2dd3814f-1687-4e30-a764-7d2ef1e366a8	ff48ea3c-5780-44d3-b021-02344d57d139	allowed-protocol-mapper-types	oidc-usermodel-attribute-mapper
af0135e7-3dc2-4688-b5dc-af8029e28555	ff48ea3c-5780-44d3-b021-02344d57d139	allowed-protocol-mapper-types	oidc-usermodel-property-mapper
202d37ce-0f55-40ec-8235-077d0def3bd4	ff48ea3c-5780-44d3-b021-02344d57d139	allowed-protocol-mapper-types	saml-role-list-mapper
225e998f-2cb4-410c-9c11-45e7552416bb	ff48ea3c-5780-44d3-b021-02344d57d139	allowed-protocol-mapper-types	saml-user-attribute-mapper
078dca5f-7597-4402-bddd-ecb1228dc4d3	9515bc53-c820-4ed5-b67b-4325c372eb35	algorithm	HS256
b705a87f-49a2-413e-825a-9bc294e9d67f	9515bc53-c820-4ed5-b67b-4325c372eb35	priority	100
ccb653be-a66c-473f-b527-9b9640536c84	9515bc53-c820-4ed5-b67b-4325c372eb35	kid	4ad85c89-cbdd-4389-a871-bb1f98d08aea
01eea7e0-0946-448b-81ae-3f33cbb9222a	9515bc53-c820-4ed5-b67b-4325c372eb35	secret	19VQaYH9p2lyfDg_nCt80rp8EG29YK2faJ-2YLZ6rN4Ls9Q1X-CRAoAD3Iv7fl59mWXUAEjippUZ_t9wW6q6AA
2421a13a-5b9d-4455-85e9-59796c574cc6	265f9c0a-5f14-40f9-852d-7653a774a4cf	allow-default-scopes	true
\.


--
-- Data for Name: composite_role; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.composite_role (composite, child_role) FROM stdin;
848d0788-e5d5-4a2c-87fb-911c46cf213a	8ef0b616-bfed-4c02-a49d-df1600429a73
848d0788-e5d5-4a2c-87fb-911c46cf213a	cf872b01-0ca1-4b57-b576-8e988093fbfb
848d0788-e5d5-4a2c-87fb-911c46cf213a	84e8898c-55e5-435d-94bf-0d7e0429ffb4
848d0788-e5d5-4a2c-87fb-911c46cf213a	d9367825-441a-412c-89fc-925b05d92c80
848d0788-e5d5-4a2c-87fb-911c46cf213a	ebf67b26-94bb-435f-aae1-5b682cd3f0aa
848d0788-e5d5-4a2c-87fb-911c46cf213a	e9a541b8-8b14-4496-99df-2ecdf9d787cd
848d0788-e5d5-4a2c-87fb-911c46cf213a	fb4febbc-d44f-4d0b-b082-b49f62093229
848d0788-e5d5-4a2c-87fb-911c46cf213a	1576b99f-e142-42d7-9f33-746f66464b72
848d0788-e5d5-4a2c-87fb-911c46cf213a	4161c244-0640-4339-84df-41c29a1ef9e6
848d0788-e5d5-4a2c-87fb-911c46cf213a	0f0e3b10-bbe2-4c19-8032-4411b5da6221
848d0788-e5d5-4a2c-87fb-911c46cf213a	84cc82bf-3528-4db5-97cc-80ad99064ca5
848d0788-e5d5-4a2c-87fb-911c46cf213a	ea187166-4e89-412a-a53a-42464df16e1e
848d0788-e5d5-4a2c-87fb-911c46cf213a	fdb85dbd-da9a-42bd-b158-eedc4108b767
848d0788-e5d5-4a2c-87fb-911c46cf213a	6ac4bee1-aea8-4eb0-9a3d-27ee6c427f8c
848d0788-e5d5-4a2c-87fb-911c46cf213a	86e75a71-0e62-400b-b28e-d0d2db814e97
848d0788-e5d5-4a2c-87fb-911c46cf213a	66b070db-3b0a-40cb-97fc-8e8272984e41
848d0788-e5d5-4a2c-87fb-911c46cf213a	ec2db440-17c2-4d08-af36-9e543e32a114
848d0788-e5d5-4a2c-87fb-911c46cf213a	58f665f8-1fb8-44fb-ad4c-a8ff9567e425
7f34dbbf-9030-4de5-9c21-c6715f36c748	6a3363e1-6416-4315-b6eb-e50b011dcc81
d9367825-441a-412c-89fc-925b05d92c80	58f665f8-1fb8-44fb-ad4c-a8ff9567e425
d9367825-441a-412c-89fc-925b05d92c80	86e75a71-0e62-400b-b28e-d0d2db814e97
ebf67b26-94bb-435f-aae1-5b682cd3f0aa	66b070db-3b0a-40cb-97fc-8e8272984e41
7f34dbbf-9030-4de5-9c21-c6715f36c748	a27bfda4-4509-454a-b7c4-011054f7bf5e
a27bfda4-4509-454a-b7c4-011054f7bf5e	af8714d9-28a8-4441-b391-3dbad8bb6da4
96162472-6396-4138-93db-3e8b414eb0b9	20c6719e-8e2e-4967-b7ea-1926b574d2a0
848d0788-e5d5-4a2c-87fb-911c46cf213a	bc2a9aeb-733b-4c14-a6e9-a30901a13616
7f34dbbf-9030-4de5-9c21-c6715f36c748	65fbf254-0242-466b-bcb0-4eca469ce313
7f34dbbf-9030-4de5-9c21-c6715f36c748	e814189e-ed04-4edc-af32-780e03ad69fc
848d0788-e5d5-4a2c-87fb-911c46cf213a	dc3a8db3-86ff-43a7-abc2-c9b1482ddda8
848d0788-e5d5-4a2c-87fb-911c46cf213a	1bd85474-fbc3-43d7-9a14-eed87d7dd23d
848d0788-e5d5-4a2c-87fb-911c46cf213a	d01c9ffb-c792-4cda-b8a4-b50598025cf1
848d0788-e5d5-4a2c-87fb-911c46cf213a	c71e0ac9-eff0-4022-a5a4-05c635961d61
848d0788-e5d5-4a2c-87fb-911c46cf213a	869d01b5-4202-4cd9-907d-9bafacb93ab4
848d0788-e5d5-4a2c-87fb-911c46cf213a	8f3f6a6d-ccd3-4fa3-9fc0-b75774cc6e08
848d0788-e5d5-4a2c-87fb-911c46cf213a	e033c0f0-84e9-4419-a1ba-b31055990b0f
848d0788-e5d5-4a2c-87fb-911c46cf213a	68762db6-8943-41c0-abeb-4b86284d6970
848d0788-e5d5-4a2c-87fb-911c46cf213a	0b01e800-4d22-46c5-a3ea-c24885a66614
848d0788-e5d5-4a2c-87fb-911c46cf213a	f14d8562-374f-43ca-abff-ffc2ce80153a
848d0788-e5d5-4a2c-87fb-911c46cf213a	27db64f8-fcb9-4385-ad1f-7b335f1e410b
848d0788-e5d5-4a2c-87fb-911c46cf213a	c4931879-182a-4c40-afc5-1df7dfd38f19
848d0788-e5d5-4a2c-87fb-911c46cf213a	91fa29f2-2baa-405f-8e45-8097fe4c27c5
848d0788-e5d5-4a2c-87fb-911c46cf213a	651cd93b-eb3a-42c9-a642-b3c2303578da
848d0788-e5d5-4a2c-87fb-911c46cf213a	62df278f-d8bd-4317-896d-cbd3ad20dc22
848d0788-e5d5-4a2c-87fb-911c46cf213a	26664dd4-5818-4bdd-a3f1-4d4318536ffe
848d0788-e5d5-4a2c-87fb-911c46cf213a	f64a42d4-c9de-4e79-9da0-ff9d35ec8005
c71e0ac9-eff0-4022-a5a4-05c635961d61	62df278f-d8bd-4317-896d-cbd3ad20dc22
d01c9ffb-c792-4cda-b8a4-b50598025cf1	f64a42d4-c9de-4e79-9da0-ff9d35ec8005
d01c9ffb-c792-4cda-b8a4-b50598025cf1	651cd93b-eb3a-42c9-a642-b3c2303578da
38457439-a532-4111-983d-1c1d2a8d58cb	def7a9a8-1e2c-4c5b-8f9f-26eac8b254aa
38457439-a532-4111-983d-1c1d2a8d58cb	51af6832-8e86-4e8f-837f-a39890eaa5d4
6c2c1357-abd6-402e-925d-b0fe4f60027c	839ea5fa-5ed8-46fc-a86d-129e97ac511b
aa1e1645-5ddc-4219-84fd-42b806510732	e35019e2-a64f-4807-b703-6eaa9ae264eb
aa1e1645-5ddc-4219-84fd-42b806510732	6c2c1357-abd6-402e-925d-b0fe4f60027c
aa1e1645-5ddc-4219-84fd-42b806510732	29899834-863c-4e9d-b1bb-f4643d4d775b
aa1e1645-5ddc-4219-84fd-42b806510732	7543a07e-98d9-46b1-b152-998de5966b3d
e09f1888-a233-436d-b7d1-cf52257aeada	8652c9da-03d0-46f2-a70e-8206ef32423b
f188fd2a-13d4-41e0-84c2-830de8f6517d	21e867b1-b1c0-4d44-a0c6-46d9746bc286
face08e1-9944-4050-84a5-3d788bf4eab6	13501f0e-822a-43b8-bac4-32f1f10f6b6e
face08e1-9944-4050-84a5-3d788bf4eab6	51af6832-8e86-4e8f-837f-a39890eaa5d4
face08e1-9944-4050-84a5-3d788bf4eab6	79f928c2-9f1f-4a7d-bd8d-a4582162d73e
face08e1-9944-4050-84a5-3d788bf4eab6	d4aadffa-cbe9-4a1a-af19-834708456189
face08e1-9944-4050-84a5-3d788bf4eab6	331858f6-63d1-4561-b179-288850e6b45d
face08e1-9944-4050-84a5-3d788bf4eab6	277be2b7-cade-4293-9f23-205f840841b4
face08e1-9944-4050-84a5-3d788bf4eab6	38457439-a532-4111-983d-1c1d2a8d58cb
face08e1-9944-4050-84a5-3d788bf4eab6	def7a9a8-1e2c-4c5b-8f9f-26eac8b254aa
face08e1-9944-4050-84a5-3d788bf4eab6	88d9839b-2028-4691-8164-9e86fe647966
face08e1-9944-4050-84a5-3d788bf4eab6	d758d353-8498-409e-a012-3a3bec44f52b
face08e1-9944-4050-84a5-3d788bf4eab6	f188fd2a-13d4-41e0-84c2-830de8f6517d
face08e1-9944-4050-84a5-3d788bf4eab6	225739d6-a58c-4224-a69a-00c4fd0a8da4
face08e1-9944-4050-84a5-3d788bf4eab6	5e08f1d3-2f86-41ad-b0b5-bc1a7850f7fe
face08e1-9944-4050-84a5-3d788bf4eab6	21e867b1-b1c0-4d44-a0c6-46d9746bc286
face08e1-9944-4050-84a5-3d788bf4eab6	3606dd17-8278-4d90-a075-63dee7f8ea56
face08e1-9944-4050-84a5-3d788bf4eab6	32861b3a-0d78-4fe3-8f54-3235f2c10631
face08e1-9944-4050-84a5-3d788bf4eab6	5a6f5ad5-53d6-4b54-b170-f90facc8fd2a
face08e1-9944-4050-84a5-3d788bf4eab6	32c50071-a52c-46c9-9362-6bafc76a0ba4
848d0788-e5d5-4a2c-87fb-911c46cf213a	559f1471-9613-4e45-a508-d06ec62d1946
\.


--
-- Data for Name: credential; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.credential (id, salt, type, user_id, created_date, user_label, secret_data, credential_data, priority) FROM stdin;
4857d585-0268-4bcf-aa0f-a40959efae86	\N	password	4ee0ebb9-ab98-4fe7-bfe1-d44727a39c4f	1712826603456	\N	{"value":"C0DoQTPgZNmGfQPfPlgqIONU5uEq5cONFycKQWH/LLo=","salt":"NGdnIIA50PoZ01UDweR+2A==","additionalParameters":{}}	{"hashIterations":27500,"algorithm":"pbkdf2-sha256","additionalParameters":{}}	10
31e8a311-8ea9-4543-8100-919c97a55d6e	\N	password	a82979e2-0863-4dea-8055-b3b93803c90b	99999999999999	\N	{"value":"VcbqSytEZDs4Tcq0YwNa0Gg2kjbuE/sLG7LHzs3bHNY=","salt":"/GYfen05sguwtptrv8L0PA==","additionalParameters":{}}	{"hashIterations":27500,"algorithm":"pbkdf2-sha256","additionalParameters":{}}	10
\.


--
-- Data for Name: databasechangelog; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.databasechangelog (id, author, filename, dateexecuted, orderexecuted, exectype, md5sum, description, comments, tag, liquibase, contexts, labels, deployment_id) FROM stdin;
1.0.0.Final-KEYCLOAK-5461	sthorger@redhat.com	META-INF/jpa-changelog-1.0.0.Final.xml	2024-04-11 09:09:55.056484	1	EXECUTED	8:bda77d94bf90182a1e30c24f1c155ec7	createTable tableName=APPLICATION_DEFAULT_ROLES; createTable tableName=CLIENT; createTable tableName=CLIENT_SESSION; createTable tableName=CLIENT_SESSION_ROLE; createTable tableName=COMPOSITE_ROLE; createTable tableName=CREDENTIAL; createTable tab...		\N	4.20.0	\N	\N	2826594441
1.0.0.Final-KEYCLOAK-5461	sthorger@redhat.com	META-INF/db2-jpa-changelog-1.0.0.Final.xml	2024-04-11 09:09:55.062114	2	MARK_RAN	8:1ecb330f30986693d1cba9ab579fa219	createTable tableName=APPLICATION_DEFAULT_ROLES; createTable tableName=CLIENT; createTable tableName=CLIENT_SESSION; createTable tableName=CLIENT_SESSION_ROLE; createTable tableName=COMPOSITE_ROLE; createTable tableName=CREDENTIAL; createTable tab...		\N	4.20.0	\N	\N	2826594441
1.1.0.Beta1	sthorger@redhat.com	META-INF/jpa-changelog-1.1.0.Beta1.xml	2024-04-11 09:09:55.120694	3	EXECUTED	8:cb7ace19bc6d959f305605d255d4c843	delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION; createTable tableName=CLIENT_ATTRIBUTES; createTable tableName=CLIENT_SESSION_NOTE; createTable tableName=APP_NODE_REGISTRATIONS; addColumn table...		\N	4.20.0	\N	\N	2826594441
1.1.0.Final	sthorger@redhat.com	META-INF/jpa-changelog-1.1.0.Final.xml	2024-04-11 09:09:55.125315	4	EXECUTED	8:80230013e961310e6872e871be424a63	renameColumn newColumnName=EVENT_TIME, oldColumnName=TIME, tableName=EVENT_ENTITY		\N	4.20.0	\N	\N	2826594441
1.2.0.Beta1	psilva@redhat.com	META-INF/jpa-changelog-1.2.0.Beta1.xml	2024-04-11 09:09:55.272589	5	EXECUTED	8:67f4c20929126adc0c8e9bf48279d244	delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION; createTable tableName=PROTOCOL_MAPPER; createTable tableName=PROTOCOL_MAPPER_CONFIG; createTable tableName=...		\N	4.20.0	\N	\N	2826594441
1.2.0.Beta1	psilva@redhat.com	META-INF/db2-jpa-changelog-1.2.0.Beta1.xml	2024-04-11 09:09:55.274948	6	MARK_RAN	8:7311018b0b8179ce14628ab412bb6783	delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION; createTable tableName=PROTOCOL_MAPPER; createTable tableName=PROTOCOL_MAPPER_CONFIG; createTable tableName=...		\N	4.20.0	\N	\N	2826594441
1.2.0.RC1	bburke@redhat.com	META-INF/jpa-changelog-1.2.0.CR1.xml	2024-04-11 09:09:55.378781	7	EXECUTED	8:037ba1216c3640f8785ee6b8e7c8e3c1	delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete tableName=USER_SESSION; createTable tableName=MIGRATION_MODEL; createTable tableName=IDENTITY_P...		\N	4.20.0	\N	\N	2826594441
1.2.0.RC1	bburke@redhat.com	META-INF/db2-jpa-changelog-1.2.0.CR1.xml	2024-04-11 09:09:55.380724	8	MARK_RAN	8:7fe6ffe4af4df289b3157de32c624263	delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete tableName=USER_SESSION; createTable tableName=MIGRATION_MODEL; createTable tableName=IDENTITY_P...		\N	4.20.0	\N	\N	2826594441
1.2.0.Final	keycloak	META-INF/jpa-changelog-1.2.0.Final.xml	2024-04-11 09:09:55.388184	9	EXECUTED	8:9c136bc3187083a98745c7d03bc8a303	update tableName=CLIENT; update tableName=CLIENT; update tableName=CLIENT		\N	4.20.0	\N	\N	2826594441
1.3.0	bburke@redhat.com	META-INF/jpa-changelog-1.3.0.xml	2024-04-11 09:09:55.4919	10	EXECUTED	8:b5f09474dca81fb56a97cf5b6553d331	delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_PROT_MAPPER; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete tableName=USER_SESSION; createTable tableName=ADMI...		\N	4.20.0	\N	\N	2826594441
1.4.0	bburke@redhat.com	META-INF/jpa-changelog-1.4.0.xml	2024-04-11 09:09:55.553313	11	EXECUTED	8:ca924f31bd2a3b219fdcfe78c82dacf4	delete tableName=CLIENT_SESSION_AUTH_STATUS; delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_PROT_MAPPER; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete table...		\N	4.20.0	\N	\N	2826594441
1.4.0	bburke@redhat.com	META-INF/db2-jpa-changelog-1.4.0.xml	2024-04-11 09:09:55.55522	12	MARK_RAN	8:8acad7483e106416bcfa6f3b824a16cd	delete tableName=CLIENT_SESSION_AUTH_STATUS; delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_PROT_MAPPER; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete table...		\N	4.20.0	\N	\N	2826594441
1.5.0	bburke@redhat.com	META-INF/jpa-changelog-1.5.0.xml	2024-04-11 09:09:55.580089	13	EXECUTED	8:9b1266d17f4f87c78226f5055408fd5e	delete tableName=CLIENT_SESSION_AUTH_STATUS; delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_PROT_MAPPER; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete table...		\N	4.20.0	\N	\N	2826594441
1.6.1_from15	mposolda@redhat.com	META-INF/jpa-changelog-1.6.1.xml	2024-04-11 09:09:55.603184	14	EXECUTED	8:d80ec4ab6dbfe573550ff72396c7e910	addColumn tableName=REALM; addColumn tableName=KEYCLOAK_ROLE; addColumn tableName=CLIENT; createTable tableName=OFFLINE_USER_SESSION; createTable tableName=OFFLINE_CLIENT_SESSION; addPrimaryKey constraintName=CONSTRAINT_OFFL_US_SES_PK2, tableName=...		\N	4.20.0	\N	\N	2826594441
1.6.1_from16-pre	mposolda@redhat.com	META-INF/jpa-changelog-1.6.1.xml	2024-04-11 09:09:55.605131	15	MARK_RAN	8:d86eb172171e7c20b9c849b584d147b2	delete tableName=OFFLINE_CLIENT_SESSION; delete tableName=OFFLINE_USER_SESSION		\N	4.20.0	\N	\N	2826594441
1.6.1_from16	mposolda@redhat.com	META-INF/jpa-changelog-1.6.1.xml	2024-04-11 09:09:55.607562	16	MARK_RAN	8:5735f46f0fa60689deb0ecdc2a0dea22	dropPrimaryKey constraintName=CONSTRAINT_OFFLINE_US_SES_PK, tableName=OFFLINE_USER_SESSION; dropPrimaryKey constraintName=CONSTRAINT_OFFLINE_CL_SES_PK, tableName=OFFLINE_CLIENT_SESSION; addColumn tableName=OFFLINE_USER_SESSION; update tableName=OF...		\N	4.20.0	\N	\N	2826594441
1.6.1	mposolda@redhat.com	META-INF/jpa-changelog-1.6.1.xml	2024-04-11 09:09:55.610696	17	EXECUTED	8:d41d8cd98f00b204e9800998ecf8427e	empty		\N	4.20.0	\N	\N	2826594441
1.7.0	bburke@redhat.com	META-INF/jpa-changelog-1.7.0.xml	2024-04-11 09:09:55.668337	18	EXECUTED	8:5c1a8fd2014ac7fc43b90a700f117b23	createTable tableName=KEYCLOAK_GROUP; createTable tableName=GROUP_ROLE_MAPPING; createTable tableName=GROUP_ATTRIBUTE; createTable tableName=USER_GROUP_MEMBERSHIP; createTable tableName=REALM_DEFAULT_GROUPS; addColumn tableName=IDENTITY_PROVIDER; ...		\N	4.20.0	\N	\N	2826594441
1.8.0	mposolda@redhat.com	META-INF/jpa-changelog-1.8.0.xml	2024-04-11 09:09:55.723486	19	EXECUTED	8:1f6c2c2dfc362aff4ed75b3f0ef6b331	addColumn tableName=IDENTITY_PROVIDER; createTable tableName=CLIENT_TEMPLATE; createTable tableName=CLIENT_TEMPLATE_ATTRIBUTES; createTable tableName=TEMPLATE_SCOPE_MAPPING; dropNotNullConstraint columnName=CLIENT_ID, tableName=PROTOCOL_MAPPER; ad...		\N	4.20.0	\N	\N	2826594441
1.8.0-2	keycloak	META-INF/jpa-changelog-1.8.0.xml	2024-04-11 09:09:55.728712	20	EXECUTED	8:dee9246280915712591f83a127665107	dropDefaultValue columnName=ALGORITHM, tableName=CREDENTIAL; update tableName=CREDENTIAL		\N	4.20.0	\N	\N	2826594441
1.8.0	mposolda@redhat.com	META-INF/db2-jpa-changelog-1.8.0.xml	2024-04-11 09:09:55.73023	21	MARK_RAN	8:9eb2ee1fa8ad1c5e426421a6f8fdfa6a	addColumn tableName=IDENTITY_PROVIDER; createTable tableName=CLIENT_TEMPLATE; createTable tableName=CLIENT_TEMPLATE_ATTRIBUTES; createTable tableName=TEMPLATE_SCOPE_MAPPING; dropNotNullConstraint columnName=CLIENT_ID, tableName=PROTOCOL_MAPPER; ad...		\N	4.20.0	\N	\N	2826594441
1.8.0-2	keycloak	META-INF/db2-jpa-changelog-1.8.0.xml	2024-04-11 09:09:55.732741	22	MARK_RAN	8:dee9246280915712591f83a127665107	dropDefaultValue columnName=ALGORITHM, tableName=CREDENTIAL; update tableName=CREDENTIAL		\N	4.20.0	\N	\N	2826594441
1.9.0	mposolda@redhat.com	META-INF/jpa-changelog-1.9.0.xml	2024-04-11 09:09:55.774369	23	EXECUTED	8:d9fa18ffa355320395b86270680dd4fe	update tableName=REALM; update tableName=REALM; update tableName=REALM; update tableName=REALM; update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=REALM; update tableName=REALM; customChange; dr...		\N	4.20.0	\N	\N	2826594441
1.9.1	keycloak	META-INF/jpa-changelog-1.9.1.xml	2024-04-11 09:09:55.781057	24	EXECUTED	8:90cff506fedb06141ffc1c71c4a1214c	modifyDataType columnName=PRIVATE_KEY, tableName=REALM; modifyDataType columnName=PUBLIC_KEY, tableName=REALM; modifyDataType columnName=CERTIFICATE, tableName=REALM		\N	4.20.0	\N	\N	2826594441
1.9.1	keycloak	META-INF/db2-jpa-changelog-1.9.1.xml	2024-04-11 09:09:55.782643	25	MARK_RAN	8:11a788aed4961d6d29c427c063af828c	modifyDataType columnName=PRIVATE_KEY, tableName=REALM; modifyDataType columnName=CERTIFICATE, tableName=REALM		\N	4.20.0	\N	\N	2826594441
1.9.2	keycloak	META-INF/jpa-changelog-1.9.2.xml	2024-04-11 09:09:55.813291	26	EXECUTED	8:a4218e51e1faf380518cce2af5d39b43	createIndex indexName=IDX_USER_EMAIL, tableName=USER_ENTITY; createIndex indexName=IDX_USER_ROLE_MAPPING, tableName=USER_ROLE_MAPPING; createIndex indexName=IDX_USER_GROUP_MAPPING, tableName=USER_GROUP_MEMBERSHIP; createIndex indexName=IDX_USER_CO...		\N	4.20.0	\N	\N	2826594441
authz-2.0.0	psilva@redhat.com	META-INF/jpa-changelog-authz-2.0.0.xml	2024-04-11 09:09:55.899567	27	EXECUTED	8:d9e9a1bfaa644da9952456050f07bbdc	createTable tableName=RESOURCE_SERVER; addPrimaryKey constraintName=CONSTRAINT_FARS, tableName=RESOURCE_SERVER; addUniqueConstraint constraintName=UK_AU8TT6T700S9V50BU18WS5HA6, tableName=RESOURCE_SERVER; createTable tableName=RESOURCE_SERVER_RESOU...		\N	4.20.0	\N	\N	2826594441
authz-2.5.1	psilva@redhat.com	META-INF/jpa-changelog-authz-2.5.1.xml	2024-04-11 09:09:55.904546	28	EXECUTED	8:d1bf991a6163c0acbfe664b615314505	update tableName=RESOURCE_SERVER_POLICY		\N	4.20.0	\N	\N	2826594441
2.1.0-KEYCLOAK-5461	bburke@redhat.com	META-INF/jpa-changelog-2.1.0.xml	2024-04-11 09:09:55.971411	29	EXECUTED	8:88a743a1e87ec5e30bf603da68058a8c	createTable tableName=BROKER_LINK; createTable tableName=FED_USER_ATTRIBUTE; createTable tableName=FED_USER_CONSENT; createTable tableName=FED_USER_CONSENT_ROLE; createTable tableName=FED_USER_CONSENT_PROT_MAPPER; createTable tableName=FED_USER_CR...		\N	4.20.0	\N	\N	2826594441
2.2.0	bburke@redhat.com	META-INF/jpa-changelog-2.2.0.xml	2024-04-11 09:09:55.986417	30	EXECUTED	8:c5517863c875d325dea463d00ec26d7a	addColumn tableName=ADMIN_EVENT_ENTITY; createTable tableName=CREDENTIAL_ATTRIBUTE; createTable tableName=FED_CREDENTIAL_ATTRIBUTE; modifyDataType columnName=VALUE, tableName=CREDENTIAL; addForeignKeyConstraint baseTableName=FED_CREDENTIAL_ATTRIBU...		\N	4.20.0	\N	\N	2826594441
2.3.0	bburke@redhat.com	META-INF/jpa-changelog-2.3.0.xml	2024-04-11 09:09:56.007991	31	EXECUTED	8:ada8b4833b74a498f376d7136bc7d327	createTable tableName=FEDERATED_USER; addPrimaryKey constraintName=CONSTR_FEDERATED_USER, tableName=FEDERATED_USER; dropDefaultValue columnName=TOTP, tableName=USER_ENTITY; dropColumn columnName=TOTP, tableName=USER_ENTITY; addColumn tableName=IDE...		\N	4.20.0	\N	\N	2826594441
2.4.0	bburke@redhat.com	META-INF/jpa-changelog-2.4.0.xml	2024-04-11 09:09:56.016043	32	EXECUTED	8:b9b73c8ea7299457f99fcbb825c263ba	customChange		\N	4.20.0	\N	\N	2826594441
2.5.0	bburke@redhat.com	META-INF/jpa-changelog-2.5.0.xml	2024-04-11 09:09:56.024708	33	EXECUTED	8:07724333e625ccfcfc5adc63d57314f3	customChange; modifyDataType columnName=USER_ID, tableName=OFFLINE_USER_SESSION		\N	4.20.0	\N	\N	2826594441
2.5.0-unicode-oracle	hmlnarik@redhat.com	META-INF/jpa-changelog-2.5.0.xml	2024-04-11 09:09:56.026296	34	MARK_RAN	8:8b6fd445958882efe55deb26fc541a7b	modifyDataType columnName=DESCRIPTION, tableName=AUTHENTICATION_FLOW; modifyDataType columnName=DESCRIPTION, tableName=CLIENT_TEMPLATE; modifyDataType columnName=DESCRIPTION, tableName=RESOURCE_SERVER_POLICY; modifyDataType columnName=DESCRIPTION,...		\N	4.20.0	\N	\N	2826594441
2.5.0-unicode-other-dbs	hmlnarik@redhat.com	META-INF/jpa-changelog-2.5.0.xml	2024-04-11 09:09:56.060236	35	EXECUTED	8:29b29cfebfd12600897680147277a9d7	modifyDataType columnName=DESCRIPTION, tableName=AUTHENTICATION_FLOW; modifyDataType columnName=DESCRIPTION, tableName=CLIENT_TEMPLATE; modifyDataType columnName=DESCRIPTION, tableName=RESOURCE_SERVER_POLICY; modifyDataType columnName=DESCRIPTION,...		\N	4.20.0	\N	\N	2826594441
2.5.0-duplicate-email-support	slawomir@dabek.name	META-INF/jpa-changelog-2.5.0.xml	2024-04-11 09:09:56.065746	36	EXECUTED	8:73ad77ca8fd0410c7f9f15a471fa52bc	addColumn tableName=REALM		\N	4.20.0	\N	\N	2826594441
2.5.0-unique-group-names	hmlnarik@redhat.com	META-INF/jpa-changelog-2.5.0.xml	2024-04-11 09:09:56.071625	37	EXECUTED	8:64f27a6fdcad57f6f9153210f2ec1bdb	addUniqueConstraint constraintName=SIBLING_NAMES, tableName=KEYCLOAK_GROUP		\N	4.20.0	\N	\N	2826594441
2.5.1	bburke@redhat.com	META-INF/jpa-changelog-2.5.1.xml	2024-04-11 09:09:56.075888	38	EXECUTED	8:27180251182e6c31846c2ddab4bc5781	addColumn tableName=FED_USER_CONSENT		\N	4.20.0	\N	\N	2826594441
3.0.0	bburke@redhat.com	META-INF/jpa-changelog-3.0.0.xml	2024-04-11 09:09:56.079929	39	EXECUTED	8:d56f201bfcfa7a1413eb3e9bc02978f9	addColumn tableName=IDENTITY_PROVIDER		\N	4.20.0	\N	\N	2826594441
3.2.0-fix	keycloak	META-INF/jpa-changelog-3.2.0.xml	2024-04-11 09:09:56.081738	40	MARK_RAN	8:91f5522bf6afdc2077dfab57fbd3455c	addNotNullConstraint columnName=REALM_ID, tableName=CLIENT_INITIAL_ACCESS		\N	4.20.0	\N	\N	2826594441
3.2.0-fix-with-keycloak-5416	keycloak	META-INF/jpa-changelog-3.2.0.xml	2024-04-11 09:09:56.084247	41	MARK_RAN	8:0f01b554f256c22caeb7d8aee3a1cdc8	dropIndex indexName=IDX_CLIENT_INIT_ACC_REALM, tableName=CLIENT_INITIAL_ACCESS; addNotNullConstraint columnName=REALM_ID, tableName=CLIENT_INITIAL_ACCESS; createIndex indexName=IDX_CLIENT_INIT_ACC_REALM, tableName=CLIENT_INITIAL_ACCESS		\N	4.20.0	\N	\N	2826594441
3.2.0-fix-offline-sessions	hmlnarik	META-INF/jpa-changelog-3.2.0.xml	2024-04-11 09:09:56.093917	42	EXECUTED	8:ab91cf9cee415867ade0e2df9651a947	customChange		\N	4.20.0	\N	\N	2826594441
3.2.0-fixed	keycloak	META-INF/jpa-changelog-3.2.0.xml	2024-04-11 09:09:56.212996	43	EXECUTED	8:ceac9b1889e97d602caf373eadb0d4b7	addColumn tableName=REALM; dropPrimaryKey constraintName=CONSTRAINT_OFFL_CL_SES_PK2, tableName=OFFLINE_CLIENT_SESSION; dropColumn columnName=CLIENT_SESSION_ID, tableName=OFFLINE_CLIENT_SESSION; addPrimaryKey constraintName=CONSTRAINT_OFFL_CL_SES_P...		\N	4.20.0	\N	\N	2826594441
3.3.0	keycloak	META-INF/jpa-changelog-3.3.0.xml	2024-04-11 09:09:56.217131	44	EXECUTED	8:84b986e628fe8f7fd8fd3c275c5259f2	addColumn tableName=USER_ENTITY		\N	4.20.0	\N	\N	2826594441
authz-3.4.0.CR1-resource-server-pk-change-part1	glavoie@gmail.com	META-INF/jpa-changelog-authz-3.4.0.CR1.xml	2024-04-11 09:09:56.222293	45	EXECUTED	8:a164ae073c56ffdbc98a615493609a52	addColumn tableName=RESOURCE_SERVER_POLICY; addColumn tableName=RESOURCE_SERVER_RESOURCE; addColumn tableName=RESOURCE_SERVER_SCOPE		\N	4.20.0	\N	\N	2826594441
authz-3.4.0.CR1-resource-server-pk-change-part2-KEYCLOAK-6095	hmlnarik@redhat.com	META-INF/jpa-changelog-authz-3.4.0.CR1.xml	2024-04-11 09:09:56.229527	46	EXECUTED	8:70a2b4f1f4bd4dbf487114bdb1810e64	customChange		\N	4.20.0	\N	\N	2826594441
authz-3.4.0.CR1-resource-server-pk-change-part3-fixed	glavoie@gmail.com	META-INF/jpa-changelog-authz-3.4.0.CR1.xml	2024-04-11 09:09:56.231069	47	MARK_RAN	8:7be68b71d2f5b94b8df2e824f2860fa2	dropIndex indexName=IDX_RES_SERV_POL_RES_SERV, tableName=RESOURCE_SERVER_POLICY; dropIndex indexName=IDX_RES_SRV_RES_RES_SRV, tableName=RESOURCE_SERVER_RESOURCE; dropIndex indexName=IDX_RES_SRV_SCOPE_RES_SRV, tableName=RESOURCE_SERVER_SCOPE		\N	4.20.0	\N	\N	2826594441
authz-3.4.0.CR1-resource-server-pk-change-part3-fixed-nodropindex	glavoie@gmail.com	META-INF/jpa-changelog-authz-3.4.0.CR1.xml	2024-04-11 09:09:56.279443	48	EXECUTED	8:bab7c631093c3861d6cf6144cd944982	addNotNullConstraint columnName=RESOURCE_SERVER_CLIENT_ID, tableName=RESOURCE_SERVER_POLICY; addNotNullConstraint columnName=RESOURCE_SERVER_CLIENT_ID, tableName=RESOURCE_SERVER_RESOURCE; addNotNullConstraint columnName=RESOURCE_SERVER_CLIENT_ID, ...		\N	4.20.0	\N	\N	2826594441
authn-3.4.0.CR1-refresh-token-max-reuse	glavoie@gmail.com	META-INF/jpa-changelog-authz-3.4.0.CR1.xml	2024-04-11 09:09:56.284412	49	EXECUTED	8:fa809ac11877d74d76fe40869916daad	addColumn tableName=REALM		\N	4.20.0	\N	\N	2826594441
3.4.0	keycloak	META-INF/jpa-changelog-3.4.0.xml	2024-04-11 09:09:56.327463	50	EXECUTED	8:fac23540a40208f5f5e326f6ceb4d291	addPrimaryKey constraintName=CONSTRAINT_REALM_DEFAULT_ROLES, tableName=REALM_DEFAULT_ROLES; addPrimaryKey constraintName=CONSTRAINT_COMPOSITE_ROLE, tableName=COMPOSITE_ROLE; addPrimaryKey constraintName=CONSTR_REALM_DEFAULT_GROUPS, tableName=REALM...		\N	4.20.0	\N	\N	2826594441
3.4.0-KEYCLOAK-5230	hmlnarik@redhat.com	META-INF/jpa-changelog-3.4.0.xml	2024-04-11 09:09:56.355315	51	EXECUTED	8:2612d1b8a97e2b5588c346e817307593	createIndex indexName=IDX_FU_ATTRIBUTE, tableName=FED_USER_ATTRIBUTE; createIndex indexName=IDX_FU_CONSENT, tableName=FED_USER_CONSENT; createIndex indexName=IDX_FU_CONSENT_RU, tableName=FED_USER_CONSENT; createIndex indexName=IDX_FU_CREDENTIAL, t...		\N	4.20.0	\N	\N	2826594441
3.4.1	psilva@redhat.com	META-INF/jpa-changelog-3.4.1.xml	2024-04-11 09:09:56.358979	52	EXECUTED	8:9842f155c5db2206c88bcb5d1046e941	modifyDataType columnName=VALUE, tableName=CLIENT_ATTRIBUTES		\N	4.20.0	\N	\N	2826594441
3.4.2	keycloak	META-INF/jpa-changelog-3.4.2.xml	2024-04-11 09:09:56.362221	53	EXECUTED	8:2e12e06e45498406db72d5b3da5bbc76	update tableName=REALM		\N	4.20.0	\N	\N	2826594441
3.4.2-KEYCLOAK-5172	mkanis@redhat.com	META-INF/jpa-changelog-3.4.2.xml	2024-04-11 09:09:56.366256	54	EXECUTED	8:33560e7c7989250c40da3abdabdc75a4	update tableName=CLIENT		\N	4.20.0	\N	\N	2826594441
4.0.0-KEYCLOAK-6335	bburke@redhat.com	META-INF/jpa-changelog-4.0.0.xml	2024-04-11 09:09:56.37256	55	EXECUTED	8:87a8d8542046817a9107c7eb9cbad1cd	createTable tableName=CLIENT_AUTH_FLOW_BINDINGS; addPrimaryKey constraintName=C_CLI_FLOW_BIND, tableName=CLIENT_AUTH_FLOW_BINDINGS		\N	4.20.0	\N	\N	2826594441
4.0.0-CLEANUP-UNUSED-TABLE	bburke@redhat.com	META-INF/jpa-changelog-4.0.0.xml	2024-04-11 09:09:56.380694	56	EXECUTED	8:3ea08490a70215ed0088c273d776311e	dropTable tableName=CLIENT_IDENTITY_PROV_MAPPING		\N	4.20.0	\N	\N	2826594441
4.0.0-KEYCLOAK-6228	bburke@redhat.com	META-INF/jpa-changelog-4.0.0.xml	2024-04-11 09:09:56.401601	57	EXECUTED	8:2d56697c8723d4592ab608ce14b6ed68	dropUniqueConstraint constraintName=UK_JKUWUVD56ONTGSUHOGM8UEWRT, tableName=USER_CONSENT; dropNotNullConstraint columnName=CLIENT_ID, tableName=USER_CONSENT; addColumn tableName=USER_CONSENT; addUniqueConstraint constraintName=UK_JKUWUVD56ONTGSUHO...		\N	4.20.0	\N	\N	2826594441
4.0.0-KEYCLOAK-5579-fixed	mposolda@redhat.com	META-INF/jpa-changelog-4.0.0.xml	2024-04-11 09:09:56.507254	58	EXECUTED	8:3e423e249f6068ea2bbe48bf907f9d86	dropForeignKeyConstraint baseTableName=CLIENT_TEMPLATE_ATTRIBUTES, constraintName=FK_CL_TEMPL_ATTR_TEMPL; renameTable newTableName=CLIENT_SCOPE_ATTRIBUTES, oldTableName=CLIENT_TEMPLATE_ATTRIBUTES; renameColumn newColumnName=SCOPE_ID, oldColumnName...		\N	4.20.0	\N	\N	2826594441
authz-4.0.0.CR1	psilva@redhat.com	META-INF/jpa-changelog-authz-4.0.0.CR1.xml	2024-04-11 09:09:56.533239	59	EXECUTED	8:15cabee5e5df0ff099510a0fc03e4103	createTable tableName=RESOURCE_SERVER_PERM_TICKET; addPrimaryKey constraintName=CONSTRAINT_FAPMT, tableName=RESOURCE_SERVER_PERM_TICKET; addForeignKeyConstraint baseTableName=RESOURCE_SERVER_PERM_TICKET, constraintName=FK_FRSRHO213XCX4WNKOG82SSPMT...		\N	4.20.0	\N	\N	2826594441
authz-4.0.0.Beta3	psilva@redhat.com	META-INF/jpa-changelog-authz-4.0.0.Beta3.xml	2024-04-11 09:09:56.539106	60	EXECUTED	8:4b80200af916ac54d2ffbfc47918ab0e	addColumn tableName=RESOURCE_SERVER_POLICY; addColumn tableName=RESOURCE_SERVER_PERM_TICKET; addForeignKeyConstraint baseTableName=RESOURCE_SERVER_PERM_TICKET, constraintName=FK_FRSRPO2128CX4WNKOG82SSRFY, referencedTableName=RESOURCE_SERVER_POLICY		\N	4.20.0	\N	\N	2826594441
authz-4.2.0.Final	mhajas@redhat.com	META-INF/jpa-changelog-authz-4.2.0.Final.xml	2024-04-11 09:09:56.552344	61	EXECUTED	8:66564cd5e168045d52252c5027485bbb	createTable tableName=RESOURCE_URIS; addForeignKeyConstraint baseTableName=RESOURCE_URIS, constraintName=FK_RESOURCE_SERVER_URIS, referencedTableName=RESOURCE_SERVER_RESOURCE; customChange; dropColumn columnName=URI, tableName=RESOURCE_SERVER_RESO...		\N	4.20.0	\N	\N	2826594441
authz-4.2.0.Final-KEYCLOAK-9944	hmlnarik@redhat.com	META-INF/jpa-changelog-authz-4.2.0.Final.xml	2024-04-11 09:09:56.558276	62	EXECUTED	8:1c7064fafb030222be2bd16ccf690f6f	addPrimaryKey constraintName=CONSTRAINT_RESOUR_URIS_PK, tableName=RESOURCE_URIS		\N	4.20.0	\N	\N	2826594441
4.2.0-KEYCLOAK-6313	wadahiro@gmail.com	META-INF/jpa-changelog-4.2.0.xml	2024-04-11 09:09:56.561826	63	EXECUTED	8:2de18a0dce10cdda5c7e65c9b719b6e5	addColumn tableName=REQUIRED_ACTION_PROVIDER		\N	4.20.0	\N	\N	2826594441
4.3.0-KEYCLOAK-7984	wadahiro@gmail.com	META-INF/jpa-changelog-4.3.0.xml	2024-04-11 09:09:56.565466	64	EXECUTED	8:03e413dd182dcbd5c57e41c34d0ef682	update tableName=REQUIRED_ACTION_PROVIDER		\N	4.20.0	\N	\N	2826594441
4.6.0-KEYCLOAK-7950	psilva@redhat.com	META-INF/jpa-changelog-4.6.0.xml	2024-04-11 09:09:56.569163	65	EXECUTED	8:d27b42bb2571c18fbe3fe4e4fb7582a7	update tableName=RESOURCE_SERVER_RESOURCE		\N	4.20.0	\N	\N	2826594441
4.6.0-KEYCLOAK-8377	keycloak	META-INF/jpa-changelog-4.6.0.xml	2024-04-11 09:09:56.586526	66	EXECUTED	8:698baf84d9fd0027e9192717c2154fb8	createTable tableName=ROLE_ATTRIBUTE; addPrimaryKey constraintName=CONSTRAINT_ROLE_ATTRIBUTE_PK, tableName=ROLE_ATTRIBUTE; addForeignKeyConstraint baseTableName=ROLE_ATTRIBUTE, constraintName=FK_ROLE_ATTRIBUTE_ID, referencedTableName=KEYCLOAK_ROLE...		\N	4.20.0	\N	\N	2826594441
4.6.0-KEYCLOAK-8555	gideonray@gmail.com	META-INF/jpa-changelog-4.6.0.xml	2024-04-11 09:09:56.595247	67	EXECUTED	8:ced8822edf0f75ef26eb51582f9a821a	createIndex indexName=IDX_COMPONENT_PROVIDER_TYPE, tableName=COMPONENT		\N	4.20.0	\N	\N	2826594441
4.7.0-KEYCLOAK-1267	sguilhen@redhat.com	META-INF/jpa-changelog-4.7.0.xml	2024-04-11 09:09:56.605505	68	EXECUTED	8:f0abba004cf429e8afc43056df06487d	addColumn tableName=REALM		\N	4.20.0	\N	\N	2826594441
4.7.0-KEYCLOAK-7275	keycloak	META-INF/jpa-changelog-4.7.0.xml	2024-04-11 09:09:56.636735	69	EXECUTED	8:6662f8b0b611caa359fcf13bf63b4e24	renameColumn newColumnName=CREATED_ON, oldColumnName=LAST_SESSION_REFRESH, tableName=OFFLINE_USER_SESSION; addNotNullConstraint columnName=CREATED_ON, tableName=OFFLINE_USER_SESSION; addColumn tableName=OFFLINE_USER_SESSION; customChange; createIn...		\N	4.20.0	\N	\N	2826594441
4.8.0-KEYCLOAK-8835	sguilhen@redhat.com	META-INF/jpa-changelog-4.8.0.xml	2024-04-11 09:09:56.650498	70	EXECUTED	8:9e6b8009560f684250bdbdf97670d39e	addNotNullConstraint columnName=SSO_MAX_LIFESPAN_REMEMBER_ME, tableName=REALM; addNotNullConstraint columnName=SSO_IDLE_TIMEOUT_REMEMBER_ME, tableName=REALM		\N	4.20.0	\N	\N	2826594441
authz-7.0.0-KEYCLOAK-10443	psilva@redhat.com	META-INF/jpa-changelog-authz-7.0.0.xml	2024-04-11 09:09:56.660709	71	EXECUTED	8:4223f561f3b8dc655846562b57bb502e	addColumn tableName=RESOURCE_SERVER		\N	4.20.0	\N	\N	2826594441
8.0.0-adding-credential-columns	keycloak	META-INF/jpa-changelog-8.0.0.xml	2024-04-11 09:09:56.680644	72	EXECUTED	8:215a31c398b363ce383a2b301202f29e	addColumn tableName=CREDENTIAL; addColumn tableName=FED_USER_CREDENTIAL		\N	4.20.0	\N	\N	2826594441
8.0.0-updating-credential-data-not-oracle-fixed	keycloak	META-INF/jpa-changelog-8.0.0.xml	2024-04-11 09:09:56.705698	73	EXECUTED	8:83f7a671792ca98b3cbd3a1a34862d3d	update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=FED_USER_CREDENTIAL; update tableName=FED_USER_CREDENTIAL; update tableName=FED_USER_CREDENTIAL		\N	4.20.0	\N	\N	2826594441
8.0.0-updating-credential-data-oracle-fixed	keycloak	META-INF/jpa-changelog-8.0.0.xml	2024-04-11 09:09:56.709333	74	MARK_RAN	8:f58ad148698cf30707a6efbdf8061aa7	update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=FED_USER_CREDENTIAL; update tableName=FED_USER_CREDENTIAL; update tableName=FED_USER_CREDENTIAL		\N	4.20.0	\N	\N	2826594441
8.0.0-credential-cleanup-fixed	keycloak	META-INF/jpa-changelog-8.0.0.xml	2024-04-11 09:09:56.786493	75	EXECUTED	8:79e4fd6c6442980e58d52ffc3ee7b19c	dropDefaultValue columnName=COUNTER, tableName=CREDENTIAL; dropDefaultValue columnName=DIGITS, tableName=CREDENTIAL; dropDefaultValue columnName=PERIOD, tableName=CREDENTIAL; dropDefaultValue columnName=ALGORITHM, tableName=CREDENTIAL; dropColumn ...		\N	4.20.0	\N	\N	2826594441
8.0.0-resource-tag-support	keycloak	META-INF/jpa-changelog-8.0.0.xml	2024-04-11 09:09:56.800861	76	EXECUTED	8:87af6a1e6d241ca4b15801d1f86a297d	addColumn tableName=MIGRATION_MODEL; createIndex indexName=IDX_UPDATE_TIME, tableName=MIGRATION_MODEL		\N	4.20.0	\N	\N	2826594441
9.0.0-always-display-client	keycloak	META-INF/jpa-changelog-9.0.0.xml	2024-04-11 09:09:56.811256	77	EXECUTED	8:b44f8d9b7b6ea455305a6d72a200ed15	addColumn tableName=CLIENT		\N	4.20.0	\N	\N	2826594441
9.0.0-drop-constraints-for-column-increase	keycloak	META-INF/jpa-changelog-9.0.0.xml	2024-04-11 09:09:56.814468	78	MARK_RAN	8:2d8ed5aaaeffd0cb004c046b4a903ac5	dropUniqueConstraint constraintName=UK_FRSR6T700S9V50BU18WS5PMT, tableName=RESOURCE_SERVER_PERM_TICKET; dropUniqueConstraint constraintName=UK_FRSR6T700S9V50BU18WS5HA6, tableName=RESOURCE_SERVER_RESOURCE; dropPrimaryKey constraintName=CONSTRAINT_O...		\N	4.20.0	\N	\N	2826594441
9.0.0-increase-column-size-federated-fk	keycloak	META-INF/jpa-changelog-9.0.0.xml	2024-04-11 09:09:56.874484	79	EXECUTED	8:e290c01fcbc275326c511633f6e2acde	modifyDataType columnName=CLIENT_ID, tableName=FED_USER_CONSENT; modifyDataType columnName=CLIENT_REALM_CONSTRAINT, tableName=KEYCLOAK_ROLE; modifyDataType columnName=OWNER, tableName=RESOURCE_SERVER_POLICY; modifyDataType columnName=CLIENT_ID, ta...		\N	4.20.0	\N	\N	2826594441
9.0.0-recreate-constraints-after-column-increase	keycloak	META-INF/jpa-changelog-9.0.0.xml	2024-04-11 09:09:56.876643	80	MARK_RAN	8:c9db8784c33cea210872ac2d805439f8	addNotNullConstraint columnName=CLIENT_ID, tableName=OFFLINE_CLIENT_SESSION; addNotNullConstraint columnName=OWNER, tableName=RESOURCE_SERVER_PERM_TICKET; addNotNullConstraint columnName=REQUESTER, tableName=RESOURCE_SERVER_PERM_TICKET; addNotNull...		\N	4.20.0	\N	\N	2826594441
9.0.1-add-index-to-client.client_id	keycloak	META-INF/jpa-changelog-9.0.1.xml	2024-04-11 09:09:56.882722	81	EXECUTED	8:95b676ce8fc546a1fcfb4c92fae4add5	createIndex indexName=IDX_CLIENT_ID, tableName=CLIENT		\N	4.20.0	\N	\N	2826594441
9.0.1-KEYCLOAK-12579-drop-constraints	keycloak	META-INF/jpa-changelog-9.0.1.xml	2024-04-11 09:09:56.884266	82	MARK_RAN	8:38a6b2a41f5651018b1aca93a41401e5	dropUniqueConstraint constraintName=SIBLING_NAMES, tableName=KEYCLOAK_GROUP		\N	4.20.0	\N	\N	2826594441
9.0.1-KEYCLOAK-12579-add-not-null-constraint	keycloak	META-INF/jpa-changelog-9.0.1.xml	2024-04-11 09:09:56.889178	83	EXECUTED	8:3fb99bcad86a0229783123ac52f7609c	addNotNullConstraint columnName=PARENT_GROUP, tableName=KEYCLOAK_GROUP		\N	4.20.0	\N	\N	2826594441
9.0.1-KEYCLOAK-12579-recreate-constraints	keycloak	META-INF/jpa-changelog-9.0.1.xml	2024-04-11 09:09:56.890734	84	MARK_RAN	8:64f27a6fdcad57f6f9153210f2ec1bdb	addUniqueConstraint constraintName=SIBLING_NAMES, tableName=KEYCLOAK_GROUP		\N	4.20.0	\N	\N	2826594441
9.0.1-add-index-to-events	keycloak	META-INF/jpa-changelog-9.0.1.xml	2024-04-11 09:09:56.896874	85	EXECUTED	8:ab4f863f39adafd4c862f7ec01890abc	createIndex indexName=IDX_EVENT_TIME, tableName=EVENT_ENTITY		\N	4.20.0	\N	\N	2826594441
map-remove-ri	keycloak	META-INF/jpa-changelog-11.0.0.xml	2024-04-11 09:09:56.904921	86	EXECUTED	8:13c419a0eb336e91ee3a3bf8fda6e2a7	dropForeignKeyConstraint baseTableName=REALM, constraintName=FK_TRAF444KK6QRKMS7N56AIWQ5Y; dropForeignKeyConstraint baseTableName=KEYCLOAK_ROLE, constraintName=FK_KJHO5LE2C0RAL09FL8CM9WFW9		\N	4.20.0	\N	\N	2826594441
map-remove-ri	keycloak	META-INF/jpa-changelog-12.0.0.xml	2024-04-11 09:09:56.920651	87	EXECUTED	8:e3fb1e698e0471487f51af1ed80fe3ac	dropForeignKeyConstraint baseTableName=REALM_DEFAULT_GROUPS, constraintName=FK_DEF_GROUPS_GROUP; dropForeignKeyConstraint baseTableName=REALM_DEFAULT_ROLES, constraintName=FK_H4WPD7W4HSOOLNI3H0SW7BTJE; dropForeignKeyConstraint baseTableName=CLIENT...		\N	4.20.0	\N	\N	2826594441
12.1.0-add-realm-localization-table	keycloak	META-INF/jpa-changelog-12.0.0.xml	2024-04-11 09:09:56.930562	88	EXECUTED	8:babadb686aab7b56562817e60bf0abd0	createTable tableName=REALM_LOCALIZATIONS; addPrimaryKey tableName=REALM_LOCALIZATIONS		\N	4.20.0	\N	\N	2826594441
default-roles	keycloak	META-INF/jpa-changelog-13.0.0.xml	2024-04-11 09:09:56.941679	89	EXECUTED	8:72d03345fda8e2f17093d08801947773	addColumn tableName=REALM; customChange		\N	4.20.0	\N	\N	2826594441
default-roles-cleanup	keycloak	META-INF/jpa-changelog-13.0.0.xml	2024-04-11 09:09:56.967424	90	EXECUTED	8:61c9233951bd96ffecd9ba75f7d978a4	dropTable tableName=REALM_DEFAULT_ROLES; dropTable tableName=CLIENT_DEFAULT_ROLES		\N	4.20.0	\N	\N	2826594441
13.0.0-KEYCLOAK-16844	keycloak	META-INF/jpa-changelog-13.0.0.xml	2024-04-11 09:09:56.977785	91	EXECUTED	8:ea82e6ad945cec250af6372767b25525	createIndex indexName=IDX_OFFLINE_USS_PRELOAD, tableName=OFFLINE_USER_SESSION		\N	4.20.0	\N	\N	2826594441
map-remove-ri-13.0.0	keycloak	META-INF/jpa-changelog-13.0.0.xml	2024-04-11 09:09:57.014386	92	EXECUTED	8:d3f4a33f41d960ddacd7e2ef30d126b3	dropForeignKeyConstraint baseTableName=DEFAULT_CLIENT_SCOPE, constraintName=FK_R_DEF_CLI_SCOPE_SCOPE; dropForeignKeyConstraint baseTableName=CLIENT_SCOPE_CLIENT, constraintName=FK_C_CLI_SCOPE_SCOPE; dropForeignKeyConstraint baseTableName=CLIENT_SC...		\N	4.20.0	\N	\N	2826594441
13.0.0-KEYCLOAK-17992-drop-constraints	keycloak	META-INF/jpa-changelog-13.0.0.xml	2024-04-11 09:09:57.017797	93	MARK_RAN	8:1284a27fbd049d65831cb6fc07c8a783	dropPrimaryKey constraintName=C_CLI_SCOPE_BIND, tableName=CLIENT_SCOPE_CLIENT; dropIndex indexName=IDX_CLSCOPE_CL, tableName=CLIENT_SCOPE_CLIENT; dropIndex indexName=IDX_CL_CLSCOPE, tableName=CLIENT_SCOPE_CLIENT		\N	4.20.0	\N	\N	2826594441
13.0.0-increase-column-size-federated	keycloak	META-INF/jpa-changelog-13.0.0.xml	2024-04-11 09:09:57.046113	94	EXECUTED	8:9d11b619db2ae27c25853b8a37cd0dea	modifyDataType columnName=CLIENT_ID, tableName=CLIENT_SCOPE_CLIENT; modifyDataType columnName=SCOPE_ID, tableName=CLIENT_SCOPE_CLIENT		\N	4.20.0	\N	\N	2826594441
13.0.0-KEYCLOAK-17992-recreate-constraints	keycloak	META-INF/jpa-changelog-13.0.0.xml	2024-04-11 09:09:57.049567	95	MARK_RAN	8:3002bb3997451bb9e8bac5c5cd8d6327	addNotNullConstraint columnName=CLIENT_ID, tableName=CLIENT_SCOPE_CLIENT; addNotNullConstraint columnName=SCOPE_ID, tableName=CLIENT_SCOPE_CLIENT; addPrimaryKey constraintName=C_CLI_SCOPE_BIND, tableName=CLIENT_SCOPE_CLIENT; createIndex indexName=...		\N	4.20.0	\N	\N	2826594441
json-string-accomodation-fixed	keycloak	META-INF/jpa-changelog-13.0.0.xml	2024-04-11 09:09:57.064552	96	EXECUTED	8:dfbee0d6237a23ef4ccbb7a4e063c163	addColumn tableName=REALM_ATTRIBUTE; update tableName=REALM_ATTRIBUTE; dropColumn columnName=VALUE, tableName=REALM_ATTRIBUTE; renameColumn newColumnName=VALUE, oldColumnName=VALUE_NEW, tableName=REALM_ATTRIBUTE		\N	4.20.0	\N	\N	2826594441
14.0.0-KEYCLOAK-11019	keycloak	META-INF/jpa-changelog-14.0.0.xml	2024-04-11 09:09:57.087268	97	EXECUTED	8:75f3e372df18d38c62734eebb986b960	createIndex indexName=IDX_OFFLINE_CSS_PRELOAD, tableName=OFFLINE_CLIENT_SESSION; createIndex indexName=IDX_OFFLINE_USS_BY_USER, tableName=OFFLINE_USER_SESSION; createIndex indexName=IDX_OFFLINE_USS_BY_USERSESS, tableName=OFFLINE_USER_SESSION		\N	4.20.0	\N	\N	2826594441
14.0.0-KEYCLOAK-18286	keycloak	META-INF/jpa-changelog-14.0.0.xml	2024-04-11 09:09:57.092663	98	MARK_RAN	8:7fee73eddf84a6035691512c85637eef	createIndex indexName=IDX_CLIENT_ATT_BY_NAME_VALUE, tableName=CLIENT_ATTRIBUTES		\N	4.20.0	\N	\N	2826594441
14.0.0-KEYCLOAK-18286-revert	keycloak	META-INF/jpa-changelog-14.0.0.xml	2024-04-11 09:09:57.12657	99	MARK_RAN	8:7a11134ab12820f999fbf3bb13c3adc8	dropIndex indexName=IDX_CLIENT_ATT_BY_NAME_VALUE, tableName=CLIENT_ATTRIBUTES		\N	4.20.0	\N	\N	2826594441
14.0.0-KEYCLOAK-18286-supported-dbs	keycloak	META-INF/jpa-changelog-14.0.0.xml	2024-04-11 09:09:57.139315	100	EXECUTED	8:c0f6eaac1f3be773ffe54cb5b8482b70	createIndex indexName=IDX_CLIENT_ATT_BY_NAME_VALUE, tableName=CLIENT_ATTRIBUTES		\N	4.20.0	\N	\N	2826594441
14.0.0-KEYCLOAK-18286-unsupported-dbs	keycloak	META-INF/jpa-changelog-14.0.0.xml	2024-04-11 09:09:57.143943	101	MARK_RAN	8:18186f0008b86e0f0f49b0c4d0e842ac	createIndex indexName=IDX_CLIENT_ATT_BY_NAME_VALUE, tableName=CLIENT_ATTRIBUTES		\N	4.20.0	\N	\N	2826594441
KEYCLOAK-17267-add-index-to-user-attributes	keycloak	META-INF/jpa-changelog-14.0.0.xml	2024-04-11 09:09:57.155292	102	EXECUTED	8:09c2780bcb23b310a7019d217dc7b433	createIndex indexName=IDX_USER_ATTRIBUTE_NAME, tableName=USER_ATTRIBUTE		\N	4.20.0	\N	\N	2826594441
KEYCLOAK-18146-add-saml-art-binding-identifier	keycloak	META-INF/jpa-changelog-14.0.0.xml	2024-04-11 09:09:57.176384	103	EXECUTED	8:276a44955eab693c970a42880197fff2	customChange		\N	4.20.0	\N	\N	2826594441
15.0.0-KEYCLOAK-18467	keycloak	META-INF/jpa-changelog-15.0.0.xml	2024-04-11 09:09:57.191311	104	EXECUTED	8:ba8ee3b694d043f2bfc1a1079d0760d7	addColumn tableName=REALM_LOCALIZATIONS; update tableName=REALM_LOCALIZATIONS; dropColumn columnName=TEXTS, tableName=REALM_LOCALIZATIONS; renameColumn newColumnName=TEXTS, oldColumnName=TEXTS_NEW, tableName=REALM_LOCALIZATIONS; addNotNullConstrai...		\N	4.20.0	\N	\N	2826594441
17.0.0-9562	keycloak	META-INF/jpa-changelog-17.0.0.xml	2024-04-11 09:09:57.201925	105	EXECUTED	8:5e06b1d75f5d17685485e610c2851b17	createIndex indexName=IDX_USER_SERVICE_ACCOUNT, tableName=USER_ENTITY		\N	4.20.0	\N	\N	2826594441
18.0.0-10625-IDX_ADMIN_EVENT_TIME	keycloak	META-INF/jpa-changelog-18.0.0.xml	2024-04-11 09:09:57.212263	106	EXECUTED	8:4b80546c1dc550ac552ee7b24a4ab7c0	createIndex indexName=IDX_ADMIN_EVENT_TIME, tableName=ADMIN_EVENT_ENTITY		\N	4.20.0	\N	\N	2826594441
19.0.0-10135	keycloak	META-INF/jpa-changelog-19.0.0.xml	2024-04-11 09:09:57.233268	107	EXECUTED	8:af510cd1bb2ab6339c45372f3e491696	customChange		\N	4.20.0	\N	\N	2826594441
20.0.0-12964-supported-dbs	keycloak	META-INF/jpa-changelog-20.0.0.xml	2024-04-11 09:09:57.245069	108	EXECUTED	8:05c99fc610845ef66ee812b7921af0ef	createIndex indexName=IDX_GROUP_ATT_BY_NAME_VALUE, tableName=GROUP_ATTRIBUTE		\N	4.20.0	\N	\N	2826594441
20.0.0-12964-unsupported-dbs	keycloak	META-INF/jpa-changelog-20.0.0.xml	2024-04-11 09:09:57.248508	109	MARK_RAN	8:314e803baf2f1ec315b3464e398b8247	createIndex indexName=IDX_GROUP_ATT_BY_NAME_VALUE, tableName=GROUP_ATTRIBUTE		\N	4.20.0	\N	\N	2826594441
client-attributes-string-accomodation-fixed	keycloak	META-INF/jpa-changelog-20.0.0.xml	2024-04-11 09:09:57.266011	110	EXECUTED	8:56e4677e7e12556f70b604c573840100	addColumn tableName=CLIENT_ATTRIBUTES; update tableName=CLIENT_ATTRIBUTES; dropColumn columnName=VALUE, tableName=CLIENT_ATTRIBUTES; renameColumn newColumnName=VALUE, oldColumnName=VALUE_NEW, tableName=CLIENT_ATTRIBUTES		\N	4.20.0	\N	\N	2826594441
21.0.2-17277	keycloak	META-INF/jpa-changelog-21.0.2.xml	2024-04-11 09:09:57.285569	111	EXECUTED	8:8806cb33d2a546ce770384bf98cf6eac	customChange		\N	4.20.0	\N	\N	2826594441
21.1.0-19404	keycloak	META-INF/jpa-changelog-21.1.0.xml	2024-04-11 09:09:57.34642	112	EXECUTED	8:fdb2924649d30555ab3a1744faba4928	modifyDataType columnName=DECISION_STRATEGY, tableName=RESOURCE_SERVER_POLICY; modifyDataType columnName=LOGIC, tableName=RESOURCE_SERVER_POLICY; modifyDataType columnName=POLICY_ENFORCE_MODE, tableName=RESOURCE_SERVER		\N	4.20.0	\N	\N	2826594441
21.1.0-19404-2	keycloak	META-INF/jpa-changelog-21.1.0.xml	2024-04-11 09:09:57.349747	113	MARK_RAN	8:1c96cc2b10903bd07a03670098d67fd6	addColumn tableName=RESOURCE_SERVER_POLICY; update tableName=RESOURCE_SERVER_POLICY; dropColumn columnName=DECISION_STRATEGY, tableName=RESOURCE_SERVER_POLICY; renameColumn newColumnName=DECISION_STRATEGY, oldColumnName=DECISION_STRATEGY_NEW, tabl...		\N	4.20.0	\N	\N	2826594441
22.0.0-17484	keycloak	META-INF/jpa-changelog-22.0.0.xml	2024-04-11 09:09:57.367255	114	EXECUTED	8:4c3d4e8b142a66fcdf21b89a4dd33301	customChange		\N	4.20.0	\N	\N	2826594441
\.


--
-- Data for Name: databasechangeloglock; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.databasechangeloglock (id, locked, lockgranted, lockedby) FROM stdin;
1	f	\N	\N
1000	f	\N	\N
1001	f	\N	\N
\.


--
-- Data for Name: default_client_scope; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.default_client_scope (realm_id, scope_id, default_scope) FROM stdin;
19e687a7-a4bd-4974-9623-d44f5d246eb1	d82f684e-5e80-4f6d-896c-0d5815ed9036	f
19e687a7-a4bd-4974-9623-d44f5d246eb1	1d1514b7-0e86-4d1b-abce-a753327a9a3e	t
19e687a7-a4bd-4974-9623-d44f5d246eb1	894ea45c-2e6b-4ec7-af1d-67dfa311ca84	t
19e687a7-a4bd-4974-9623-d44f5d246eb1	af34023f-5047-4ba8-97af-a34962abaaa2	t
19e687a7-a4bd-4974-9623-d44f5d246eb1	47e6c487-6677-413a-a4da-9a319c3cde69	f
19e687a7-a4bd-4974-9623-d44f5d246eb1	42ebb2c1-c204-4d65-8037-6024d7a86c51	f
19e687a7-a4bd-4974-9623-d44f5d246eb1	2159cdef-1e6e-4042-9bc4-e27d25500d7b	t
19e687a7-a4bd-4974-9623-d44f5d246eb1	660bd312-8871-4451-84a1-c7368b29a90a	t
19e687a7-a4bd-4974-9623-d44f5d246eb1	1fffa51b-bddc-42fa-b194-8b7b297ee10e	f
19e687a7-a4bd-4974-9623-d44f5d246eb1	5570a29b-70d4-4a4e-9cb3-dedee9ce6f8a	t
8211ffad-2ece-45e2-be4d-9509ea27c3d5	0752ff44-11f5-4b5b-bde5-3d7be9386298	t
8211ffad-2ece-45e2-be4d-9509ea27c3d5	df584f35-0de0-4de7-939d-635bd41a9229	t
8211ffad-2ece-45e2-be4d-9509ea27c3d5	0a3e7bd4-fb95-4fdc-8285-813707299d9b	t
8211ffad-2ece-45e2-be4d-9509ea27c3d5	11429889-deb1-4e5d-9b2c-06e8070b0f68	t
8211ffad-2ece-45e2-be4d-9509ea27c3d5	d4c2adb0-6164-480a-8740-e84b18f86045	t
8211ffad-2ece-45e2-be4d-9509ea27c3d5	566a6009-e84e-4929-9e04-82514fd7d9ad	t
8211ffad-2ece-45e2-be4d-9509ea27c3d5	4545cb6b-9d1d-477e-a19f-0c34a0a2f9fa	f
8211ffad-2ece-45e2-be4d-9509ea27c3d5	0cd91965-a09c-4bda-a42a-fc9cb861eedf	f
8211ffad-2ece-45e2-be4d-9509ea27c3d5	7eb9b141-bc61-44b4-88ef-100e4a24eeb4	f
8211ffad-2ece-45e2-be4d-9509ea27c3d5	6be0da5b-3e6e-4599-86d0-6aa51519f8df	f
\.


--
-- Data for Name: event_entity; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.event_entity (id, client_id, details_json, error, ip_address, realm_id, session_id, event_time, type, user_id) FROM stdin;
\.


--
-- Data for Name: fed_user_attribute; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.fed_user_attribute (id, name, user_id, realm_id, storage_provider_id, value) FROM stdin;
\.


--
-- Data for Name: fed_user_consent; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.fed_user_consent (id, client_id, user_id, realm_id, storage_provider_id, created_date, last_updated_date, client_storage_provider, external_client_id) FROM stdin;
\.


--
-- Data for Name: fed_user_consent_cl_scope; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.fed_user_consent_cl_scope (user_consent_id, scope_id) FROM stdin;
\.


--
-- Data for Name: fed_user_credential; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.fed_user_credential (id, salt, type, created_date, user_id, realm_id, storage_provider_id, user_label, secret_data, credential_data, priority) FROM stdin;
\.


--
-- Data for Name: fed_user_group_membership; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.fed_user_group_membership (group_id, user_id, realm_id, storage_provider_id) FROM stdin;
\.


--
-- Data for Name: fed_user_required_action; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.fed_user_required_action (required_action, user_id, realm_id, storage_provider_id) FROM stdin;
\.


--
-- Data for Name: fed_user_role_mapping; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.fed_user_role_mapping (role_id, user_id, realm_id, storage_provider_id) FROM stdin;
\.


--
-- Data for Name: federated_identity; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.federated_identity (identity_provider, realm_id, federated_user_id, federated_username, token, user_id) FROM stdin;
\.


--
-- Data for Name: federated_user; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.federated_user (id, storage_provider_id, realm_id) FROM stdin;
\.


--
-- Data for Name: group_attribute; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.group_attribute (id, name, value, group_id) FROM stdin;
\.


--
-- Data for Name: group_role_mapping; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.group_role_mapping (role_id, group_id) FROM stdin;
b5255aa9-2d8b-439b-b953-66af402f91e9	631df8db-8023-4003-83f1-c196b819e6d2
d147dc5e-f9f9-479d-9eb0-8f7785e5ebcf	631df8db-8023-4003-83f1-c196b819e6d2
e2af2b0a-21d4-4a26-a545-8ecbeca6bb81	b4737f4e-349d-42a1-8149-e4cf40b06171
8c23a7cb-65de-4a0d-a297-febb9df524fd	b4737f4e-349d-42a1-8149-e4cf40b06171
6c2c1357-abd6-402e-925d-b0fe4f60027c	b4737f4e-349d-42a1-8149-e4cf40b06171
8652c9da-03d0-46f2-a70e-8206ef32423b	b4737f4e-349d-42a1-8149-e4cf40b06171
29899834-863c-4e9d-b1bb-f4643d4d775b	b4737f4e-349d-42a1-8149-e4cf40b06171
e09f1888-a233-436d-b7d1-cf52257aeada	b4737f4e-349d-42a1-8149-e4cf40b06171
7442d7ca-158e-4b86-9827-d01bde829f58	b4737f4e-349d-42a1-8149-e4cf40b06171
839ea5fa-5ed8-46fc-a86d-129e97ac511b	b4737f4e-349d-42a1-8149-e4cf40b06171
\.


--
-- Data for Name: identity_provider; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.identity_provider (internal_id, enabled, provider_alias, provider_id, store_token, authenticate_by_default, realm_id, add_token_role, trust_email, first_broker_login_flow_id, post_broker_login_flow_id, provider_display_name, link_only) FROM stdin;
\.


--
-- Data for Name: identity_provider_config; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.identity_provider_config (identity_provider_id, value, name) FROM stdin;
\.


--
-- Data for Name: identity_provider_mapper; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.identity_provider_mapper (id, name, idp_alias, idp_mapper_name, realm_id) FROM stdin;
\.


--
-- Data for Name: idp_mapper_config; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.idp_mapper_config (idp_mapper_id, value, name) FROM stdin;
\.


--
-- Data for Name: keycloak_group; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.keycloak_group (id, name, parent_group, realm_id) FROM stdin;
631df8db-8023-4003-83f1-c196b819e6d2	Administrator	 	8211ffad-2ece-45e2-be4d-9509ea27c3d5
b4737f4e-349d-42a1-8149-e4cf40b06171	User	 	8211ffad-2ece-45e2-be4d-9509ea27c3d5
\.


--
-- Data for Name: keycloak_role; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.keycloak_role (id, client_realm_constraint, client_role, description, name, realm_id, client, realm) FROM stdin;
7f34dbbf-9030-4de5-9c21-c6715f36c748	19e687a7-a4bd-4974-9623-d44f5d246eb1	f	${role_default-roles}	default-roles-master	19e687a7-a4bd-4974-9623-d44f5d246eb1	\N	\N
848d0788-e5d5-4a2c-87fb-911c46cf213a	19e687a7-a4bd-4974-9623-d44f5d246eb1	f	${role_admin}	admin	19e687a7-a4bd-4974-9623-d44f5d246eb1	\N	\N
8ef0b616-bfed-4c02-a49d-df1600429a73	19e687a7-a4bd-4974-9623-d44f5d246eb1	f	${role_create-realm}	create-realm	19e687a7-a4bd-4974-9623-d44f5d246eb1	\N	\N
cf872b01-0ca1-4b57-b576-8e988093fbfb	3ff497aa-8999-44cd-af9f-17d098e04a42	t	${role_create-client}	create-client	19e687a7-a4bd-4974-9623-d44f5d246eb1	3ff497aa-8999-44cd-af9f-17d098e04a42	\N
84e8898c-55e5-435d-94bf-0d7e0429ffb4	3ff497aa-8999-44cd-af9f-17d098e04a42	t	${role_view-realm}	view-realm	19e687a7-a4bd-4974-9623-d44f5d246eb1	3ff497aa-8999-44cd-af9f-17d098e04a42	\N
d9367825-441a-412c-89fc-925b05d92c80	3ff497aa-8999-44cd-af9f-17d098e04a42	t	${role_view-users}	view-users	19e687a7-a4bd-4974-9623-d44f5d246eb1	3ff497aa-8999-44cd-af9f-17d098e04a42	\N
ebf67b26-94bb-435f-aae1-5b682cd3f0aa	3ff497aa-8999-44cd-af9f-17d098e04a42	t	${role_view-clients}	view-clients	19e687a7-a4bd-4974-9623-d44f5d246eb1	3ff497aa-8999-44cd-af9f-17d098e04a42	\N
e9a541b8-8b14-4496-99df-2ecdf9d787cd	3ff497aa-8999-44cd-af9f-17d098e04a42	t	${role_view-events}	view-events	19e687a7-a4bd-4974-9623-d44f5d246eb1	3ff497aa-8999-44cd-af9f-17d098e04a42	\N
fb4febbc-d44f-4d0b-b082-b49f62093229	3ff497aa-8999-44cd-af9f-17d098e04a42	t	${role_view-identity-providers}	view-identity-providers	19e687a7-a4bd-4974-9623-d44f5d246eb1	3ff497aa-8999-44cd-af9f-17d098e04a42	\N
1576b99f-e142-42d7-9f33-746f66464b72	3ff497aa-8999-44cd-af9f-17d098e04a42	t	${role_view-authorization}	view-authorization	19e687a7-a4bd-4974-9623-d44f5d246eb1	3ff497aa-8999-44cd-af9f-17d098e04a42	\N
4161c244-0640-4339-84df-41c29a1ef9e6	3ff497aa-8999-44cd-af9f-17d098e04a42	t	${role_manage-realm}	manage-realm	19e687a7-a4bd-4974-9623-d44f5d246eb1	3ff497aa-8999-44cd-af9f-17d098e04a42	\N
0f0e3b10-bbe2-4c19-8032-4411b5da6221	3ff497aa-8999-44cd-af9f-17d098e04a42	t	${role_manage-users}	manage-users	19e687a7-a4bd-4974-9623-d44f5d246eb1	3ff497aa-8999-44cd-af9f-17d098e04a42	\N
84cc82bf-3528-4db5-97cc-80ad99064ca5	3ff497aa-8999-44cd-af9f-17d098e04a42	t	${role_manage-clients}	manage-clients	19e687a7-a4bd-4974-9623-d44f5d246eb1	3ff497aa-8999-44cd-af9f-17d098e04a42	\N
ea187166-4e89-412a-a53a-42464df16e1e	3ff497aa-8999-44cd-af9f-17d098e04a42	t	${role_manage-events}	manage-events	19e687a7-a4bd-4974-9623-d44f5d246eb1	3ff497aa-8999-44cd-af9f-17d098e04a42	\N
fdb85dbd-da9a-42bd-b158-eedc4108b767	3ff497aa-8999-44cd-af9f-17d098e04a42	t	${role_manage-identity-providers}	manage-identity-providers	19e687a7-a4bd-4974-9623-d44f5d246eb1	3ff497aa-8999-44cd-af9f-17d098e04a42	\N
6ac4bee1-aea8-4eb0-9a3d-27ee6c427f8c	3ff497aa-8999-44cd-af9f-17d098e04a42	t	${role_manage-authorization}	manage-authorization	19e687a7-a4bd-4974-9623-d44f5d246eb1	3ff497aa-8999-44cd-af9f-17d098e04a42	\N
86e75a71-0e62-400b-b28e-d0d2db814e97	3ff497aa-8999-44cd-af9f-17d098e04a42	t	${role_query-users}	query-users	19e687a7-a4bd-4974-9623-d44f5d246eb1	3ff497aa-8999-44cd-af9f-17d098e04a42	\N
66b070db-3b0a-40cb-97fc-8e8272984e41	3ff497aa-8999-44cd-af9f-17d098e04a42	t	${role_query-clients}	query-clients	19e687a7-a4bd-4974-9623-d44f5d246eb1	3ff497aa-8999-44cd-af9f-17d098e04a42	\N
ec2db440-17c2-4d08-af36-9e543e32a114	3ff497aa-8999-44cd-af9f-17d098e04a42	t	${role_query-realms}	query-realms	19e687a7-a4bd-4974-9623-d44f5d246eb1	3ff497aa-8999-44cd-af9f-17d098e04a42	\N
58f665f8-1fb8-44fb-ad4c-a8ff9567e425	3ff497aa-8999-44cd-af9f-17d098e04a42	t	${role_query-groups}	query-groups	19e687a7-a4bd-4974-9623-d44f5d246eb1	3ff497aa-8999-44cd-af9f-17d098e04a42	\N
6a3363e1-6416-4315-b6eb-e50b011dcc81	adec04de-0d89-4b02-a17c-85c2a535c226	t	${role_view-profile}	view-profile	19e687a7-a4bd-4974-9623-d44f5d246eb1	adec04de-0d89-4b02-a17c-85c2a535c226	\N
a27bfda4-4509-454a-b7c4-011054f7bf5e	adec04de-0d89-4b02-a17c-85c2a535c226	t	${role_manage-account}	manage-account	19e687a7-a4bd-4974-9623-d44f5d246eb1	adec04de-0d89-4b02-a17c-85c2a535c226	\N
af8714d9-28a8-4441-b391-3dbad8bb6da4	adec04de-0d89-4b02-a17c-85c2a535c226	t	${role_manage-account-links}	manage-account-links	19e687a7-a4bd-4974-9623-d44f5d246eb1	adec04de-0d89-4b02-a17c-85c2a535c226	\N
3c318233-9f51-4bb3-93b3-ec0d561892f7	adec04de-0d89-4b02-a17c-85c2a535c226	t	${role_view-applications}	view-applications	19e687a7-a4bd-4974-9623-d44f5d246eb1	adec04de-0d89-4b02-a17c-85c2a535c226	\N
20c6719e-8e2e-4967-b7ea-1926b574d2a0	adec04de-0d89-4b02-a17c-85c2a535c226	t	${role_view-consent}	view-consent	19e687a7-a4bd-4974-9623-d44f5d246eb1	adec04de-0d89-4b02-a17c-85c2a535c226	\N
96162472-6396-4138-93db-3e8b414eb0b9	adec04de-0d89-4b02-a17c-85c2a535c226	t	${role_manage-consent}	manage-consent	19e687a7-a4bd-4974-9623-d44f5d246eb1	adec04de-0d89-4b02-a17c-85c2a535c226	\N
1aaa5633-ffd3-45e8-b4a2-5307ce2ae074	adec04de-0d89-4b02-a17c-85c2a535c226	t	${role_view-groups}	view-groups	19e687a7-a4bd-4974-9623-d44f5d246eb1	adec04de-0d89-4b02-a17c-85c2a535c226	\N
4334ef30-fa04-41c8-bed2-bb4edcd3374f	adec04de-0d89-4b02-a17c-85c2a535c226	t	${role_delete-account}	delete-account	19e687a7-a4bd-4974-9623-d44f5d246eb1	adec04de-0d89-4b02-a17c-85c2a535c226	\N
d16bdc3a-476e-4f50-ae7e-861dbdc1823c	add41af2-b51e-4410-ac33-9419dc6a000c	t	${role_read-token}	read-token	19e687a7-a4bd-4974-9623-d44f5d246eb1	add41af2-b51e-4410-ac33-9419dc6a000c	\N
bc2a9aeb-733b-4c14-a6e9-a30901a13616	3ff497aa-8999-44cd-af9f-17d098e04a42	t	${role_impersonation}	impersonation	19e687a7-a4bd-4974-9623-d44f5d246eb1	3ff497aa-8999-44cd-af9f-17d098e04a42	\N
65fbf254-0242-466b-bcb0-4eca469ce313	19e687a7-a4bd-4974-9623-d44f5d246eb1	f	${role_offline-access}	offline_access	19e687a7-a4bd-4974-9623-d44f5d246eb1	\N	\N
e814189e-ed04-4edc-af32-780e03ad69fc	19e687a7-a4bd-4974-9623-d44f5d246eb1	f	${role_uma_authorization}	uma_authorization	19e687a7-a4bd-4974-9623-d44f5d246eb1	\N	\N
aa1e1645-5ddc-4219-84fd-42b806510732	8211ffad-2ece-45e2-be4d-9509ea27c3d5	f	${role_default-roles}	default-roles-fragjetzt	8211ffad-2ece-45e2-be4d-9509ea27c3d5	\N	\N
dc3a8db3-86ff-43a7-abc2-c9b1482ddda8	e0df3a5b-d8f3-4641-af97-e2649ab5b9ab	t	${role_create-client}	create-client	19e687a7-a4bd-4974-9623-d44f5d246eb1	e0df3a5b-d8f3-4641-af97-e2649ab5b9ab	\N
1bd85474-fbc3-43d7-9a14-eed87d7dd23d	e0df3a5b-d8f3-4641-af97-e2649ab5b9ab	t	${role_view-realm}	view-realm	19e687a7-a4bd-4974-9623-d44f5d246eb1	e0df3a5b-d8f3-4641-af97-e2649ab5b9ab	\N
d01c9ffb-c792-4cda-b8a4-b50598025cf1	e0df3a5b-d8f3-4641-af97-e2649ab5b9ab	t	${role_view-users}	view-users	19e687a7-a4bd-4974-9623-d44f5d246eb1	e0df3a5b-d8f3-4641-af97-e2649ab5b9ab	\N
c71e0ac9-eff0-4022-a5a4-05c635961d61	e0df3a5b-d8f3-4641-af97-e2649ab5b9ab	t	${role_view-clients}	view-clients	19e687a7-a4bd-4974-9623-d44f5d246eb1	e0df3a5b-d8f3-4641-af97-e2649ab5b9ab	\N
869d01b5-4202-4cd9-907d-9bafacb93ab4	e0df3a5b-d8f3-4641-af97-e2649ab5b9ab	t	${role_view-events}	view-events	19e687a7-a4bd-4974-9623-d44f5d246eb1	e0df3a5b-d8f3-4641-af97-e2649ab5b9ab	\N
331858f6-63d1-4561-b179-288850e6b45d	b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	t	${role_view-events}	view-events	8211ffad-2ece-45e2-be4d-9509ea27c3d5	b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	\N
8f3f6a6d-ccd3-4fa3-9fc0-b75774cc6e08	e0df3a5b-d8f3-4641-af97-e2649ab5b9ab	t	${role_view-identity-providers}	view-identity-providers	19e687a7-a4bd-4974-9623-d44f5d246eb1	e0df3a5b-d8f3-4641-af97-e2649ab5b9ab	\N
e033c0f0-84e9-4419-a1ba-b31055990b0f	e0df3a5b-d8f3-4641-af97-e2649ab5b9ab	t	${role_view-authorization}	view-authorization	19e687a7-a4bd-4974-9623-d44f5d246eb1	e0df3a5b-d8f3-4641-af97-e2649ab5b9ab	\N
68762db6-8943-41c0-abeb-4b86284d6970	e0df3a5b-d8f3-4641-af97-e2649ab5b9ab	t	${role_manage-realm}	manage-realm	19e687a7-a4bd-4974-9623-d44f5d246eb1	e0df3a5b-d8f3-4641-af97-e2649ab5b9ab	\N
0b01e800-4d22-46c5-a3ea-c24885a66614	e0df3a5b-d8f3-4641-af97-e2649ab5b9ab	t	${role_manage-users}	manage-users	19e687a7-a4bd-4974-9623-d44f5d246eb1	e0df3a5b-d8f3-4641-af97-e2649ab5b9ab	\N
f14d8562-374f-43ca-abff-ffc2ce80153a	e0df3a5b-d8f3-4641-af97-e2649ab5b9ab	t	${role_manage-clients}	manage-clients	19e687a7-a4bd-4974-9623-d44f5d246eb1	e0df3a5b-d8f3-4641-af97-e2649ab5b9ab	\N
27db64f8-fcb9-4385-ad1f-7b335f1e410b	e0df3a5b-d8f3-4641-af97-e2649ab5b9ab	t	${role_manage-events}	manage-events	19e687a7-a4bd-4974-9623-d44f5d246eb1	e0df3a5b-d8f3-4641-af97-e2649ab5b9ab	\N
c4931879-182a-4c40-afc5-1df7dfd38f19	e0df3a5b-d8f3-4641-af97-e2649ab5b9ab	t	${role_manage-identity-providers}	manage-identity-providers	19e687a7-a4bd-4974-9623-d44f5d246eb1	e0df3a5b-d8f3-4641-af97-e2649ab5b9ab	\N
91fa29f2-2baa-405f-8e45-8097fe4c27c5	e0df3a5b-d8f3-4641-af97-e2649ab5b9ab	t	${role_manage-authorization}	manage-authorization	19e687a7-a4bd-4974-9623-d44f5d246eb1	e0df3a5b-d8f3-4641-af97-e2649ab5b9ab	\N
651cd93b-eb3a-42c9-a642-b3c2303578da	e0df3a5b-d8f3-4641-af97-e2649ab5b9ab	t	${role_query-users}	query-users	19e687a7-a4bd-4974-9623-d44f5d246eb1	e0df3a5b-d8f3-4641-af97-e2649ab5b9ab	\N
62df278f-d8bd-4317-896d-cbd3ad20dc22	e0df3a5b-d8f3-4641-af97-e2649ab5b9ab	t	${role_query-clients}	query-clients	19e687a7-a4bd-4974-9623-d44f5d246eb1	e0df3a5b-d8f3-4641-af97-e2649ab5b9ab	\N
26664dd4-5818-4bdd-a3f1-4d4318536ffe	e0df3a5b-d8f3-4641-af97-e2649ab5b9ab	t	${role_query-realms}	query-realms	19e687a7-a4bd-4974-9623-d44f5d246eb1	e0df3a5b-d8f3-4641-af97-e2649ab5b9ab	\N
f64a42d4-c9de-4e79-9da0-ff9d35ec8005	e0df3a5b-d8f3-4641-af97-e2649ab5b9ab	t	${role_query-groups}	query-groups	19e687a7-a4bd-4974-9623-d44f5d246eb1	e0df3a5b-d8f3-4641-af97-e2649ab5b9ab	\N
b5255aa9-2d8b-439b-b953-66af402f91e9	8211ffad-2ece-45e2-be4d-9509ea27c3d5	f	Allows access to the frag.jetzt Admin Dashboard	admin-dashboard	8211ffad-2ece-45e2-be4d-9509ea27c3d5	\N	\N
e35019e2-a64f-4807-b703-6eaa9ae264eb	8211ffad-2ece-45e2-be4d-9509ea27c3d5	f	${role_offline-access}	offline_access	8211ffad-2ece-45e2-be4d-9509ea27c3d5	\N	\N
d147dc5e-f9f9-479d-9eb0-8f7785e5ebcf	8211ffad-2ece-45e2-be4d-9509ea27c3d5	f	Allows owner access to all rooms and resources	admin-all-rooms-owner	8211ffad-2ece-45e2-be4d-9509ea27c3d5	\N	\N
7543a07e-98d9-46b1-b152-998de5966b3d	8211ffad-2ece-45e2-be4d-9509ea27c3d5	f	${role_uma_authorization}	uma_authorization	8211ffad-2ece-45e2-be4d-9509ea27c3d5	\N	\N
13501f0e-822a-43b8-bac4-32f1f10f6b6e	b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	t	${role_query-realms}	query-realms	8211ffad-2ece-45e2-be4d-9509ea27c3d5	b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	\N
51af6832-8e86-4e8f-837f-a39890eaa5d4	b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	t	${role_query-groups}	query-groups	8211ffad-2ece-45e2-be4d-9509ea27c3d5	b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	\N
79f928c2-9f1f-4a7d-bd8d-a4582162d73e	b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	t	${role_manage-authorization}	manage-authorization	8211ffad-2ece-45e2-be4d-9509ea27c3d5	b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	\N
d4aadffa-cbe9-4a1a-af19-834708456189	b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	t	${role_view-realm}	view-realm	8211ffad-2ece-45e2-be4d-9509ea27c3d5	b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	\N
277be2b7-cade-4293-9f23-205f840841b4	b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	t	${role_manage-users}	manage-users	8211ffad-2ece-45e2-be4d-9509ea27c3d5	b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	\N
38457439-a532-4111-983d-1c1d2a8d58cb	b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	t	${role_view-users}	view-users	8211ffad-2ece-45e2-be4d-9509ea27c3d5	b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	\N
def7a9a8-1e2c-4c5b-8f9f-26eac8b254aa	b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	t	${role_query-users}	query-users	8211ffad-2ece-45e2-be4d-9509ea27c3d5	b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	\N
88d9839b-2028-4691-8164-9e86fe647966	b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	t	${role_view-authorization}	view-authorization	8211ffad-2ece-45e2-be4d-9509ea27c3d5	b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	\N
d758d353-8498-409e-a012-3a3bec44f52b	b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	t	${role_manage-clients}	manage-clients	8211ffad-2ece-45e2-be4d-9509ea27c3d5	b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	\N
f188fd2a-13d4-41e0-84c2-830de8f6517d	b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	t	${role_view-clients}	view-clients	8211ffad-2ece-45e2-be4d-9509ea27c3d5	b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	\N
225739d6-a58c-4224-a69a-00c4fd0a8da4	b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	t	${role_impersonation}	impersonation	8211ffad-2ece-45e2-be4d-9509ea27c3d5	b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	\N
5e08f1d3-2f86-41ad-b0b5-bc1a7850f7fe	b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	t	${role_create-client}	create-client	8211ffad-2ece-45e2-be4d-9509ea27c3d5	b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	\N
face08e1-9944-4050-84a5-3d788bf4eab6	b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	t	${role_realm-admin}	realm-admin	8211ffad-2ece-45e2-be4d-9509ea27c3d5	b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	\N
21e867b1-b1c0-4d44-a0c6-46d9746bc286	b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	t	${role_query-clients}	query-clients	8211ffad-2ece-45e2-be4d-9509ea27c3d5	b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	\N
3606dd17-8278-4d90-a075-63dee7f8ea56	b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	t	${role_manage-identity-providers}	manage-identity-providers	8211ffad-2ece-45e2-be4d-9509ea27c3d5	b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	\N
32861b3a-0d78-4fe3-8f54-3235f2c10631	b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	t	${role_view-identity-providers}	view-identity-providers	8211ffad-2ece-45e2-be4d-9509ea27c3d5	b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	\N
5a6f5ad5-53d6-4b54-b170-f90facc8fd2a	b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	t	${role_manage-realm}	manage-realm	8211ffad-2ece-45e2-be4d-9509ea27c3d5	b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	\N
32c50071-a52c-46c9-9362-6bafc76a0ba4	b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	t	${role_manage-events}	manage-events	8211ffad-2ece-45e2-be4d-9509ea27c3d5	b1a4a7d0-545e-4fc7-85dd-628ecacd9ebc	\N
10838da1-8b25-4b1d-accf-ed2a1adbd1ec	2f9dc197-7822-4483-8aae-99f87b573f22	t	${role_read-token}	read-token	8211ffad-2ece-45e2-be4d-9509ea27c3d5	2f9dc197-7822-4483-8aae-99f87b573f22	\N
e2af2b0a-21d4-4a26-a545-8ecbeca6bb81	a6e6b6b5-1888-44a4-892d-202b54ae2459	t	${role_view-groups}	view-groups	8211ffad-2ece-45e2-be4d-9509ea27c3d5	a6e6b6b5-1888-44a4-892d-202b54ae2459	\N
8c23a7cb-65de-4a0d-a297-febb9df524fd	a6e6b6b5-1888-44a4-892d-202b54ae2459	t	${role_delete-account}	delete-account	8211ffad-2ece-45e2-be4d-9509ea27c3d5	a6e6b6b5-1888-44a4-892d-202b54ae2459	\N
6c2c1357-abd6-402e-925d-b0fe4f60027c	a6e6b6b5-1888-44a4-892d-202b54ae2459	t	${role_manage-account}	manage-account	8211ffad-2ece-45e2-be4d-9509ea27c3d5	a6e6b6b5-1888-44a4-892d-202b54ae2459	\N
8652c9da-03d0-46f2-a70e-8206ef32423b	a6e6b6b5-1888-44a4-892d-202b54ae2459	t	${role_view-consent}	view-consent	8211ffad-2ece-45e2-be4d-9509ea27c3d5	a6e6b6b5-1888-44a4-892d-202b54ae2459	\N
29899834-863c-4e9d-b1bb-f4643d4d775b	a6e6b6b5-1888-44a4-892d-202b54ae2459	t	${role_view-profile}	view-profile	8211ffad-2ece-45e2-be4d-9509ea27c3d5	a6e6b6b5-1888-44a4-892d-202b54ae2459	\N
e09f1888-a233-436d-b7d1-cf52257aeada	a6e6b6b5-1888-44a4-892d-202b54ae2459	t	${role_manage-consent}	manage-consent	8211ffad-2ece-45e2-be4d-9509ea27c3d5	a6e6b6b5-1888-44a4-892d-202b54ae2459	\N
7442d7ca-158e-4b86-9827-d01bde829f58	a6e6b6b5-1888-44a4-892d-202b54ae2459	t	${role_view-applications}	view-applications	8211ffad-2ece-45e2-be4d-9509ea27c3d5	a6e6b6b5-1888-44a4-892d-202b54ae2459	\N
839ea5fa-5ed8-46fc-a86d-129e97ac511b	a6e6b6b5-1888-44a4-892d-202b54ae2459	t	${role_manage-account-links}	manage-account-links	8211ffad-2ece-45e2-be4d-9509ea27c3d5	a6e6b6b5-1888-44a4-892d-202b54ae2459	\N
559f1471-9613-4e45-a508-d06ec62d1946	e0df3a5b-d8f3-4641-af97-e2649ab5b9ab	t	${role_impersonation}	impersonation	19e687a7-a4bd-4974-9623-d44f5d246eb1	e0df3a5b-d8f3-4641-af97-e2649ab5b9ab	\N
\.


--
-- Data for Name: migration_model; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.migration_model (id, version, update_time) FROM stdin;
5qfmr	22.0.0	1712826597
\.


--
-- Data for Name: offline_client_session; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.offline_client_session (user_session_id, client_id, offline_flag, "timestamp", data, client_storage_provider, external_client_id) FROM stdin;
\.


--
-- Data for Name: offline_user_session; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.offline_user_session (user_session_id, user_id, realm_id, created_on, offline_flag, data, last_session_refresh) FROM stdin;
\.


--
-- Data for Name: policy_config; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.policy_config (policy_id, name, value) FROM stdin;
\.


--
-- Data for Name: protocol_mapper; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.protocol_mapper (id, name, protocol, protocol_mapper_name, client_id, client_scope_id) FROM stdin;
3397ad08-d52e-4f99-86ac-aaf8db49c522	audience resolve	openid-connect	oidc-audience-resolve-mapper	70fbcc0a-f28d-4e1e-a96f-cf3ce4433149	\N
fafe8e01-3644-46a3-b81a-435c5763b06e	locale	openid-connect	oidc-usermodel-attribute-mapper	79c8f459-6cb5-43cf-9527-51d3489cf8ac	\N
c8244bb8-d464-4ae9-89d6-3805e139f17a	role list	saml	saml-role-list-mapper	\N	1d1514b7-0e86-4d1b-abce-a753327a9a3e
5f2ddead-6c14-470f-9174-2cead8a2cdca	full name	openid-connect	oidc-full-name-mapper	\N	894ea45c-2e6b-4ec7-af1d-67dfa311ca84
8e7ace55-8a95-499d-a0ac-2aa80e6f27a5	family name	openid-connect	oidc-usermodel-attribute-mapper	\N	894ea45c-2e6b-4ec7-af1d-67dfa311ca84
eb8bbc22-2b3d-4183-9b28-91ebfd795a84	given name	openid-connect	oidc-usermodel-attribute-mapper	\N	894ea45c-2e6b-4ec7-af1d-67dfa311ca84
4fc90e35-8d3f-4c42-bdc6-a52d408ae277	middle name	openid-connect	oidc-usermodel-attribute-mapper	\N	894ea45c-2e6b-4ec7-af1d-67dfa311ca84
1993673b-cb8d-48ff-a2ec-0b320724ad02	nickname	openid-connect	oidc-usermodel-attribute-mapper	\N	894ea45c-2e6b-4ec7-af1d-67dfa311ca84
80fbe7c5-a925-4750-816b-0562bb15c384	username	openid-connect	oidc-usermodel-attribute-mapper	\N	894ea45c-2e6b-4ec7-af1d-67dfa311ca84
3eb4f68f-7d72-41a1-a1a7-7f5366582069	profile	openid-connect	oidc-usermodel-attribute-mapper	\N	894ea45c-2e6b-4ec7-af1d-67dfa311ca84
2508bacd-2622-4df3-9736-a685014dd5ea	picture	openid-connect	oidc-usermodel-attribute-mapper	\N	894ea45c-2e6b-4ec7-af1d-67dfa311ca84
3d5242b9-9387-44bf-b812-15bd3d99f475	website	openid-connect	oidc-usermodel-attribute-mapper	\N	894ea45c-2e6b-4ec7-af1d-67dfa311ca84
8bf9a705-9a22-4e24-bedc-80b3852de533	gender	openid-connect	oidc-usermodel-attribute-mapper	\N	894ea45c-2e6b-4ec7-af1d-67dfa311ca84
d35576e6-0657-4fc3-a1c6-c035ff0a7eaa	birthdate	openid-connect	oidc-usermodel-attribute-mapper	\N	894ea45c-2e6b-4ec7-af1d-67dfa311ca84
87cfe205-01b9-44e6-9941-4e1e57fb3f72	zoneinfo	openid-connect	oidc-usermodel-attribute-mapper	\N	894ea45c-2e6b-4ec7-af1d-67dfa311ca84
97c643e9-9700-49f7-82a8-7318ac4281d8	locale	openid-connect	oidc-usermodel-attribute-mapper	\N	894ea45c-2e6b-4ec7-af1d-67dfa311ca84
b70973fe-cb5f-497e-af33-1a9189c92304	updated at	openid-connect	oidc-usermodel-attribute-mapper	\N	894ea45c-2e6b-4ec7-af1d-67dfa311ca84
23934d3c-8b2a-464a-90df-97a0734b91a5	email	openid-connect	oidc-usermodel-attribute-mapper	\N	af34023f-5047-4ba8-97af-a34962abaaa2
829ee6f6-c7c6-4bd1-a2d3-5a2b078d5fd4	email verified	openid-connect	oidc-usermodel-property-mapper	\N	af34023f-5047-4ba8-97af-a34962abaaa2
16ade61e-7f60-4883-8fc9-1753e388e80e	address	openid-connect	oidc-address-mapper	\N	47e6c487-6677-413a-a4da-9a319c3cde69
89a6e626-a103-4f26-a926-61624a94a65f	phone number	openid-connect	oidc-usermodel-attribute-mapper	\N	42ebb2c1-c204-4d65-8037-6024d7a86c51
2890acc3-f1e1-40a3-a582-6c2ecce7b449	phone number verified	openid-connect	oidc-usermodel-attribute-mapper	\N	42ebb2c1-c204-4d65-8037-6024d7a86c51
f2e8f39c-6659-469c-ada8-4350de093333	realm roles	openid-connect	oidc-usermodel-realm-role-mapper	\N	2159cdef-1e6e-4042-9bc4-e27d25500d7b
180479f6-13d2-4349-b66f-932f3030bf3b	client roles	openid-connect	oidc-usermodel-client-role-mapper	\N	2159cdef-1e6e-4042-9bc4-e27d25500d7b
dc8c48c0-c390-4568-a39f-877181a64d4f	audience resolve	openid-connect	oidc-audience-resolve-mapper	\N	2159cdef-1e6e-4042-9bc4-e27d25500d7b
3134170d-d106-4033-b44a-875331ab4bb9	allowed web origins	openid-connect	oidc-allowed-origins-mapper	\N	660bd312-8871-4451-84a1-c7368b29a90a
e72cf8ae-3138-460b-85c3-5b84e2e591f7	upn	openid-connect	oidc-usermodel-attribute-mapper	\N	1fffa51b-bddc-42fa-b194-8b7b297ee10e
6650597e-33a1-4fb5-8f1b-889ffb3c3255	groups	openid-connect	oidc-usermodel-realm-role-mapper	\N	1fffa51b-bddc-42fa-b194-8b7b297ee10e
b9fe5743-8851-463e-aaee-0db538a1eea8	acr loa level	openid-connect	oidc-acr-mapper	\N	5570a29b-70d4-4a4e-9cb3-dedee9ce6f8a
402c3f98-2e1a-4d40-b6d2-ec4765f7f738	role list	saml	saml-role-list-mapper	\N	0752ff44-11f5-4b5b-bde5-3d7be9386298
29139e2f-06e0-4535-9c64-50ffb3cd1e98	address	openid-connect	oidc-address-mapper	\N	0cd91965-a09c-4bda-a42a-fc9cb861eedf
dd5436a3-c0a8-4dbb-90a7-f78006bed46c	acr loa level	openid-connect	oidc-acr-mapper	\N	566a6009-e84e-4929-9e04-82514fd7d9ad
8931b4ee-54be-4ffa-b3cf-413173c8e6ec	phone number	openid-connect	oidc-usermodel-attribute-mapper	\N	7eb9b141-bc61-44b4-88ef-100e4a24eeb4
84d5480f-0bb2-407a-92f5-114afd8c2b68	phone number verified	openid-connect	oidc-usermodel-attribute-mapper	\N	7eb9b141-bc61-44b4-88ef-100e4a24eeb4
2119a7d8-28be-4514-9627-4fb1bf482f96	email	openid-connect	oidc-usermodel-attribute-mapper	\N	0a3e7bd4-fb95-4fdc-8285-813707299d9b
327ff9a7-b401-4e6a-88eb-87b31b713233	email verified	openid-connect	oidc-usermodel-property-mapper	\N	0a3e7bd4-fb95-4fdc-8285-813707299d9b
50656748-8ecb-4acd-b528-d4517e152c5e	audience resolve	openid-connect	oidc-audience-resolve-mapper	\N	11429889-deb1-4e5d-9b2c-06e8070b0f68
1d5d06aa-9706-4664-858b-1847a57b4607	realm roles	openid-connect	oidc-usermodel-realm-role-mapper	\N	11429889-deb1-4e5d-9b2c-06e8070b0f68
4650fe11-2025-4160-a7cc-699905997a64	client roles	openid-connect	oidc-usermodel-client-role-mapper	\N	11429889-deb1-4e5d-9b2c-06e8070b0f68
3af3c9f5-88cf-4b7f-a8a5-1d0f4444cc27	allowed web origins	openid-connect	oidc-allowed-origins-mapper	\N	d4c2adb0-6164-480a-8740-e84b18f86045
c5255b5a-a344-42bc-873a-2150154976de	picture	openid-connect	oidc-usermodel-attribute-mapper	\N	df584f35-0de0-4de7-939d-635bd41a9229
acda0d3d-3e02-46b7-9572-649a1f98155d	zoneinfo	openid-connect	oidc-usermodel-attribute-mapper	\N	df584f35-0de0-4de7-939d-635bd41a9229
2454e9fb-3c22-4856-8256-df6e75e53e4c	username	openid-connect	oidc-usermodel-attribute-mapper	\N	df584f35-0de0-4de7-939d-635bd41a9229
f3fe48ec-cf97-4e2d-a6c9-1faab3ce9df1	full name	openid-connect	oidc-full-name-mapper	\N	df584f35-0de0-4de7-939d-635bd41a9229
7f9bd9a7-5e53-4a8f-b9e0-8f7debd7fa86	profile	openid-connect	oidc-usermodel-attribute-mapper	\N	df584f35-0de0-4de7-939d-635bd41a9229
796e0157-c7b2-4591-a272-26786157c00a	birthdate	openid-connect	oidc-usermodel-attribute-mapper	\N	df584f35-0de0-4de7-939d-635bd41a9229
08f7accc-f9f1-4442-b5a0-e82319a676ce	given name	openid-connect	oidc-usermodel-attribute-mapper	\N	df584f35-0de0-4de7-939d-635bd41a9229
79e4f284-ba8e-4007-a9c8-18cc54ec5611	gender	openid-connect	oidc-usermodel-attribute-mapper	\N	df584f35-0de0-4de7-939d-635bd41a9229
3c426bda-6ff9-4338-966f-80d159c41a87	updated at	openid-connect	oidc-usermodel-attribute-mapper	\N	df584f35-0de0-4de7-939d-635bd41a9229
549bb677-54ec-4da9-90e7-43e197cc484e	nickname	openid-connect	oidc-usermodel-attribute-mapper	\N	df584f35-0de0-4de7-939d-635bd41a9229
8b38e06f-920f-4a75-894c-65c4a863d563	website	openid-connect	oidc-usermodel-attribute-mapper	\N	df584f35-0de0-4de7-939d-635bd41a9229
b6d683b8-d99a-42e6-8799-bcd8a301b0e0	middle name	openid-connect	oidc-usermodel-attribute-mapper	\N	df584f35-0de0-4de7-939d-635bd41a9229
dd2cd5f9-7deb-4e2d-b4bf-887092495130	locale	openid-connect	oidc-usermodel-attribute-mapper	\N	df584f35-0de0-4de7-939d-635bd41a9229
32b80d10-e8e5-4c07-8d92-0d42d20a33ee	family name	openid-connect	oidc-usermodel-attribute-mapper	\N	df584f35-0de0-4de7-939d-635bd41a9229
71790784-f0ea-43c1-b75c-d4cb2dd8129a	upn	openid-connect	oidc-usermodel-attribute-mapper	\N	6be0da5b-3e6e-4599-86d0-6aa51519f8df
43cf9b96-4649-47ab-af19-624a4e7ea1aa	groups	openid-connect	oidc-usermodel-realm-role-mapper	\N	6be0da5b-3e6e-4599-86d0-6aa51519f8df
3ea0c0d4-f384-4156-83da-dd243b4d559c	audience resolve	openid-connect	oidc-audience-resolve-mapper	ad527668-535b-41b0-bfcd-3bbb4a3e684e	\N
8717e63d-ab28-46e3-9ea1-ab05f5e9f4ea	locale	openid-connect	oidc-usermodel-attribute-mapper	f41d3566-a7d2-4326-8425-4c9ce085d282	\N
\.


--
-- Data for Name: protocol_mapper_config; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.protocol_mapper_config (protocol_mapper_id, value, name) FROM stdin;
fafe8e01-3644-46a3-b81a-435c5763b06e	true	userinfo.token.claim
fafe8e01-3644-46a3-b81a-435c5763b06e	locale	user.attribute
fafe8e01-3644-46a3-b81a-435c5763b06e	true	id.token.claim
fafe8e01-3644-46a3-b81a-435c5763b06e	true	access.token.claim
fafe8e01-3644-46a3-b81a-435c5763b06e	locale	claim.name
fafe8e01-3644-46a3-b81a-435c5763b06e	String	jsonType.label
c8244bb8-d464-4ae9-89d6-3805e139f17a	false	single
c8244bb8-d464-4ae9-89d6-3805e139f17a	Basic	attribute.nameformat
c8244bb8-d464-4ae9-89d6-3805e139f17a	Role	attribute.name
1993673b-cb8d-48ff-a2ec-0b320724ad02	true	userinfo.token.claim
1993673b-cb8d-48ff-a2ec-0b320724ad02	nickname	user.attribute
1993673b-cb8d-48ff-a2ec-0b320724ad02	true	id.token.claim
1993673b-cb8d-48ff-a2ec-0b320724ad02	true	access.token.claim
1993673b-cb8d-48ff-a2ec-0b320724ad02	nickname	claim.name
1993673b-cb8d-48ff-a2ec-0b320724ad02	String	jsonType.label
2508bacd-2622-4df3-9736-a685014dd5ea	true	userinfo.token.claim
2508bacd-2622-4df3-9736-a685014dd5ea	picture	user.attribute
2508bacd-2622-4df3-9736-a685014dd5ea	true	id.token.claim
2508bacd-2622-4df3-9736-a685014dd5ea	true	access.token.claim
2508bacd-2622-4df3-9736-a685014dd5ea	picture	claim.name
2508bacd-2622-4df3-9736-a685014dd5ea	String	jsonType.label
3d5242b9-9387-44bf-b812-15bd3d99f475	true	userinfo.token.claim
3d5242b9-9387-44bf-b812-15bd3d99f475	website	user.attribute
3d5242b9-9387-44bf-b812-15bd3d99f475	true	id.token.claim
3d5242b9-9387-44bf-b812-15bd3d99f475	true	access.token.claim
3d5242b9-9387-44bf-b812-15bd3d99f475	website	claim.name
3d5242b9-9387-44bf-b812-15bd3d99f475	String	jsonType.label
3eb4f68f-7d72-41a1-a1a7-7f5366582069	true	userinfo.token.claim
3eb4f68f-7d72-41a1-a1a7-7f5366582069	profile	user.attribute
3eb4f68f-7d72-41a1-a1a7-7f5366582069	true	id.token.claim
3eb4f68f-7d72-41a1-a1a7-7f5366582069	true	access.token.claim
3eb4f68f-7d72-41a1-a1a7-7f5366582069	profile	claim.name
3eb4f68f-7d72-41a1-a1a7-7f5366582069	String	jsonType.label
4fc90e35-8d3f-4c42-bdc6-a52d408ae277	true	userinfo.token.claim
4fc90e35-8d3f-4c42-bdc6-a52d408ae277	middleName	user.attribute
4fc90e35-8d3f-4c42-bdc6-a52d408ae277	true	id.token.claim
4fc90e35-8d3f-4c42-bdc6-a52d408ae277	true	access.token.claim
4fc90e35-8d3f-4c42-bdc6-a52d408ae277	middle_name	claim.name
4fc90e35-8d3f-4c42-bdc6-a52d408ae277	String	jsonType.label
5f2ddead-6c14-470f-9174-2cead8a2cdca	true	userinfo.token.claim
5f2ddead-6c14-470f-9174-2cead8a2cdca	true	id.token.claim
5f2ddead-6c14-470f-9174-2cead8a2cdca	true	access.token.claim
80fbe7c5-a925-4750-816b-0562bb15c384	true	userinfo.token.claim
80fbe7c5-a925-4750-816b-0562bb15c384	username	user.attribute
80fbe7c5-a925-4750-816b-0562bb15c384	true	id.token.claim
80fbe7c5-a925-4750-816b-0562bb15c384	true	access.token.claim
80fbe7c5-a925-4750-816b-0562bb15c384	preferred_username	claim.name
80fbe7c5-a925-4750-816b-0562bb15c384	String	jsonType.label
87cfe205-01b9-44e6-9941-4e1e57fb3f72	true	userinfo.token.claim
87cfe205-01b9-44e6-9941-4e1e57fb3f72	zoneinfo	user.attribute
87cfe205-01b9-44e6-9941-4e1e57fb3f72	true	id.token.claim
87cfe205-01b9-44e6-9941-4e1e57fb3f72	true	access.token.claim
87cfe205-01b9-44e6-9941-4e1e57fb3f72	zoneinfo	claim.name
87cfe205-01b9-44e6-9941-4e1e57fb3f72	String	jsonType.label
8bf9a705-9a22-4e24-bedc-80b3852de533	true	userinfo.token.claim
8bf9a705-9a22-4e24-bedc-80b3852de533	gender	user.attribute
8bf9a705-9a22-4e24-bedc-80b3852de533	true	id.token.claim
8bf9a705-9a22-4e24-bedc-80b3852de533	true	access.token.claim
8bf9a705-9a22-4e24-bedc-80b3852de533	gender	claim.name
8bf9a705-9a22-4e24-bedc-80b3852de533	String	jsonType.label
8e7ace55-8a95-499d-a0ac-2aa80e6f27a5	true	userinfo.token.claim
8e7ace55-8a95-499d-a0ac-2aa80e6f27a5	lastName	user.attribute
8e7ace55-8a95-499d-a0ac-2aa80e6f27a5	true	id.token.claim
8e7ace55-8a95-499d-a0ac-2aa80e6f27a5	true	access.token.claim
8e7ace55-8a95-499d-a0ac-2aa80e6f27a5	family_name	claim.name
8e7ace55-8a95-499d-a0ac-2aa80e6f27a5	String	jsonType.label
97c643e9-9700-49f7-82a8-7318ac4281d8	true	userinfo.token.claim
97c643e9-9700-49f7-82a8-7318ac4281d8	locale	user.attribute
97c643e9-9700-49f7-82a8-7318ac4281d8	true	id.token.claim
97c643e9-9700-49f7-82a8-7318ac4281d8	true	access.token.claim
97c643e9-9700-49f7-82a8-7318ac4281d8	locale	claim.name
97c643e9-9700-49f7-82a8-7318ac4281d8	String	jsonType.label
b70973fe-cb5f-497e-af33-1a9189c92304	true	userinfo.token.claim
b70973fe-cb5f-497e-af33-1a9189c92304	updatedAt	user.attribute
b70973fe-cb5f-497e-af33-1a9189c92304	true	id.token.claim
b70973fe-cb5f-497e-af33-1a9189c92304	true	access.token.claim
b70973fe-cb5f-497e-af33-1a9189c92304	updated_at	claim.name
b70973fe-cb5f-497e-af33-1a9189c92304	long	jsonType.label
d35576e6-0657-4fc3-a1c6-c035ff0a7eaa	true	userinfo.token.claim
d35576e6-0657-4fc3-a1c6-c035ff0a7eaa	birthdate	user.attribute
d35576e6-0657-4fc3-a1c6-c035ff0a7eaa	true	id.token.claim
d35576e6-0657-4fc3-a1c6-c035ff0a7eaa	true	access.token.claim
d35576e6-0657-4fc3-a1c6-c035ff0a7eaa	birthdate	claim.name
d35576e6-0657-4fc3-a1c6-c035ff0a7eaa	String	jsonType.label
eb8bbc22-2b3d-4183-9b28-91ebfd795a84	true	userinfo.token.claim
eb8bbc22-2b3d-4183-9b28-91ebfd795a84	firstName	user.attribute
eb8bbc22-2b3d-4183-9b28-91ebfd795a84	true	id.token.claim
eb8bbc22-2b3d-4183-9b28-91ebfd795a84	true	access.token.claim
eb8bbc22-2b3d-4183-9b28-91ebfd795a84	given_name	claim.name
eb8bbc22-2b3d-4183-9b28-91ebfd795a84	String	jsonType.label
23934d3c-8b2a-464a-90df-97a0734b91a5	true	userinfo.token.claim
23934d3c-8b2a-464a-90df-97a0734b91a5	email	user.attribute
23934d3c-8b2a-464a-90df-97a0734b91a5	true	id.token.claim
23934d3c-8b2a-464a-90df-97a0734b91a5	true	access.token.claim
23934d3c-8b2a-464a-90df-97a0734b91a5	email	claim.name
23934d3c-8b2a-464a-90df-97a0734b91a5	String	jsonType.label
829ee6f6-c7c6-4bd1-a2d3-5a2b078d5fd4	true	userinfo.token.claim
829ee6f6-c7c6-4bd1-a2d3-5a2b078d5fd4	emailVerified	user.attribute
829ee6f6-c7c6-4bd1-a2d3-5a2b078d5fd4	true	id.token.claim
829ee6f6-c7c6-4bd1-a2d3-5a2b078d5fd4	true	access.token.claim
829ee6f6-c7c6-4bd1-a2d3-5a2b078d5fd4	email_verified	claim.name
829ee6f6-c7c6-4bd1-a2d3-5a2b078d5fd4	boolean	jsonType.label
16ade61e-7f60-4883-8fc9-1753e388e80e	formatted	user.attribute.formatted
16ade61e-7f60-4883-8fc9-1753e388e80e	country	user.attribute.country
16ade61e-7f60-4883-8fc9-1753e388e80e	postal_code	user.attribute.postal_code
16ade61e-7f60-4883-8fc9-1753e388e80e	true	userinfo.token.claim
16ade61e-7f60-4883-8fc9-1753e388e80e	street	user.attribute.street
16ade61e-7f60-4883-8fc9-1753e388e80e	true	id.token.claim
16ade61e-7f60-4883-8fc9-1753e388e80e	region	user.attribute.region
16ade61e-7f60-4883-8fc9-1753e388e80e	true	access.token.claim
16ade61e-7f60-4883-8fc9-1753e388e80e	locality	user.attribute.locality
2890acc3-f1e1-40a3-a582-6c2ecce7b449	true	userinfo.token.claim
2890acc3-f1e1-40a3-a582-6c2ecce7b449	phoneNumberVerified	user.attribute
2890acc3-f1e1-40a3-a582-6c2ecce7b449	true	id.token.claim
2890acc3-f1e1-40a3-a582-6c2ecce7b449	true	access.token.claim
2890acc3-f1e1-40a3-a582-6c2ecce7b449	phone_number_verified	claim.name
2890acc3-f1e1-40a3-a582-6c2ecce7b449	boolean	jsonType.label
89a6e626-a103-4f26-a926-61624a94a65f	true	userinfo.token.claim
89a6e626-a103-4f26-a926-61624a94a65f	phoneNumber	user.attribute
89a6e626-a103-4f26-a926-61624a94a65f	true	id.token.claim
89a6e626-a103-4f26-a926-61624a94a65f	true	access.token.claim
89a6e626-a103-4f26-a926-61624a94a65f	phone_number	claim.name
89a6e626-a103-4f26-a926-61624a94a65f	String	jsonType.label
180479f6-13d2-4349-b66f-932f3030bf3b	true	multivalued
180479f6-13d2-4349-b66f-932f3030bf3b	foo	user.attribute
180479f6-13d2-4349-b66f-932f3030bf3b	true	access.token.claim
180479f6-13d2-4349-b66f-932f3030bf3b	resource_access.${client_id}.roles	claim.name
180479f6-13d2-4349-b66f-932f3030bf3b	String	jsonType.label
f2e8f39c-6659-469c-ada8-4350de093333	true	multivalued
f2e8f39c-6659-469c-ada8-4350de093333	foo	user.attribute
f2e8f39c-6659-469c-ada8-4350de093333	true	access.token.claim
f2e8f39c-6659-469c-ada8-4350de093333	realm_access.roles	claim.name
f2e8f39c-6659-469c-ada8-4350de093333	String	jsonType.label
6650597e-33a1-4fb5-8f1b-889ffb3c3255	true	multivalued
6650597e-33a1-4fb5-8f1b-889ffb3c3255	foo	user.attribute
6650597e-33a1-4fb5-8f1b-889ffb3c3255	true	id.token.claim
6650597e-33a1-4fb5-8f1b-889ffb3c3255	true	access.token.claim
6650597e-33a1-4fb5-8f1b-889ffb3c3255	groups	claim.name
6650597e-33a1-4fb5-8f1b-889ffb3c3255	String	jsonType.label
e72cf8ae-3138-460b-85c3-5b84e2e591f7	true	userinfo.token.claim
e72cf8ae-3138-460b-85c3-5b84e2e591f7	username	user.attribute
e72cf8ae-3138-460b-85c3-5b84e2e591f7	true	id.token.claim
e72cf8ae-3138-460b-85c3-5b84e2e591f7	true	access.token.claim
e72cf8ae-3138-460b-85c3-5b84e2e591f7	upn	claim.name
e72cf8ae-3138-460b-85c3-5b84e2e591f7	String	jsonType.label
b9fe5743-8851-463e-aaee-0db538a1eea8	true	id.token.claim
b9fe5743-8851-463e-aaee-0db538a1eea8	true	access.token.claim
402c3f98-2e1a-4d40-b6d2-ec4765f7f738	false	single
402c3f98-2e1a-4d40-b6d2-ec4765f7f738	Basic	attribute.nameformat
402c3f98-2e1a-4d40-b6d2-ec4765f7f738	Role	attribute.name
29139e2f-06e0-4535-9c64-50ffb3cd1e98	formatted	user.attribute.formatted
29139e2f-06e0-4535-9c64-50ffb3cd1e98	country	user.attribute.country
29139e2f-06e0-4535-9c64-50ffb3cd1e98	postal_code	user.attribute.postal_code
29139e2f-06e0-4535-9c64-50ffb3cd1e98	true	userinfo.token.claim
29139e2f-06e0-4535-9c64-50ffb3cd1e98	street	user.attribute.street
29139e2f-06e0-4535-9c64-50ffb3cd1e98	true	id.token.claim
29139e2f-06e0-4535-9c64-50ffb3cd1e98	region	user.attribute.region
29139e2f-06e0-4535-9c64-50ffb3cd1e98	true	access.token.claim
29139e2f-06e0-4535-9c64-50ffb3cd1e98	locality	user.attribute.locality
dd5436a3-c0a8-4dbb-90a7-f78006bed46c	true	id.token.claim
dd5436a3-c0a8-4dbb-90a7-f78006bed46c	true	access.token.claim
dd5436a3-c0a8-4dbb-90a7-f78006bed46c	true	userinfo.token.claim
84d5480f-0bb2-407a-92f5-114afd8c2b68	true	userinfo.token.claim
84d5480f-0bb2-407a-92f5-114afd8c2b68	phoneNumberVerified	user.attribute
84d5480f-0bb2-407a-92f5-114afd8c2b68	true	id.token.claim
84d5480f-0bb2-407a-92f5-114afd8c2b68	true	access.token.claim
84d5480f-0bb2-407a-92f5-114afd8c2b68	phone_number_verified	claim.name
84d5480f-0bb2-407a-92f5-114afd8c2b68	boolean	jsonType.label
8931b4ee-54be-4ffa-b3cf-413173c8e6ec	true	userinfo.token.claim
8931b4ee-54be-4ffa-b3cf-413173c8e6ec	phoneNumber	user.attribute
8931b4ee-54be-4ffa-b3cf-413173c8e6ec	true	id.token.claim
8931b4ee-54be-4ffa-b3cf-413173c8e6ec	true	access.token.claim
8931b4ee-54be-4ffa-b3cf-413173c8e6ec	phone_number	claim.name
8931b4ee-54be-4ffa-b3cf-413173c8e6ec	String	jsonType.label
2119a7d8-28be-4514-9627-4fb1bf482f96	true	userinfo.token.claim
2119a7d8-28be-4514-9627-4fb1bf482f96	email	user.attribute
2119a7d8-28be-4514-9627-4fb1bf482f96	true	id.token.claim
2119a7d8-28be-4514-9627-4fb1bf482f96	true	access.token.claim
2119a7d8-28be-4514-9627-4fb1bf482f96	email	claim.name
2119a7d8-28be-4514-9627-4fb1bf482f96	String	jsonType.label
327ff9a7-b401-4e6a-88eb-87b31b713233	true	userinfo.token.claim
327ff9a7-b401-4e6a-88eb-87b31b713233	emailVerified	user.attribute
327ff9a7-b401-4e6a-88eb-87b31b713233	true	id.token.claim
327ff9a7-b401-4e6a-88eb-87b31b713233	true	access.token.claim
327ff9a7-b401-4e6a-88eb-87b31b713233	email_verified	claim.name
327ff9a7-b401-4e6a-88eb-87b31b713233	boolean	jsonType.label
1d5d06aa-9706-4664-858b-1847a57b4607	foo	user.attribute
1d5d06aa-9706-4664-858b-1847a57b4607	true	access.token.claim
1d5d06aa-9706-4664-858b-1847a57b4607	realm_access.roles	claim.name
1d5d06aa-9706-4664-858b-1847a57b4607	String	jsonType.label
1d5d06aa-9706-4664-858b-1847a57b4607	true	multivalued
4650fe11-2025-4160-a7cc-699905997a64	foo	user.attribute
4650fe11-2025-4160-a7cc-699905997a64	true	access.token.claim
4650fe11-2025-4160-a7cc-699905997a64	resource_access.${client_id}.roles	claim.name
4650fe11-2025-4160-a7cc-699905997a64	String	jsonType.label
4650fe11-2025-4160-a7cc-699905997a64	true	multivalued
08f7accc-f9f1-4442-b5a0-e82319a676ce	true	userinfo.token.claim
08f7accc-f9f1-4442-b5a0-e82319a676ce	firstName	user.attribute
08f7accc-f9f1-4442-b5a0-e82319a676ce	true	id.token.claim
08f7accc-f9f1-4442-b5a0-e82319a676ce	true	access.token.claim
08f7accc-f9f1-4442-b5a0-e82319a676ce	given_name	claim.name
08f7accc-f9f1-4442-b5a0-e82319a676ce	String	jsonType.label
2454e9fb-3c22-4856-8256-df6e75e53e4c	true	userinfo.token.claim
2454e9fb-3c22-4856-8256-df6e75e53e4c	username	user.attribute
2454e9fb-3c22-4856-8256-df6e75e53e4c	true	id.token.claim
2454e9fb-3c22-4856-8256-df6e75e53e4c	true	access.token.claim
2454e9fb-3c22-4856-8256-df6e75e53e4c	preferred_username	claim.name
2454e9fb-3c22-4856-8256-df6e75e53e4c	String	jsonType.label
32b80d10-e8e5-4c07-8d92-0d42d20a33ee	true	userinfo.token.claim
32b80d10-e8e5-4c07-8d92-0d42d20a33ee	lastName	user.attribute
32b80d10-e8e5-4c07-8d92-0d42d20a33ee	true	id.token.claim
32b80d10-e8e5-4c07-8d92-0d42d20a33ee	true	access.token.claim
32b80d10-e8e5-4c07-8d92-0d42d20a33ee	family_name	claim.name
32b80d10-e8e5-4c07-8d92-0d42d20a33ee	String	jsonType.label
3c426bda-6ff9-4338-966f-80d159c41a87	true	userinfo.token.claim
3c426bda-6ff9-4338-966f-80d159c41a87	updatedAt	user.attribute
3c426bda-6ff9-4338-966f-80d159c41a87	true	id.token.claim
3c426bda-6ff9-4338-966f-80d159c41a87	true	access.token.claim
3c426bda-6ff9-4338-966f-80d159c41a87	updated_at	claim.name
3c426bda-6ff9-4338-966f-80d159c41a87	long	jsonType.label
549bb677-54ec-4da9-90e7-43e197cc484e	true	userinfo.token.claim
549bb677-54ec-4da9-90e7-43e197cc484e	nickname	user.attribute
549bb677-54ec-4da9-90e7-43e197cc484e	true	id.token.claim
549bb677-54ec-4da9-90e7-43e197cc484e	true	access.token.claim
549bb677-54ec-4da9-90e7-43e197cc484e	nickname	claim.name
549bb677-54ec-4da9-90e7-43e197cc484e	String	jsonType.label
796e0157-c7b2-4591-a272-26786157c00a	true	userinfo.token.claim
796e0157-c7b2-4591-a272-26786157c00a	birthdate	user.attribute
796e0157-c7b2-4591-a272-26786157c00a	true	id.token.claim
796e0157-c7b2-4591-a272-26786157c00a	true	access.token.claim
796e0157-c7b2-4591-a272-26786157c00a	birthdate	claim.name
796e0157-c7b2-4591-a272-26786157c00a	String	jsonType.label
79e4f284-ba8e-4007-a9c8-18cc54ec5611	true	userinfo.token.claim
79e4f284-ba8e-4007-a9c8-18cc54ec5611	gender	user.attribute
79e4f284-ba8e-4007-a9c8-18cc54ec5611	true	id.token.claim
79e4f284-ba8e-4007-a9c8-18cc54ec5611	true	access.token.claim
79e4f284-ba8e-4007-a9c8-18cc54ec5611	gender	claim.name
79e4f284-ba8e-4007-a9c8-18cc54ec5611	String	jsonType.label
7f9bd9a7-5e53-4a8f-b9e0-8f7debd7fa86	true	userinfo.token.claim
7f9bd9a7-5e53-4a8f-b9e0-8f7debd7fa86	profile	user.attribute
7f9bd9a7-5e53-4a8f-b9e0-8f7debd7fa86	true	id.token.claim
7f9bd9a7-5e53-4a8f-b9e0-8f7debd7fa86	true	access.token.claim
7f9bd9a7-5e53-4a8f-b9e0-8f7debd7fa86	profile	claim.name
7f9bd9a7-5e53-4a8f-b9e0-8f7debd7fa86	String	jsonType.label
8b38e06f-920f-4a75-894c-65c4a863d563	true	userinfo.token.claim
8b38e06f-920f-4a75-894c-65c4a863d563	website	user.attribute
8b38e06f-920f-4a75-894c-65c4a863d563	true	id.token.claim
8b38e06f-920f-4a75-894c-65c4a863d563	true	access.token.claim
8b38e06f-920f-4a75-894c-65c4a863d563	website	claim.name
8b38e06f-920f-4a75-894c-65c4a863d563	String	jsonType.label
acda0d3d-3e02-46b7-9572-649a1f98155d	true	userinfo.token.claim
acda0d3d-3e02-46b7-9572-649a1f98155d	zoneinfo	user.attribute
acda0d3d-3e02-46b7-9572-649a1f98155d	true	id.token.claim
acda0d3d-3e02-46b7-9572-649a1f98155d	true	access.token.claim
acda0d3d-3e02-46b7-9572-649a1f98155d	zoneinfo	claim.name
acda0d3d-3e02-46b7-9572-649a1f98155d	String	jsonType.label
b6d683b8-d99a-42e6-8799-bcd8a301b0e0	true	userinfo.token.claim
b6d683b8-d99a-42e6-8799-bcd8a301b0e0	middleName	user.attribute
b6d683b8-d99a-42e6-8799-bcd8a301b0e0	true	id.token.claim
b6d683b8-d99a-42e6-8799-bcd8a301b0e0	true	access.token.claim
b6d683b8-d99a-42e6-8799-bcd8a301b0e0	middle_name	claim.name
b6d683b8-d99a-42e6-8799-bcd8a301b0e0	String	jsonType.label
c5255b5a-a344-42bc-873a-2150154976de	true	userinfo.token.claim
c5255b5a-a344-42bc-873a-2150154976de	picture	user.attribute
c5255b5a-a344-42bc-873a-2150154976de	true	id.token.claim
c5255b5a-a344-42bc-873a-2150154976de	true	access.token.claim
c5255b5a-a344-42bc-873a-2150154976de	picture	claim.name
c5255b5a-a344-42bc-873a-2150154976de	String	jsonType.label
dd2cd5f9-7deb-4e2d-b4bf-887092495130	true	userinfo.token.claim
dd2cd5f9-7deb-4e2d-b4bf-887092495130	locale	user.attribute
dd2cd5f9-7deb-4e2d-b4bf-887092495130	true	id.token.claim
dd2cd5f9-7deb-4e2d-b4bf-887092495130	true	access.token.claim
dd2cd5f9-7deb-4e2d-b4bf-887092495130	locale	claim.name
dd2cd5f9-7deb-4e2d-b4bf-887092495130	String	jsonType.label
f3fe48ec-cf97-4e2d-a6c9-1faab3ce9df1	true	id.token.claim
f3fe48ec-cf97-4e2d-a6c9-1faab3ce9df1	true	access.token.claim
f3fe48ec-cf97-4e2d-a6c9-1faab3ce9df1	true	userinfo.token.claim
43cf9b96-4649-47ab-af19-624a4e7ea1aa	true	multivalued
43cf9b96-4649-47ab-af19-624a4e7ea1aa	true	userinfo.token.claim
43cf9b96-4649-47ab-af19-624a4e7ea1aa	foo	user.attribute
43cf9b96-4649-47ab-af19-624a4e7ea1aa	true	id.token.claim
43cf9b96-4649-47ab-af19-624a4e7ea1aa	true	access.token.claim
43cf9b96-4649-47ab-af19-624a4e7ea1aa	groups	claim.name
43cf9b96-4649-47ab-af19-624a4e7ea1aa	String	jsonType.label
71790784-f0ea-43c1-b75c-d4cb2dd8129a	true	userinfo.token.claim
71790784-f0ea-43c1-b75c-d4cb2dd8129a	username	user.attribute
71790784-f0ea-43c1-b75c-d4cb2dd8129a	true	id.token.claim
71790784-f0ea-43c1-b75c-d4cb2dd8129a	true	access.token.claim
71790784-f0ea-43c1-b75c-d4cb2dd8129a	upn	claim.name
71790784-f0ea-43c1-b75c-d4cb2dd8129a	String	jsonType.label
8717e63d-ab28-46e3-9ea1-ab05f5e9f4ea	true	userinfo.token.claim
8717e63d-ab28-46e3-9ea1-ab05f5e9f4ea	locale	user.attribute
8717e63d-ab28-46e3-9ea1-ab05f5e9f4ea	true	id.token.claim
8717e63d-ab28-46e3-9ea1-ab05f5e9f4ea	true	access.token.claim
8717e63d-ab28-46e3-9ea1-ab05f5e9f4ea	locale	claim.name
8717e63d-ab28-46e3-9ea1-ab05f5e9f4ea	String	jsonType.label
\.


--
-- Data for Name: realm; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.realm (id, access_code_lifespan, user_action_lifespan, access_token_lifespan, account_theme, admin_theme, email_theme, enabled, events_enabled, events_expiration, login_theme, name, not_before, password_policy, registration_allowed, remember_me, reset_password_allowed, social, ssl_required, sso_idle_timeout, sso_max_lifespan, update_profile_on_soc_login, verify_email, master_admin_client, login_lifespan, internationalization_enabled, default_locale, reg_email_as_username, admin_events_enabled, admin_events_details_enabled, edit_username_allowed, otp_policy_counter, otp_policy_window, otp_policy_period, otp_policy_digits, otp_policy_alg, otp_policy_type, browser_flow, registration_flow, direct_grant_flow, reset_credentials_flow, client_auth_flow, offline_session_idle_timeout, revoke_refresh_token, access_token_life_implicit, login_with_email_allowed, duplicate_emails_allowed, docker_auth_flow, refresh_token_max_reuse, allow_user_managed_access, sso_max_lifespan_remember_me, sso_idle_timeout_remember_me, default_role) FROM stdin;
19e687a7-a4bd-4974-9623-d44f5d246eb1	60	300	60	\N	\N	\N	t	f	0	\N	master	0	\N	f	f	f	f	EXTERNAL	1800	36000	f	f	3ff497aa-8999-44cd-af9f-17d098e04a42	1800	f	\N	f	f	f	f	0	1	30	6	HmacSHA1	totp	b8e5713a-a3da-4ecd-9dd8-e567ce0b3a5a	f5b0f0fc-55cb-4a45-997e-fa1c0c8b452a	2d037f8c-ea48-4173-80f6-c67cd33788a9	f36063ba-cae3-4ba6-b537-e7820d44088d	f3abbf9a-321e-43b6-add0-f4cc33fc1a68	2592000	f	900	t	f	b9efbeb9-9d2c-4f7e-90c2-91c34d2264ce	0	f	0	0	7f34dbbf-9030-4de5-9c21-c6715f36c748
8211ffad-2ece-45e2-be4d-9509ea27c3d5	60	300	300	keycloak.v2	keycloak.v2	keycloak	t	f	0	fragjetzt	fragjetzt	0	length(8) and passwordHistory(3) and notUsername(undefined) and notEmail(undefined) and hashIterations(27501) and hashAlgorithm(pbkdf2-sha256) and forceExpiredPasswordChange(180) and maxLength(64) and upperCase(1) and specialChars(1) and lowerCase(1) and digits(1)	t	t	t	f	EXTERNAL	1800	36000	f	t	e0df3a5b-d8f3-4641-af97-e2649ab5b9ab	1800	t	en	t	f	f	t	0	1	30	6	HmacSHA1	totp	b705ebeb-91ec-42b4-8072-c33b24ae8868	83645a93-dfe1-496e-ab3c-387221c38e94	787d2a18-62ab-4853-a171-e75548103b79	42da4271-4d88-4aea-9a8d-f5bca266788e	57fc7d69-26a6-4815-95ef-84c4bd3b1f64	2592000	f	900	t	f	91484abe-cb9b-48e8-b783-159fb070059d	0	f	0	0	aa1e1645-5ddc-4219-84fd-42b806510732
\.


--
-- Data for Name: realm_attribute; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.realm_attribute (name, realm_id, value) FROM stdin;
_browser_header.contentSecurityPolicyReportOnly	19e687a7-a4bd-4974-9623-d44f5d246eb1	
_browser_header.xContentTypeOptions	19e687a7-a4bd-4974-9623-d44f5d246eb1	nosniff
_browser_header.referrerPolicy	19e687a7-a4bd-4974-9623-d44f5d246eb1	no-referrer
_browser_header.xRobotsTag	19e687a7-a4bd-4974-9623-d44f5d246eb1	none
_browser_header.xFrameOptions	19e687a7-a4bd-4974-9623-d44f5d246eb1	SAMEORIGIN
_browser_header.contentSecurityPolicy	19e687a7-a4bd-4974-9623-d44f5d246eb1	frame-src 'self'; frame-ancestors 'self'; object-src 'none';
_browser_header.xXSSProtection	19e687a7-a4bd-4974-9623-d44f5d246eb1	1; mode=block
_browser_header.strictTransportSecurity	19e687a7-a4bd-4974-9623-d44f5d246eb1	max-age=31536000; includeSubDomains
bruteForceProtected	19e687a7-a4bd-4974-9623-d44f5d246eb1	false
permanentLockout	19e687a7-a4bd-4974-9623-d44f5d246eb1	false
maxFailureWaitSeconds	19e687a7-a4bd-4974-9623-d44f5d246eb1	900
minimumQuickLoginWaitSeconds	19e687a7-a4bd-4974-9623-d44f5d246eb1	60
waitIncrementSeconds	19e687a7-a4bd-4974-9623-d44f5d246eb1	60
quickLoginCheckMilliSeconds	19e687a7-a4bd-4974-9623-d44f5d246eb1	1000
maxDeltaTimeSeconds	19e687a7-a4bd-4974-9623-d44f5d246eb1	43200
failureFactor	19e687a7-a4bd-4974-9623-d44f5d246eb1	30
realmReusableOtpCode	19e687a7-a4bd-4974-9623-d44f5d246eb1	false
displayName	19e687a7-a4bd-4974-9623-d44f5d246eb1	Keycloak
displayNameHtml	19e687a7-a4bd-4974-9623-d44f5d246eb1	<div class="kc-logo-text"><span>Keycloak</span></div>
defaultSignatureAlgorithm	19e687a7-a4bd-4974-9623-d44f5d246eb1	RS256
offlineSessionMaxLifespanEnabled	19e687a7-a4bd-4974-9623-d44f5d246eb1	false
offlineSessionMaxLifespan	19e687a7-a4bd-4974-9623-d44f5d246eb1	5184000
_browser_header.contentSecurityPolicyReportOnly	8211ffad-2ece-45e2-be4d-9509ea27c3d5	
_browser_header.xContentTypeOptions	8211ffad-2ece-45e2-be4d-9509ea27c3d5	nosniff
_browser_header.referrerPolicy	8211ffad-2ece-45e2-be4d-9509ea27c3d5	no-referrer
_browser_header.xRobotsTag	8211ffad-2ece-45e2-be4d-9509ea27c3d5	none
_browser_header.xFrameOptions	8211ffad-2ece-45e2-be4d-9509ea27c3d5	SAMEORIGIN
_browser_header.contentSecurityPolicy	8211ffad-2ece-45e2-be4d-9509ea27c3d5	frame-src 'self'; frame-ancestors 'self'; object-src 'none';
_browser_header.xXSSProtection	8211ffad-2ece-45e2-be4d-9509ea27c3d5	1; mode=block
_browser_header.strictTransportSecurity	8211ffad-2ece-45e2-be4d-9509ea27c3d5	max-age=31536000; includeSubDomains
bruteForceProtected	8211ffad-2ece-45e2-be4d-9509ea27c3d5	false
permanentLockout	8211ffad-2ece-45e2-be4d-9509ea27c3d5	false
maxFailureWaitSeconds	8211ffad-2ece-45e2-be4d-9509ea27c3d5	900
minimumQuickLoginWaitSeconds	8211ffad-2ece-45e2-be4d-9509ea27c3d5	60
waitIncrementSeconds	8211ffad-2ece-45e2-be4d-9509ea27c3d5	60
quickLoginCheckMilliSeconds	8211ffad-2ece-45e2-be4d-9509ea27c3d5	1000
maxDeltaTimeSeconds	8211ffad-2ece-45e2-be4d-9509ea27c3d5	43200
failureFactor	8211ffad-2ece-45e2-be4d-9509ea27c3d5	30
realmReusableOtpCode	8211ffad-2ece-45e2-be4d-9509ea27c3d5	false
displayName	8211ffad-2ece-45e2-be4d-9509ea27c3d5	frag.jetzt
displayNameHtml	8211ffad-2ece-45e2-be4d-9509ea27c3d5	<span id="header-title">frag.jetzt</span><br/><span id="header-subtitle">& ChatGPT</span>
defaultSignatureAlgorithm	8211ffad-2ece-45e2-be4d-9509ea27c3d5	RS256
offlineSessionMaxLifespanEnabled	8211ffad-2ece-45e2-be4d-9509ea27c3d5	false
offlineSessionMaxLifespan	8211ffad-2ece-45e2-be4d-9509ea27c3d5	5184000
clientSessionIdleTimeout	8211ffad-2ece-45e2-be4d-9509ea27c3d5	0
clientSessionMaxLifespan	8211ffad-2ece-45e2-be4d-9509ea27c3d5	0
clientOfflineSessionIdleTimeout	8211ffad-2ece-45e2-be4d-9509ea27c3d5	0
clientOfflineSessionMaxLifespan	8211ffad-2ece-45e2-be4d-9509ea27c3d5	0
actionTokenGeneratedByAdminLifespan	8211ffad-2ece-45e2-be4d-9509ea27c3d5	43200
actionTokenGeneratedByUserLifespan	8211ffad-2ece-45e2-be4d-9509ea27c3d5	300
oauth2DeviceCodeLifespan	8211ffad-2ece-45e2-be4d-9509ea27c3d5	600
oauth2DevicePollingInterval	8211ffad-2ece-45e2-be4d-9509ea27c3d5	5
webAuthnPolicyRpEntityName	8211ffad-2ece-45e2-be4d-9509ea27c3d5	frag.jetzt
webAuthnPolicySignatureAlgorithms	8211ffad-2ece-45e2-be4d-9509ea27c3d5	ES256
webAuthnPolicyRpId	8211ffad-2ece-45e2-be4d-9509ea27c3d5	
webAuthnPolicyAttestationConveyancePreference	8211ffad-2ece-45e2-be4d-9509ea27c3d5	not specified
webAuthnPolicyAuthenticatorAttachment	8211ffad-2ece-45e2-be4d-9509ea27c3d5	not specified
webAuthnPolicyRequireResidentKey	8211ffad-2ece-45e2-be4d-9509ea27c3d5	not specified
webAuthnPolicyUserVerificationRequirement	8211ffad-2ece-45e2-be4d-9509ea27c3d5	not specified
webAuthnPolicyCreateTimeout	8211ffad-2ece-45e2-be4d-9509ea27c3d5	0
webAuthnPolicyAvoidSameAuthenticatorRegister	8211ffad-2ece-45e2-be4d-9509ea27c3d5	false
webAuthnPolicyRpEntityNamePasswordless	8211ffad-2ece-45e2-be4d-9509ea27c3d5	frag.jetzt
webAuthnPolicySignatureAlgorithmsPasswordless	8211ffad-2ece-45e2-be4d-9509ea27c3d5	ES256
webAuthnPolicyRpIdPasswordless	8211ffad-2ece-45e2-be4d-9509ea27c3d5	
webAuthnPolicyAttestationConveyancePreferencePasswordless	8211ffad-2ece-45e2-be4d-9509ea27c3d5	not specified
webAuthnPolicyAuthenticatorAttachmentPasswordless	8211ffad-2ece-45e2-be4d-9509ea27c3d5	not specified
webAuthnPolicyRequireResidentKeyPasswordless	8211ffad-2ece-45e2-be4d-9509ea27c3d5	Yes
webAuthnPolicyUserVerificationRequirementPasswordless	8211ffad-2ece-45e2-be4d-9509ea27c3d5	not specified
webAuthnPolicyCreateTimeoutPasswordless	8211ffad-2ece-45e2-be4d-9509ea27c3d5	0
webAuthnPolicyAvoidSameAuthenticatorRegisterPasswordless	8211ffad-2ece-45e2-be4d-9509ea27c3d5	true
cibaBackchannelTokenDeliveryMode	8211ffad-2ece-45e2-be4d-9509ea27c3d5	poll
cibaExpiresIn	8211ffad-2ece-45e2-be4d-9509ea27c3d5	120
cibaInterval	8211ffad-2ece-45e2-be4d-9509ea27c3d5	5
cibaAuthRequestedUserHint	8211ffad-2ece-45e2-be4d-9509ea27c3d5	login_hint
parRequestUriLifespan	8211ffad-2ece-45e2-be4d-9509ea27c3d5	60
frontendUrl	8211ffad-2ece-45e2-be4d-9509ea27c3d5	
acr.loa.map	8211ffad-2ece-45e2-be4d-9509ea27c3d5	{}
client-policies.profiles	8211ffad-2ece-45e2-be4d-9509ea27c3d5	{"profiles":[]}
client-policies.policies	8211ffad-2ece-45e2-be4d-9509ea27c3d5	{"policies":[]}
\.


--
-- Data for Name: realm_default_groups; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.realm_default_groups (realm_id, group_id) FROM stdin;
8211ffad-2ece-45e2-be4d-9509ea27c3d5	b4737f4e-349d-42a1-8149-e4cf40b06171
\.


--
-- Data for Name: realm_enabled_event_types; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.realm_enabled_event_types (realm_id, value) FROM stdin;
8211ffad-2ece-45e2-be4d-9509ea27c3d5	UPDATE_CONSENT_ERROR
8211ffad-2ece-45e2-be4d-9509ea27c3d5	SEND_RESET_PASSWORD
8211ffad-2ece-45e2-be4d-9509ea27c3d5	GRANT_CONSENT
8211ffad-2ece-45e2-be4d-9509ea27c3d5	VERIFY_PROFILE_ERROR
8211ffad-2ece-45e2-be4d-9509ea27c3d5	UPDATE_TOTP
8211ffad-2ece-45e2-be4d-9509ea27c3d5	REMOVE_TOTP
8211ffad-2ece-45e2-be4d-9509ea27c3d5	REVOKE_GRANT
8211ffad-2ece-45e2-be4d-9509ea27c3d5	LOGIN_ERROR
8211ffad-2ece-45e2-be4d-9509ea27c3d5	CLIENT_LOGIN
8211ffad-2ece-45e2-be4d-9509ea27c3d5	RESET_PASSWORD_ERROR
8211ffad-2ece-45e2-be4d-9509ea27c3d5	IMPERSONATE_ERROR
8211ffad-2ece-45e2-be4d-9509ea27c3d5	CODE_TO_TOKEN_ERROR
8211ffad-2ece-45e2-be4d-9509ea27c3d5	CUSTOM_REQUIRED_ACTION
8211ffad-2ece-45e2-be4d-9509ea27c3d5	OAUTH2_DEVICE_CODE_TO_TOKEN_ERROR
8211ffad-2ece-45e2-be4d-9509ea27c3d5	RESTART_AUTHENTICATION
8211ffad-2ece-45e2-be4d-9509ea27c3d5	UPDATE_PROFILE_ERROR
8211ffad-2ece-45e2-be4d-9509ea27c3d5	IMPERSONATE
8211ffad-2ece-45e2-be4d-9509ea27c3d5	LOGIN
8211ffad-2ece-45e2-be4d-9509ea27c3d5	UPDATE_PASSWORD_ERROR
8211ffad-2ece-45e2-be4d-9509ea27c3d5	OAUTH2_DEVICE_VERIFY_USER_CODE
8211ffad-2ece-45e2-be4d-9509ea27c3d5	CLIENT_INITIATED_ACCOUNT_LINKING
8211ffad-2ece-45e2-be4d-9509ea27c3d5	TOKEN_EXCHANGE
8211ffad-2ece-45e2-be4d-9509ea27c3d5	REGISTER
8211ffad-2ece-45e2-be4d-9509ea27c3d5	LOGOUT
8211ffad-2ece-45e2-be4d-9509ea27c3d5	AUTHREQID_TO_TOKEN
8211ffad-2ece-45e2-be4d-9509ea27c3d5	DELETE_ACCOUNT_ERROR
8211ffad-2ece-45e2-be4d-9509ea27c3d5	CLIENT_REGISTER
8211ffad-2ece-45e2-be4d-9509ea27c3d5	IDENTITY_PROVIDER_LINK_ACCOUNT
8211ffad-2ece-45e2-be4d-9509ea27c3d5	UPDATE_PASSWORD
8211ffad-2ece-45e2-be4d-9509ea27c3d5	DELETE_ACCOUNT
8211ffad-2ece-45e2-be4d-9509ea27c3d5	FEDERATED_IDENTITY_LINK_ERROR
8211ffad-2ece-45e2-be4d-9509ea27c3d5	CLIENT_DELETE
8211ffad-2ece-45e2-be4d-9509ea27c3d5	IDENTITY_PROVIDER_FIRST_LOGIN
8211ffad-2ece-45e2-be4d-9509ea27c3d5	VERIFY_EMAIL
8211ffad-2ece-45e2-be4d-9509ea27c3d5	CLIENT_DELETE_ERROR
8211ffad-2ece-45e2-be4d-9509ea27c3d5	CLIENT_LOGIN_ERROR
8211ffad-2ece-45e2-be4d-9509ea27c3d5	RESTART_AUTHENTICATION_ERROR
8211ffad-2ece-45e2-be4d-9509ea27c3d5	REMOVE_FEDERATED_IDENTITY_ERROR
8211ffad-2ece-45e2-be4d-9509ea27c3d5	EXECUTE_ACTIONS
8211ffad-2ece-45e2-be4d-9509ea27c3d5	TOKEN_EXCHANGE_ERROR
8211ffad-2ece-45e2-be4d-9509ea27c3d5	PERMISSION_TOKEN
8211ffad-2ece-45e2-be4d-9509ea27c3d5	SEND_IDENTITY_PROVIDER_LINK_ERROR
8211ffad-2ece-45e2-be4d-9509ea27c3d5	EXECUTE_ACTION_TOKEN_ERROR
8211ffad-2ece-45e2-be4d-9509ea27c3d5	SEND_VERIFY_EMAIL
8211ffad-2ece-45e2-be4d-9509ea27c3d5	OAUTH2_DEVICE_AUTH
8211ffad-2ece-45e2-be4d-9509ea27c3d5	EXECUTE_ACTIONS_ERROR
8211ffad-2ece-45e2-be4d-9509ea27c3d5	REMOVE_FEDERATED_IDENTITY
8211ffad-2ece-45e2-be4d-9509ea27c3d5	OAUTH2_DEVICE_CODE_TO_TOKEN
8211ffad-2ece-45e2-be4d-9509ea27c3d5	IDENTITY_PROVIDER_POST_LOGIN
8211ffad-2ece-45e2-be4d-9509ea27c3d5	IDENTITY_PROVIDER_LINK_ACCOUNT_ERROR
8211ffad-2ece-45e2-be4d-9509ea27c3d5	UPDATE_EMAIL
8211ffad-2ece-45e2-be4d-9509ea27c3d5	OAUTH2_DEVICE_VERIFY_USER_CODE_ERROR
8211ffad-2ece-45e2-be4d-9509ea27c3d5	REGISTER_ERROR
8211ffad-2ece-45e2-be4d-9509ea27c3d5	REVOKE_GRANT_ERROR
8211ffad-2ece-45e2-be4d-9509ea27c3d5	LOGOUT_ERROR
8211ffad-2ece-45e2-be4d-9509ea27c3d5	UPDATE_EMAIL_ERROR
8211ffad-2ece-45e2-be4d-9509ea27c3d5	EXECUTE_ACTION_TOKEN
8211ffad-2ece-45e2-be4d-9509ea27c3d5	CLIENT_UPDATE_ERROR
8211ffad-2ece-45e2-be4d-9509ea27c3d5	UPDATE_PROFILE
8211ffad-2ece-45e2-be4d-9509ea27c3d5	AUTHREQID_TO_TOKEN_ERROR
8211ffad-2ece-45e2-be4d-9509ea27c3d5	FEDERATED_IDENTITY_LINK
8211ffad-2ece-45e2-be4d-9509ea27c3d5	CLIENT_REGISTER_ERROR
8211ffad-2ece-45e2-be4d-9509ea27c3d5	SEND_VERIFY_EMAIL_ERROR
8211ffad-2ece-45e2-be4d-9509ea27c3d5	SEND_IDENTITY_PROVIDER_LINK
8211ffad-2ece-45e2-be4d-9509ea27c3d5	RESET_PASSWORD
8211ffad-2ece-45e2-be4d-9509ea27c3d5	CLIENT_INITIATED_ACCOUNT_LINKING_ERROR
8211ffad-2ece-45e2-be4d-9509ea27c3d5	OAUTH2_DEVICE_AUTH_ERROR
8211ffad-2ece-45e2-be4d-9509ea27c3d5	UPDATE_CONSENT
8211ffad-2ece-45e2-be4d-9509ea27c3d5	REMOVE_TOTP_ERROR
8211ffad-2ece-45e2-be4d-9509ea27c3d5	VERIFY_EMAIL_ERROR
8211ffad-2ece-45e2-be4d-9509ea27c3d5	SEND_RESET_PASSWORD_ERROR
8211ffad-2ece-45e2-be4d-9509ea27c3d5	CLIENT_UPDATE
8211ffad-2ece-45e2-be4d-9509ea27c3d5	IDENTITY_PROVIDER_POST_LOGIN_ERROR
8211ffad-2ece-45e2-be4d-9509ea27c3d5	CUSTOM_REQUIRED_ACTION_ERROR
8211ffad-2ece-45e2-be4d-9509ea27c3d5	UPDATE_TOTP_ERROR
8211ffad-2ece-45e2-be4d-9509ea27c3d5	CODE_TO_TOKEN
8211ffad-2ece-45e2-be4d-9509ea27c3d5	VERIFY_PROFILE
8211ffad-2ece-45e2-be4d-9509ea27c3d5	GRANT_CONSENT_ERROR
8211ffad-2ece-45e2-be4d-9509ea27c3d5	IDENTITY_PROVIDER_FIRST_LOGIN_ERROR
\.


--
-- Data for Name: realm_events_listeners; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.realm_events_listeners (realm_id, value) FROM stdin;
19e687a7-a4bd-4974-9623-d44f5d246eb1	jboss-logging
8211ffad-2ece-45e2-be4d-9509ea27c3d5	jboss-logging
8211ffad-2ece-45e2-be4d-9509ea27c3d5	frag_jetzt_keycloak_plugin
\.


--
-- Data for Name: realm_localizations; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.realm_localizations (realm_id, locale, texts) FROM stdin;
\.


--
-- Data for Name: realm_required_credential; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.realm_required_credential (type, form_label, input, secret, realm_id) FROM stdin;
password	password	t	t	19e687a7-a4bd-4974-9623-d44f5d246eb1
password	password	t	t	8211ffad-2ece-45e2-be4d-9509ea27c3d5
\.


--
-- Data for Name: realm_smtp_config; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.realm_smtp_config (realm_id, value, name) FROM stdin;
8211ffad-2ece-45e2-be4d-9509ea27c3d5	**********	password
8211ffad-2ece-45e2-be4d-9509ea27c3d5		replyToDisplayName
8211ffad-2ece-45e2-be4d-9509ea27c3d5	false	starttls
8211ffad-2ece-45e2-be4d-9509ea27c3d5	false	auth
8211ffad-2ece-45e2-be4d-9509ea27c3d5	fragjetzt-mailhog	host
8211ffad-2ece-45e2-be4d-9509ea27c3d5		replyTo
8211ffad-2ece-45e2-be4d-9509ea27c3d5	postmaster@localhost	from
8211ffad-2ece-45e2-be4d-9509ea27c3d5	frag.jetzt	fromDisplayName
8211ffad-2ece-45e2-be4d-9509ea27c3d5		envelopeFrom
8211ffad-2ece-45e2-be4d-9509ea27c3d5	false	ssl
8211ffad-2ece-45e2-be4d-9509ea27c3d5		user
\.


--
-- Data for Name: realm_supported_locales; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.realm_supported_locales (realm_id, value) FROM stdin;
8211ffad-2ece-45e2-be4d-9509ea27c3d5	de
8211ffad-2ece-45e2-be4d-9509ea27c3d5	en
8211ffad-2ece-45e2-be4d-9509ea27c3d5	fr
\.


--
-- Data for Name: redirect_uris; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.redirect_uris (client_id, value) FROM stdin;
adec04de-0d89-4b02-a17c-85c2a535c226	/realms/master/account/*
70fbcc0a-f28d-4e1e-a96f-cf3ce4433149	/realms/master/account/*
79c8f459-6cb5-43cf-9527-51d3489cf8ac	/admin/master/console/*
a6e6b6b5-1888-44a4-892d-202b54ae2459	/realms/fragjetzt/account/*
ad527668-535b-41b0-bfcd-3bbb4a3e684e	/realms/fragjetzt/account/*
472187cb-354b-42de-acad-b0d8f8a9c031	http://localhost:4200/*
f41d3566-a7d2-4326-8425-4c9ce085d282	/admin/fragjetzt/console/*
\.


--
-- Data for Name: required_action_config; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.required_action_config (required_action_id, value, name) FROM stdin;
\.


--
-- Data for Name: required_action_provider; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.required_action_provider (id, alias, name, realm_id, enabled, default_action, provider_id, priority) FROM stdin;
9065a7df-773e-4ffe-a074-522453abe5d0	VERIFY_EMAIL	Verify Email	19e687a7-a4bd-4974-9623-d44f5d246eb1	t	f	VERIFY_EMAIL	50
345389e6-ce1d-4420-807b-12fc05f1a3a0	UPDATE_PROFILE	Update Profile	19e687a7-a4bd-4974-9623-d44f5d246eb1	t	f	UPDATE_PROFILE	40
260d4222-61e8-400f-8cdd-3fbfe02ead3f	CONFIGURE_TOTP	Configure OTP	19e687a7-a4bd-4974-9623-d44f5d246eb1	t	f	CONFIGURE_TOTP	10
e3ec3545-8d5a-4bb3-901a-feed45ae08d7	UPDATE_PASSWORD	Update Password	19e687a7-a4bd-4974-9623-d44f5d246eb1	t	f	UPDATE_PASSWORD	30
71ba3a36-50b5-4368-88e5-f0838ba1cce3	TERMS_AND_CONDITIONS	Terms and Conditions	19e687a7-a4bd-4974-9623-d44f5d246eb1	f	f	TERMS_AND_CONDITIONS	20
b5811a54-be06-4e64-83cf-2ad64b4f19be	delete_account	Delete Account	19e687a7-a4bd-4974-9623-d44f5d246eb1	f	f	delete_account	60
871261f2-d841-433e-acb5-aab00c0d72c9	update_user_locale	Update User Locale	19e687a7-a4bd-4974-9623-d44f5d246eb1	t	f	update_user_locale	1000
e51183d1-b45c-4d52-b6c5-deab3e78bf61	webauthn-register	Webauthn Register	19e687a7-a4bd-4974-9623-d44f5d246eb1	t	f	webauthn-register	70
9e297ed3-6e1a-4d83-83f5-c6bee7c74f89	webauthn-register-passwordless	Webauthn Register Passwordless	19e687a7-a4bd-4974-9623-d44f5d246eb1	t	f	webauthn-register-passwordless	80
8967c9d2-85d4-452a-bf25-078e7fb14622	UPDATE_PROFILE	Update Profile	8211ffad-2ece-45e2-be4d-9509ea27c3d5	t	f	UPDATE_PROFILE	10
e425cf8f-f120-46df-a346-254f2ae64c87	CONFIGURE_TOTP	Configure OTP	8211ffad-2ece-45e2-be4d-9509ea27c3d5	t	f	CONFIGURE_TOTP	20
184031c4-8e15-4a8c-bbc0-d421cc8ef9c9	TERMS_AND_CONDITIONS	Terms and Conditions	8211ffad-2ece-45e2-be4d-9509ea27c3d5	f	t	TERMS_AND_CONDITIONS	30
16e5a416-d9be-46a0-9841-83d623cc156c	UPDATE_PASSWORD	Update Password	8211ffad-2ece-45e2-be4d-9509ea27c3d5	t	f	UPDATE_PASSWORD	40
14d61531-84ba-4d56-9b3c-076a45a5b578	VERIFY_EMAIL	Verify Email	8211ffad-2ece-45e2-be4d-9509ea27c3d5	t	f	VERIFY_EMAIL	50
b5fa39fa-78d4-4ba6-8f14-7cdb09a938cd	delete_account	Delete Account	8211ffad-2ece-45e2-be4d-9509ea27c3d5	t	f	delete_account	60
eac9469c-7ec7-4352-b8d3-a67b97f962d1	webauthn-register	Webauthn Register	8211ffad-2ece-45e2-be4d-9509ea27c3d5	t	f	webauthn-register	70
efd42edf-1705-4f2d-9a54-752b263b3bab	webauthn-register-passwordless	Webauthn Register Passwordless	8211ffad-2ece-45e2-be4d-9509ea27c3d5	t	f	webauthn-register-passwordless	80
55db302f-e3df-415f-8ec6-dc03cb8b7201	update_user_locale	Update User Locale	8211ffad-2ece-45e2-be4d-9509ea27c3d5	t	f	update_user_locale	1000
947d1f7a-88f5-4dad-822f-f5346663ac4d	VERIFY_PROFILE	Verify Profile	8211ffad-2ece-45e2-be4d-9509ea27c3d5	f	f	VERIFY_PROFILE	1001
\.


--
-- Data for Name: resource_attribute; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.resource_attribute (id, name, value, resource_id) FROM stdin;
\.


--
-- Data for Name: resource_policy; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.resource_policy (resource_id, policy_id) FROM stdin;
\.


--
-- Data for Name: resource_scope; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.resource_scope (resource_id, scope_id) FROM stdin;
\.


--
-- Data for Name: resource_server; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.resource_server (id, allow_rs_remote_mgmt, policy_enforce_mode, decision_strategy) FROM stdin;
\.


--
-- Data for Name: resource_server_perm_ticket; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.resource_server_perm_ticket (id, owner, requester, created_timestamp, granted_timestamp, resource_id, scope_id, resource_server_id, policy_id) FROM stdin;
\.


--
-- Data for Name: resource_server_policy; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.resource_server_policy (id, name, description, type, decision_strategy, logic, resource_server_id, owner) FROM stdin;
\.


--
-- Data for Name: resource_server_resource; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.resource_server_resource (id, name, type, icon_uri, owner, resource_server_id, owner_managed_access, display_name) FROM stdin;
\.


--
-- Data for Name: resource_server_scope; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.resource_server_scope (id, name, icon_uri, resource_server_id, display_name) FROM stdin;
\.


--
-- Data for Name: resource_uris; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.resource_uris (resource_id, value) FROM stdin;
\.


--
-- Data for Name: role_attribute; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.role_attribute (id, role_id, name, value) FROM stdin;
\.


--
-- Data for Name: scope_mapping; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.scope_mapping (client_id, role_id) FROM stdin;
70fbcc0a-f28d-4e1e-a96f-cf3ce4433149	1aaa5633-ffd3-45e8-b4a2-5307ce2ae074
70fbcc0a-f28d-4e1e-a96f-cf3ce4433149	a27bfda4-4509-454a-b7c4-011054f7bf5e
ad527668-535b-41b0-bfcd-3bbb4a3e684e	e2af2b0a-21d4-4a26-a545-8ecbeca6bb81
ad527668-535b-41b0-bfcd-3bbb4a3e684e	6c2c1357-abd6-402e-925d-b0fe4f60027c
\.


--
-- Data for Name: scope_policy; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.scope_policy (scope_id, policy_id) FROM stdin;
\.


--
-- Data for Name: user_attribute; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.user_attribute (name, value, user_id, id) FROM stdin;
\.


--
-- Data for Name: user_consent; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.user_consent (id, client_id, user_id, created_date, last_updated_date, client_storage_provider, external_client_id) FROM stdin;
\.


--
-- Data for Name: user_consent_client_scope; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.user_consent_client_scope (user_consent_id, scope_id) FROM stdin;
\.


--
-- Data for Name: user_entity; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.user_entity (id, email, email_constraint, email_verified, enabled, federation_link, first_name, last_name, realm_id, username, created_timestamp, service_account_client_link, not_before) FROM stdin;
4ee0ebb9-ab98-4fe7-bfe1-d44727a39c4f	\N	c52b12ff-777c-44ec-9a18-47a6c98ec502	f	t	\N	\N	\N	19e687a7-a4bd-4974-9623-d44f5d246eb1	admin	1712826603372	\N	0
a82979e2-0863-4dea-8055-b3b93803c90b	admin@admin	admin@admin	t	t	\N			8211ffad-2ece-45e2-be4d-9509ea27c3d5	admin@admin	1712827018530	\N	0
\.


--
-- Data for Name: user_federation_config; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.user_federation_config (user_federation_provider_id, value, name) FROM stdin;
\.


--
-- Data for Name: user_federation_mapper; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.user_federation_mapper (id, name, federation_provider_id, federation_mapper_type, realm_id) FROM stdin;
\.


--
-- Data for Name: user_federation_mapper_config; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.user_federation_mapper_config (user_federation_mapper_id, value, name) FROM stdin;
\.


--
-- Data for Name: user_federation_provider; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.user_federation_provider (id, changed_sync_period, display_name, full_sync_period, last_sync, priority, provider_name, realm_id) FROM stdin;
\.


--
-- Data for Name: user_group_membership; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.user_group_membership (group_id, user_id) FROM stdin;
b4737f4e-349d-42a1-8149-e4cf40b06171	a82979e2-0863-4dea-8055-b3b93803c90b
631df8db-8023-4003-83f1-c196b819e6d2	a82979e2-0863-4dea-8055-b3b93803c90b
\.


--
-- Data for Name: user_required_action; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.user_required_action (user_id, required_action) FROM stdin;
\.


--
-- Data for Name: user_role_mapping; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.user_role_mapping (role_id, user_id) FROM stdin;
7f34dbbf-9030-4de5-9c21-c6715f36c748	4ee0ebb9-ab98-4fe7-bfe1-d44727a39c4f
848d0788-e5d5-4a2c-87fb-911c46cf213a	4ee0ebb9-ab98-4fe7-bfe1-d44727a39c4f
aa1e1645-5ddc-4219-84fd-42b806510732	a82979e2-0863-4dea-8055-b3b93803c90b
\.


--
-- Data for Name: user_session; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.user_session (id, auth_method, ip_address, last_session_refresh, login_username, realm_id, remember_me, started, user_id, user_session_state, broker_session_id, broker_user_id) FROM stdin;
\.


--
-- Data for Name: user_session_note; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.user_session_note (user_session, name, value) FROM stdin;
\.


--
-- Data for Name: username_login_failure; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.username_login_failure (realm_id, username, failed_login_not_before, last_failure, last_ip_failure, num_failures) FROM stdin;
\.


--
-- Data for Name: web_origins; Type: TABLE DATA; Schema: public; Owner: keycloak
--

COPY public.web_origins (client_id, value) FROM stdin;
79c8f459-6cb5-43cf-9527-51d3489cf8ac	+
472187cb-354b-42de-acad-b0d8f8a9c031	http://localhost:4200
f41d3566-a7d2-4326-8425-4c9ce085d282	+
\.


--
-- Name: username_login_failure CONSTRAINT_17-2; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.username_login_failure
    ADD CONSTRAINT "CONSTRAINT_17-2" PRIMARY KEY (realm_id, username);


--
-- Name: keycloak_role UK_J3RWUVD56ONTGSUHOGM184WW2-2; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.keycloak_role
    ADD CONSTRAINT "UK_J3RWUVD56ONTGSUHOGM184WW2-2" UNIQUE (name, client_realm_constraint);


--
-- Name: client_auth_flow_bindings c_cli_flow_bind; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_auth_flow_bindings
    ADD CONSTRAINT c_cli_flow_bind PRIMARY KEY (client_id, binding_name);


--
-- Name: client_scope_client c_cli_scope_bind; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_scope_client
    ADD CONSTRAINT c_cli_scope_bind PRIMARY KEY (client_id, scope_id);


--
-- Name: client_initial_access cnstr_client_init_acc_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_initial_access
    ADD CONSTRAINT cnstr_client_init_acc_pk PRIMARY KEY (id);


--
-- Name: realm_default_groups con_group_id_def_groups; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_default_groups
    ADD CONSTRAINT con_group_id_def_groups UNIQUE (group_id);


--
-- Name: broker_link constr_broker_link_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.broker_link
    ADD CONSTRAINT constr_broker_link_pk PRIMARY KEY (identity_provider, user_id);


--
-- Name: client_user_session_note constr_cl_usr_ses_note; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_user_session_note
    ADD CONSTRAINT constr_cl_usr_ses_note PRIMARY KEY (client_session, name);


--
-- Name: component_config constr_component_config_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.component_config
    ADD CONSTRAINT constr_component_config_pk PRIMARY KEY (id);


--
-- Name: component constr_component_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.component
    ADD CONSTRAINT constr_component_pk PRIMARY KEY (id);


--
-- Name: fed_user_required_action constr_fed_required_action; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.fed_user_required_action
    ADD CONSTRAINT constr_fed_required_action PRIMARY KEY (required_action, user_id);


--
-- Name: fed_user_attribute constr_fed_user_attr_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.fed_user_attribute
    ADD CONSTRAINT constr_fed_user_attr_pk PRIMARY KEY (id);


--
-- Name: fed_user_consent constr_fed_user_consent_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.fed_user_consent
    ADD CONSTRAINT constr_fed_user_consent_pk PRIMARY KEY (id);


--
-- Name: fed_user_credential constr_fed_user_cred_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.fed_user_credential
    ADD CONSTRAINT constr_fed_user_cred_pk PRIMARY KEY (id);


--
-- Name: fed_user_group_membership constr_fed_user_group; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.fed_user_group_membership
    ADD CONSTRAINT constr_fed_user_group PRIMARY KEY (group_id, user_id);


--
-- Name: fed_user_role_mapping constr_fed_user_role; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.fed_user_role_mapping
    ADD CONSTRAINT constr_fed_user_role PRIMARY KEY (role_id, user_id);


--
-- Name: federated_user constr_federated_user; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.federated_user
    ADD CONSTRAINT constr_federated_user PRIMARY KEY (id);


--
-- Name: realm_default_groups constr_realm_default_groups; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_default_groups
    ADD CONSTRAINT constr_realm_default_groups PRIMARY KEY (realm_id, group_id);


--
-- Name: realm_enabled_event_types constr_realm_enabl_event_types; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_enabled_event_types
    ADD CONSTRAINT constr_realm_enabl_event_types PRIMARY KEY (realm_id, value);


--
-- Name: realm_events_listeners constr_realm_events_listeners; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_events_listeners
    ADD CONSTRAINT constr_realm_events_listeners PRIMARY KEY (realm_id, value);


--
-- Name: realm_supported_locales constr_realm_supported_locales; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_supported_locales
    ADD CONSTRAINT constr_realm_supported_locales PRIMARY KEY (realm_id, value);


--
-- Name: identity_provider constraint_2b; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.identity_provider
    ADD CONSTRAINT constraint_2b PRIMARY KEY (internal_id);


--
-- Name: client_attributes constraint_3c; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_attributes
    ADD CONSTRAINT constraint_3c PRIMARY KEY (client_id, name);


--
-- Name: event_entity constraint_4; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.event_entity
    ADD CONSTRAINT constraint_4 PRIMARY KEY (id);


--
-- Name: federated_identity constraint_40; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.federated_identity
    ADD CONSTRAINT constraint_40 PRIMARY KEY (identity_provider, user_id);


--
-- Name: realm constraint_4a; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm
    ADD CONSTRAINT constraint_4a PRIMARY KEY (id);


--
-- Name: client_session_role constraint_5; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_session_role
    ADD CONSTRAINT constraint_5 PRIMARY KEY (client_session, role_id);


--
-- Name: user_session constraint_57; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_session
    ADD CONSTRAINT constraint_57 PRIMARY KEY (id);


--
-- Name: user_federation_provider constraint_5c; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_federation_provider
    ADD CONSTRAINT constraint_5c PRIMARY KEY (id);


--
-- Name: client_session_note constraint_5e; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_session_note
    ADD CONSTRAINT constraint_5e PRIMARY KEY (client_session, name);


--
-- Name: client constraint_7; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client
    ADD CONSTRAINT constraint_7 PRIMARY KEY (id);


--
-- Name: client_session constraint_8; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_session
    ADD CONSTRAINT constraint_8 PRIMARY KEY (id);


--
-- Name: scope_mapping constraint_81; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.scope_mapping
    ADD CONSTRAINT constraint_81 PRIMARY KEY (client_id, role_id);


--
-- Name: client_node_registrations constraint_84; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_node_registrations
    ADD CONSTRAINT constraint_84 PRIMARY KEY (client_id, name);


--
-- Name: realm_attribute constraint_9; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_attribute
    ADD CONSTRAINT constraint_9 PRIMARY KEY (name, realm_id);


--
-- Name: realm_required_credential constraint_92; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_required_credential
    ADD CONSTRAINT constraint_92 PRIMARY KEY (realm_id, type);


--
-- Name: keycloak_role constraint_a; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.keycloak_role
    ADD CONSTRAINT constraint_a PRIMARY KEY (id);


--
-- Name: admin_event_entity constraint_admin_event_entity; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.admin_event_entity
    ADD CONSTRAINT constraint_admin_event_entity PRIMARY KEY (id);


--
-- Name: authenticator_config_entry constraint_auth_cfg_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.authenticator_config_entry
    ADD CONSTRAINT constraint_auth_cfg_pk PRIMARY KEY (authenticator_id, name);


--
-- Name: authentication_execution constraint_auth_exec_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.authentication_execution
    ADD CONSTRAINT constraint_auth_exec_pk PRIMARY KEY (id);


--
-- Name: authentication_flow constraint_auth_flow_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.authentication_flow
    ADD CONSTRAINT constraint_auth_flow_pk PRIMARY KEY (id);


--
-- Name: authenticator_config constraint_auth_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.authenticator_config
    ADD CONSTRAINT constraint_auth_pk PRIMARY KEY (id);


--
-- Name: client_session_auth_status constraint_auth_status_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_session_auth_status
    ADD CONSTRAINT constraint_auth_status_pk PRIMARY KEY (client_session, authenticator);


--
-- Name: user_role_mapping constraint_c; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_role_mapping
    ADD CONSTRAINT constraint_c PRIMARY KEY (role_id, user_id);


--
-- Name: composite_role constraint_composite_role; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.composite_role
    ADD CONSTRAINT constraint_composite_role PRIMARY KEY (composite, child_role);


--
-- Name: client_session_prot_mapper constraint_cs_pmp_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_session_prot_mapper
    ADD CONSTRAINT constraint_cs_pmp_pk PRIMARY KEY (client_session, protocol_mapper_id);


--
-- Name: identity_provider_config constraint_d; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.identity_provider_config
    ADD CONSTRAINT constraint_d PRIMARY KEY (identity_provider_id, name);


--
-- Name: policy_config constraint_dpc; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.policy_config
    ADD CONSTRAINT constraint_dpc PRIMARY KEY (policy_id, name);


--
-- Name: realm_smtp_config constraint_e; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_smtp_config
    ADD CONSTRAINT constraint_e PRIMARY KEY (realm_id, name);


--
-- Name: credential constraint_f; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.credential
    ADD CONSTRAINT constraint_f PRIMARY KEY (id);


--
-- Name: user_federation_config constraint_f9; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_federation_config
    ADD CONSTRAINT constraint_f9 PRIMARY KEY (user_federation_provider_id, name);


--
-- Name: resource_server_perm_ticket constraint_fapmt; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server_perm_ticket
    ADD CONSTRAINT constraint_fapmt PRIMARY KEY (id);


--
-- Name: resource_server_resource constraint_farsr; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server_resource
    ADD CONSTRAINT constraint_farsr PRIMARY KEY (id);


--
-- Name: resource_server_policy constraint_farsrp; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server_policy
    ADD CONSTRAINT constraint_farsrp PRIMARY KEY (id);


--
-- Name: associated_policy constraint_farsrpap; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.associated_policy
    ADD CONSTRAINT constraint_farsrpap PRIMARY KEY (policy_id, associated_policy_id);


--
-- Name: resource_policy constraint_farsrpp; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_policy
    ADD CONSTRAINT constraint_farsrpp PRIMARY KEY (resource_id, policy_id);


--
-- Name: resource_server_scope constraint_farsrs; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server_scope
    ADD CONSTRAINT constraint_farsrs PRIMARY KEY (id);


--
-- Name: resource_scope constraint_farsrsp; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_scope
    ADD CONSTRAINT constraint_farsrsp PRIMARY KEY (resource_id, scope_id);


--
-- Name: scope_policy constraint_farsrsps; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.scope_policy
    ADD CONSTRAINT constraint_farsrsps PRIMARY KEY (scope_id, policy_id);


--
-- Name: user_entity constraint_fb; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_entity
    ADD CONSTRAINT constraint_fb PRIMARY KEY (id);


--
-- Name: user_federation_mapper_config constraint_fedmapper_cfg_pm; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_federation_mapper_config
    ADD CONSTRAINT constraint_fedmapper_cfg_pm PRIMARY KEY (user_federation_mapper_id, name);


--
-- Name: user_federation_mapper constraint_fedmapperpm; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_federation_mapper
    ADD CONSTRAINT constraint_fedmapperpm PRIMARY KEY (id);


--
-- Name: fed_user_consent_cl_scope constraint_fgrntcsnt_clsc_pm; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.fed_user_consent_cl_scope
    ADD CONSTRAINT constraint_fgrntcsnt_clsc_pm PRIMARY KEY (user_consent_id, scope_id);


--
-- Name: user_consent_client_scope constraint_grntcsnt_clsc_pm; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_consent_client_scope
    ADD CONSTRAINT constraint_grntcsnt_clsc_pm PRIMARY KEY (user_consent_id, scope_id);


--
-- Name: user_consent constraint_grntcsnt_pm; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_consent
    ADD CONSTRAINT constraint_grntcsnt_pm PRIMARY KEY (id);


--
-- Name: keycloak_group constraint_group; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.keycloak_group
    ADD CONSTRAINT constraint_group PRIMARY KEY (id);


--
-- Name: group_attribute constraint_group_attribute_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.group_attribute
    ADD CONSTRAINT constraint_group_attribute_pk PRIMARY KEY (id);


--
-- Name: group_role_mapping constraint_group_role; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.group_role_mapping
    ADD CONSTRAINT constraint_group_role PRIMARY KEY (role_id, group_id);


--
-- Name: identity_provider_mapper constraint_idpm; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.identity_provider_mapper
    ADD CONSTRAINT constraint_idpm PRIMARY KEY (id);


--
-- Name: idp_mapper_config constraint_idpmconfig; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.idp_mapper_config
    ADD CONSTRAINT constraint_idpmconfig PRIMARY KEY (idp_mapper_id, name);


--
-- Name: migration_model constraint_migmod; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.migration_model
    ADD CONSTRAINT constraint_migmod PRIMARY KEY (id);


--
-- Name: offline_client_session constraint_offl_cl_ses_pk3; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.offline_client_session
    ADD CONSTRAINT constraint_offl_cl_ses_pk3 PRIMARY KEY (user_session_id, client_id, client_storage_provider, external_client_id, offline_flag);


--
-- Name: offline_user_session constraint_offl_us_ses_pk2; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.offline_user_session
    ADD CONSTRAINT constraint_offl_us_ses_pk2 PRIMARY KEY (user_session_id, offline_flag);


--
-- Name: protocol_mapper constraint_pcm; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.protocol_mapper
    ADD CONSTRAINT constraint_pcm PRIMARY KEY (id);


--
-- Name: protocol_mapper_config constraint_pmconfig; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.protocol_mapper_config
    ADD CONSTRAINT constraint_pmconfig PRIMARY KEY (protocol_mapper_id, name);


--
-- Name: redirect_uris constraint_redirect_uris; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.redirect_uris
    ADD CONSTRAINT constraint_redirect_uris PRIMARY KEY (client_id, value);


--
-- Name: required_action_config constraint_req_act_cfg_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.required_action_config
    ADD CONSTRAINT constraint_req_act_cfg_pk PRIMARY KEY (required_action_id, name);


--
-- Name: required_action_provider constraint_req_act_prv_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.required_action_provider
    ADD CONSTRAINT constraint_req_act_prv_pk PRIMARY KEY (id);


--
-- Name: user_required_action constraint_required_action; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_required_action
    ADD CONSTRAINT constraint_required_action PRIMARY KEY (required_action, user_id);


--
-- Name: resource_uris constraint_resour_uris_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_uris
    ADD CONSTRAINT constraint_resour_uris_pk PRIMARY KEY (resource_id, value);


--
-- Name: role_attribute constraint_role_attribute_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.role_attribute
    ADD CONSTRAINT constraint_role_attribute_pk PRIMARY KEY (id);


--
-- Name: user_attribute constraint_user_attribute_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_attribute
    ADD CONSTRAINT constraint_user_attribute_pk PRIMARY KEY (id);


--
-- Name: user_group_membership constraint_user_group; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_group_membership
    ADD CONSTRAINT constraint_user_group PRIMARY KEY (group_id, user_id);


--
-- Name: user_session_note constraint_usn_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_session_note
    ADD CONSTRAINT constraint_usn_pk PRIMARY KEY (user_session, name);


--
-- Name: web_origins constraint_web_origins; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.web_origins
    ADD CONSTRAINT constraint_web_origins PRIMARY KEY (client_id, value);


--
-- Name: databasechangeloglock databasechangeloglock_pkey; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.databasechangeloglock
    ADD CONSTRAINT databasechangeloglock_pkey PRIMARY KEY (id);


--
-- Name: client_scope_attributes pk_cl_tmpl_attr; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_scope_attributes
    ADD CONSTRAINT pk_cl_tmpl_attr PRIMARY KEY (scope_id, name);


--
-- Name: client_scope pk_cli_template; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_scope
    ADD CONSTRAINT pk_cli_template PRIMARY KEY (id);


--
-- Name: resource_server pk_resource_server; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server
    ADD CONSTRAINT pk_resource_server PRIMARY KEY (id);


--
-- Name: client_scope_role_mapping pk_template_scope; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_scope_role_mapping
    ADD CONSTRAINT pk_template_scope PRIMARY KEY (scope_id, role_id);


--
-- Name: default_client_scope r_def_cli_scope_bind; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.default_client_scope
    ADD CONSTRAINT r_def_cli_scope_bind PRIMARY KEY (realm_id, scope_id);


--
-- Name: realm_localizations realm_localizations_pkey; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_localizations
    ADD CONSTRAINT realm_localizations_pkey PRIMARY KEY (realm_id, locale);


--
-- Name: resource_attribute res_attr_pk; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_attribute
    ADD CONSTRAINT res_attr_pk PRIMARY KEY (id);


--
-- Name: keycloak_group sibling_names; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.keycloak_group
    ADD CONSTRAINT sibling_names UNIQUE (realm_id, parent_group, name);


--
-- Name: identity_provider uk_2daelwnibji49avxsrtuf6xj33; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.identity_provider
    ADD CONSTRAINT uk_2daelwnibji49avxsrtuf6xj33 UNIQUE (provider_alias, realm_id);


--
-- Name: client uk_b71cjlbenv945rb6gcon438at; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client
    ADD CONSTRAINT uk_b71cjlbenv945rb6gcon438at UNIQUE (realm_id, client_id);


--
-- Name: client_scope uk_cli_scope; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_scope
    ADD CONSTRAINT uk_cli_scope UNIQUE (realm_id, name);


--
-- Name: user_entity uk_dykn684sl8up1crfei6eckhd7; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_entity
    ADD CONSTRAINT uk_dykn684sl8up1crfei6eckhd7 UNIQUE (realm_id, email_constraint);


--
-- Name: resource_server_resource uk_frsr6t700s9v50bu18ws5ha6; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server_resource
    ADD CONSTRAINT uk_frsr6t700s9v50bu18ws5ha6 UNIQUE (name, owner, resource_server_id);


--
-- Name: resource_server_perm_ticket uk_frsr6t700s9v50bu18ws5pmt; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server_perm_ticket
    ADD CONSTRAINT uk_frsr6t700s9v50bu18ws5pmt UNIQUE (owner, requester, resource_server_id, resource_id, scope_id);


--
-- Name: resource_server_policy uk_frsrpt700s9v50bu18ws5ha6; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server_policy
    ADD CONSTRAINT uk_frsrpt700s9v50bu18ws5ha6 UNIQUE (name, resource_server_id);


--
-- Name: resource_server_scope uk_frsrst700s9v50bu18ws5ha6; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server_scope
    ADD CONSTRAINT uk_frsrst700s9v50bu18ws5ha6 UNIQUE (name, resource_server_id);


--
-- Name: user_consent uk_jkuwuvd56ontgsuhogm8uewrt; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_consent
    ADD CONSTRAINT uk_jkuwuvd56ontgsuhogm8uewrt UNIQUE (client_id, client_storage_provider, external_client_id, user_id);


--
-- Name: realm uk_orvsdmla56612eaefiq6wl5oi; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm
    ADD CONSTRAINT uk_orvsdmla56612eaefiq6wl5oi UNIQUE (name);


--
-- Name: user_entity uk_ru8tt6t700s9v50bu18ws5ha6; Type: CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_entity
    ADD CONSTRAINT uk_ru8tt6t700s9v50bu18ws5ha6 UNIQUE (realm_id, username);


--
-- Name: idx_admin_event_time; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_admin_event_time ON public.admin_event_entity USING btree (realm_id, admin_event_time);


--
-- Name: idx_assoc_pol_assoc_pol_id; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_assoc_pol_assoc_pol_id ON public.associated_policy USING btree (associated_policy_id);


--
-- Name: idx_auth_config_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_auth_config_realm ON public.authenticator_config USING btree (realm_id);


--
-- Name: idx_auth_exec_flow; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_auth_exec_flow ON public.authentication_execution USING btree (flow_id);


--
-- Name: idx_auth_exec_realm_flow; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_auth_exec_realm_flow ON public.authentication_execution USING btree (realm_id, flow_id);


--
-- Name: idx_auth_flow_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_auth_flow_realm ON public.authentication_flow USING btree (realm_id);


--
-- Name: idx_cl_clscope; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_cl_clscope ON public.client_scope_client USING btree (scope_id);


--
-- Name: idx_client_id; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_client_id ON public.client USING btree (client_id);


--
-- Name: idx_client_init_acc_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_client_init_acc_realm ON public.client_initial_access USING btree (realm_id);


--
-- Name: idx_client_session_session; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_client_session_session ON public.client_session USING btree (session_id);


--
-- Name: idx_clscope_attrs; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_clscope_attrs ON public.client_scope_attributes USING btree (scope_id);


--
-- Name: idx_clscope_cl; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_clscope_cl ON public.client_scope_client USING btree (client_id);


--
-- Name: idx_clscope_protmap; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_clscope_protmap ON public.protocol_mapper USING btree (client_scope_id);


--
-- Name: idx_clscope_role; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_clscope_role ON public.client_scope_role_mapping USING btree (scope_id);


--
-- Name: idx_compo_config_compo; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_compo_config_compo ON public.component_config USING btree (component_id);


--
-- Name: idx_component_provider_type; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_component_provider_type ON public.component USING btree (provider_type);


--
-- Name: idx_component_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_component_realm ON public.component USING btree (realm_id);


--
-- Name: idx_composite; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_composite ON public.composite_role USING btree (composite);


--
-- Name: idx_composite_child; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_composite_child ON public.composite_role USING btree (child_role);


--
-- Name: idx_defcls_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_defcls_realm ON public.default_client_scope USING btree (realm_id);


--
-- Name: idx_defcls_scope; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_defcls_scope ON public.default_client_scope USING btree (scope_id);


--
-- Name: idx_event_time; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_event_time ON public.event_entity USING btree (realm_id, event_time);


--
-- Name: idx_fedidentity_feduser; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_fedidentity_feduser ON public.federated_identity USING btree (federated_user_id);


--
-- Name: idx_fedidentity_user; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_fedidentity_user ON public.federated_identity USING btree (user_id);


--
-- Name: idx_fu_attribute; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_fu_attribute ON public.fed_user_attribute USING btree (user_id, realm_id, name);


--
-- Name: idx_fu_cnsnt_ext; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_fu_cnsnt_ext ON public.fed_user_consent USING btree (user_id, client_storage_provider, external_client_id);


--
-- Name: idx_fu_consent; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_fu_consent ON public.fed_user_consent USING btree (user_id, client_id);


--
-- Name: idx_fu_consent_ru; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_fu_consent_ru ON public.fed_user_consent USING btree (realm_id, user_id);


--
-- Name: idx_fu_credential; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_fu_credential ON public.fed_user_credential USING btree (user_id, type);


--
-- Name: idx_fu_credential_ru; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_fu_credential_ru ON public.fed_user_credential USING btree (realm_id, user_id);


--
-- Name: idx_fu_group_membership; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_fu_group_membership ON public.fed_user_group_membership USING btree (user_id, group_id);


--
-- Name: idx_fu_group_membership_ru; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_fu_group_membership_ru ON public.fed_user_group_membership USING btree (realm_id, user_id);


--
-- Name: idx_fu_required_action; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_fu_required_action ON public.fed_user_required_action USING btree (user_id, required_action);


--
-- Name: idx_fu_required_action_ru; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_fu_required_action_ru ON public.fed_user_required_action USING btree (realm_id, user_id);


--
-- Name: idx_fu_role_mapping; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_fu_role_mapping ON public.fed_user_role_mapping USING btree (user_id, role_id);


--
-- Name: idx_fu_role_mapping_ru; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_fu_role_mapping_ru ON public.fed_user_role_mapping USING btree (realm_id, user_id);


--
-- Name: idx_group_att_by_name_value; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_group_att_by_name_value ON public.group_attribute USING btree (name, ((value)::character varying(250)));


--
-- Name: idx_group_attr_group; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_group_attr_group ON public.group_attribute USING btree (group_id);


--
-- Name: idx_group_role_mapp_group; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_group_role_mapp_group ON public.group_role_mapping USING btree (group_id);


--
-- Name: idx_id_prov_mapp_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_id_prov_mapp_realm ON public.identity_provider_mapper USING btree (realm_id);


--
-- Name: idx_ident_prov_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_ident_prov_realm ON public.identity_provider USING btree (realm_id);


--
-- Name: idx_keycloak_role_client; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_keycloak_role_client ON public.keycloak_role USING btree (client);


--
-- Name: idx_keycloak_role_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_keycloak_role_realm ON public.keycloak_role USING btree (realm);


--
-- Name: idx_offline_css_preload; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_offline_css_preload ON public.offline_client_session USING btree (client_id, offline_flag);


--
-- Name: idx_offline_uss_by_user; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_offline_uss_by_user ON public.offline_user_session USING btree (user_id, realm_id, offline_flag);


--
-- Name: idx_offline_uss_by_usersess; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_offline_uss_by_usersess ON public.offline_user_session USING btree (realm_id, offline_flag, user_session_id);


--
-- Name: idx_offline_uss_createdon; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_offline_uss_createdon ON public.offline_user_session USING btree (created_on);


--
-- Name: idx_offline_uss_preload; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_offline_uss_preload ON public.offline_user_session USING btree (offline_flag, created_on, user_session_id);


--
-- Name: idx_protocol_mapper_client; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_protocol_mapper_client ON public.protocol_mapper USING btree (client_id);


--
-- Name: idx_realm_attr_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_realm_attr_realm ON public.realm_attribute USING btree (realm_id);


--
-- Name: idx_realm_clscope; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_realm_clscope ON public.client_scope USING btree (realm_id);


--
-- Name: idx_realm_def_grp_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_realm_def_grp_realm ON public.realm_default_groups USING btree (realm_id);


--
-- Name: idx_realm_evt_list_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_realm_evt_list_realm ON public.realm_events_listeners USING btree (realm_id);


--
-- Name: idx_realm_evt_types_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_realm_evt_types_realm ON public.realm_enabled_event_types USING btree (realm_id);


--
-- Name: idx_realm_master_adm_cli; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_realm_master_adm_cli ON public.realm USING btree (master_admin_client);


--
-- Name: idx_realm_supp_local_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_realm_supp_local_realm ON public.realm_supported_locales USING btree (realm_id);


--
-- Name: idx_redir_uri_client; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_redir_uri_client ON public.redirect_uris USING btree (client_id);


--
-- Name: idx_req_act_prov_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_req_act_prov_realm ON public.required_action_provider USING btree (realm_id);


--
-- Name: idx_res_policy_policy; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_res_policy_policy ON public.resource_policy USING btree (policy_id);


--
-- Name: idx_res_scope_scope; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_res_scope_scope ON public.resource_scope USING btree (scope_id);


--
-- Name: idx_res_serv_pol_res_serv; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_res_serv_pol_res_serv ON public.resource_server_policy USING btree (resource_server_id);


--
-- Name: idx_res_srv_res_res_srv; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_res_srv_res_res_srv ON public.resource_server_resource USING btree (resource_server_id);


--
-- Name: idx_res_srv_scope_res_srv; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_res_srv_scope_res_srv ON public.resource_server_scope USING btree (resource_server_id);


--
-- Name: idx_role_attribute; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_role_attribute ON public.role_attribute USING btree (role_id);


--
-- Name: idx_role_clscope; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_role_clscope ON public.client_scope_role_mapping USING btree (role_id);


--
-- Name: idx_scope_mapping_role; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_scope_mapping_role ON public.scope_mapping USING btree (role_id);


--
-- Name: idx_scope_policy_policy; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_scope_policy_policy ON public.scope_policy USING btree (policy_id);


--
-- Name: idx_update_time; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_update_time ON public.migration_model USING btree (update_time);


--
-- Name: idx_us_sess_id_on_cl_sess; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_us_sess_id_on_cl_sess ON public.offline_client_session USING btree (user_session_id);


--
-- Name: idx_usconsent_clscope; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_usconsent_clscope ON public.user_consent_client_scope USING btree (user_consent_id);


--
-- Name: idx_user_attribute; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_user_attribute ON public.user_attribute USING btree (user_id);


--
-- Name: idx_user_attribute_name; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_user_attribute_name ON public.user_attribute USING btree (name, value);


--
-- Name: idx_user_consent; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_user_consent ON public.user_consent USING btree (user_id);


--
-- Name: idx_user_credential; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_user_credential ON public.credential USING btree (user_id);


--
-- Name: idx_user_email; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_user_email ON public.user_entity USING btree (email);


--
-- Name: idx_user_group_mapping; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_user_group_mapping ON public.user_group_membership USING btree (user_id);


--
-- Name: idx_user_reqactions; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_user_reqactions ON public.user_required_action USING btree (user_id);


--
-- Name: idx_user_role_mapping; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_user_role_mapping ON public.user_role_mapping USING btree (user_id);


--
-- Name: idx_user_service_account; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_user_service_account ON public.user_entity USING btree (realm_id, service_account_client_link);


--
-- Name: idx_usr_fed_map_fed_prv; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_usr_fed_map_fed_prv ON public.user_federation_mapper USING btree (federation_provider_id);


--
-- Name: idx_usr_fed_map_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_usr_fed_map_realm ON public.user_federation_mapper USING btree (realm_id);


--
-- Name: idx_usr_fed_prv_realm; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_usr_fed_prv_realm ON public.user_federation_provider USING btree (realm_id);


--
-- Name: idx_web_orig_client; Type: INDEX; Schema: public; Owner: keycloak
--

CREATE INDEX idx_web_orig_client ON public.web_origins USING btree (client_id);


--
-- Name: client_session_auth_status auth_status_constraint; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_session_auth_status
    ADD CONSTRAINT auth_status_constraint FOREIGN KEY (client_session) REFERENCES public.client_session(id);


--
-- Name: identity_provider fk2b4ebc52ae5c3b34; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.identity_provider
    ADD CONSTRAINT fk2b4ebc52ae5c3b34 FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: client_attributes fk3c47c64beacca966; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_attributes
    ADD CONSTRAINT fk3c47c64beacca966 FOREIGN KEY (client_id) REFERENCES public.client(id);


--
-- Name: federated_identity fk404288b92ef007a6; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.federated_identity
    ADD CONSTRAINT fk404288b92ef007a6 FOREIGN KEY (user_id) REFERENCES public.user_entity(id);


--
-- Name: client_node_registrations fk4129723ba992f594; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_node_registrations
    ADD CONSTRAINT fk4129723ba992f594 FOREIGN KEY (client_id) REFERENCES public.client(id);


--
-- Name: client_session_note fk5edfb00ff51c2736; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_session_note
    ADD CONSTRAINT fk5edfb00ff51c2736 FOREIGN KEY (client_session) REFERENCES public.client_session(id);


--
-- Name: user_session_note fk5edfb00ff51d3472; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_session_note
    ADD CONSTRAINT fk5edfb00ff51d3472 FOREIGN KEY (user_session) REFERENCES public.user_session(id);


--
-- Name: client_session_role fk_11b7sgqw18i532811v7o2dv76; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_session_role
    ADD CONSTRAINT fk_11b7sgqw18i532811v7o2dv76 FOREIGN KEY (client_session) REFERENCES public.client_session(id);


--
-- Name: redirect_uris fk_1burs8pb4ouj97h5wuppahv9f; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.redirect_uris
    ADD CONSTRAINT fk_1burs8pb4ouj97h5wuppahv9f FOREIGN KEY (client_id) REFERENCES public.client(id);


--
-- Name: user_federation_provider fk_1fj32f6ptolw2qy60cd8n01e8; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_federation_provider
    ADD CONSTRAINT fk_1fj32f6ptolw2qy60cd8n01e8 FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: client_session_prot_mapper fk_33a8sgqw18i532811v7o2dk89; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_session_prot_mapper
    ADD CONSTRAINT fk_33a8sgqw18i532811v7o2dk89 FOREIGN KEY (client_session) REFERENCES public.client_session(id);


--
-- Name: realm_required_credential fk_5hg65lybevavkqfki3kponh9v; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_required_credential
    ADD CONSTRAINT fk_5hg65lybevavkqfki3kponh9v FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: resource_attribute fk_5hrm2vlf9ql5fu022kqepovbr; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_attribute
    ADD CONSTRAINT fk_5hrm2vlf9ql5fu022kqepovbr FOREIGN KEY (resource_id) REFERENCES public.resource_server_resource(id);


--
-- Name: user_attribute fk_5hrm2vlf9ql5fu043kqepovbr; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_attribute
    ADD CONSTRAINT fk_5hrm2vlf9ql5fu043kqepovbr FOREIGN KEY (user_id) REFERENCES public.user_entity(id);


--
-- Name: user_required_action fk_6qj3w1jw9cvafhe19bwsiuvmd; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_required_action
    ADD CONSTRAINT fk_6qj3w1jw9cvafhe19bwsiuvmd FOREIGN KEY (user_id) REFERENCES public.user_entity(id);


--
-- Name: keycloak_role fk_6vyqfe4cn4wlq8r6kt5vdsj5c; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.keycloak_role
    ADD CONSTRAINT fk_6vyqfe4cn4wlq8r6kt5vdsj5c FOREIGN KEY (realm) REFERENCES public.realm(id);


--
-- Name: realm_smtp_config fk_70ej8xdxgxd0b9hh6180irr0o; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_smtp_config
    ADD CONSTRAINT fk_70ej8xdxgxd0b9hh6180irr0o FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: realm_attribute fk_8shxd6l3e9atqukacxgpffptw; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_attribute
    ADD CONSTRAINT fk_8shxd6l3e9atqukacxgpffptw FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: composite_role fk_a63wvekftu8jo1pnj81e7mce2; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.composite_role
    ADD CONSTRAINT fk_a63wvekftu8jo1pnj81e7mce2 FOREIGN KEY (composite) REFERENCES public.keycloak_role(id);


--
-- Name: authentication_execution fk_auth_exec_flow; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.authentication_execution
    ADD CONSTRAINT fk_auth_exec_flow FOREIGN KEY (flow_id) REFERENCES public.authentication_flow(id);


--
-- Name: authentication_execution fk_auth_exec_realm; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.authentication_execution
    ADD CONSTRAINT fk_auth_exec_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: authentication_flow fk_auth_flow_realm; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.authentication_flow
    ADD CONSTRAINT fk_auth_flow_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: authenticator_config fk_auth_realm; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.authenticator_config
    ADD CONSTRAINT fk_auth_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: client_session fk_b4ao2vcvat6ukau74wbwtfqo1; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_session
    ADD CONSTRAINT fk_b4ao2vcvat6ukau74wbwtfqo1 FOREIGN KEY (session_id) REFERENCES public.user_session(id);


--
-- Name: user_role_mapping fk_c4fqv34p1mbylloxang7b1q3l; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_role_mapping
    ADD CONSTRAINT fk_c4fqv34p1mbylloxang7b1q3l FOREIGN KEY (user_id) REFERENCES public.user_entity(id);


--
-- Name: client_scope_attributes fk_cl_scope_attr_scope; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_scope_attributes
    ADD CONSTRAINT fk_cl_scope_attr_scope FOREIGN KEY (scope_id) REFERENCES public.client_scope(id);


--
-- Name: client_scope_role_mapping fk_cl_scope_rm_scope; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_scope_role_mapping
    ADD CONSTRAINT fk_cl_scope_rm_scope FOREIGN KEY (scope_id) REFERENCES public.client_scope(id);


--
-- Name: client_user_session_note fk_cl_usr_ses_note; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_user_session_note
    ADD CONSTRAINT fk_cl_usr_ses_note FOREIGN KEY (client_session) REFERENCES public.client_session(id);


--
-- Name: protocol_mapper fk_cli_scope_mapper; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.protocol_mapper
    ADD CONSTRAINT fk_cli_scope_mapper FOREIGN KEY (client_scope_id) REFERENCES public.client_scope(id);


--
-- Name: client_initial_access fk_client_init_acc_realm; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.client_initial_access
    ADD CONSTRAINT fk_client_init_acc_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: component_config fk_component_config; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.component_config
    ADD CONSTRAINT fk_component_config FOREIGN KEY (component_id) REFERENCES public.component(id);


--
-- Name: component fk_component_realm; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.component
    ADD CONSTRAINT fk_component_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: realm_default_groups fk_def_groups_realm; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_default_groups
    ADD CONSTRAINT fk_def_groups_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: user_federation_mapper_config fk_fedmapper_cfg; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_federation_mapper_config
    ADD CONSTRAINT fk_fedmapper_cfg FOREIGN KEY (user_federation_mapper_id) REFERENCES public.user_federation_mapper(id);


--
-- Name: user_federation_mapper fk_fedmapperpm_fedprv; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_federation_mapper
    ADD CONSTRAINT fk_fedmapperpm_fedprv FOREIGN KEY (federation_provider_id) REFERENCES public.user_federation_provider(id);


--
-- Name: user_federation_mapper fk_fedmapperpm_realm; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_federation_mapper
    ADD CONSTRAINT fk_fedmapperpm_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: associated_policy fk_frsr5s213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.associated_policy
    ADD CONSTRAINT fk_frsr5s213xcx4wnkog82ssrfy FOREIGN KEY (associated_policy_id) REFERENCES public.resource_server_policy(id);


--
-- Name: scope_policy fk_frsrasp13xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.scope_policy
    ADD CONSTRAINT fk_frsrasp13xcx4wnkog82ssrfy FOREIGN KEY (policy_id) REFERENCES public.resource_server_policy(id);


--
-- Name: resource_server_perm_ticket fk_frsrho213xcx4wnkog82sspmt; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server_perm_ticket
    ADD CONSTRAINT fk_frsrho213xcx4wnkog82sspmt FOREIGN KEY (resource_server_id) REFERENCES public.resource_server(id);


--
-- Name: resource_server_resource fk_frsrho213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server_resource
    ADD CONSTRAINT fk_frsrho213xcx4wnkog82ssrfy FOREIGN KEY (resource_server_id) REFERENCES public.resource_server(id);


--
-- Name: resource_server_perm_ticket fk_frsrho213xcx4wnkog83sspmt; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server_perm_ticket
    ADD CONSTRAINT fk_frsrho213xcx4wnkog83sspmt FOREIGN KEY (resource_id) REFERENCES public.resource_server_resource(id);


--
-- Name: resource_server_perm_ticket fk_frsrho213xcx4wnkog84sspmt; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server_perm_ticket
    ADD CONSTRAINT fk_frsrho213xcx4wnkog84sspmt FOREIGN KEY (scope_id) REFERENCES public.resource_server_scope(id);


--
-- Name: associated_policy fk_frsrpas14xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.associated_policy
    ADD CONSTRAINT fk_frsrpas14xcx4wnkog82ssrfy FOREIGN KEY (policy_id) REFERENCES public.resource_server_policy(id);


--
-- Name: scope_policy fk_frsrpass3xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.scope_policy
    ADD CONSTRAINT fk_frsrpass3xcx4wnkog82ssrfy FOREIGN KEY (scope_id) REFERENCES public.resource_server_scope(id);


--
-- Name: resource_server_perm_ticket fk_frsrpo2128cx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server_perm_ticket
    ADD CONSTRAINT fk_frsrpo2128cx4wnkog82ssrfy FOREIGN KEY (policy_id) REFERENCES public.resource_server_policy(id);


--
-- Name: resource_server_policy fk_frsrpo213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server_policy
    ADD CONSTRAINT fk_frsrpo213xcx4wnkog82ssrfy FOREIGN KEY (resource_server_id) REFERENCES public.resource_server(id);


--
-- Name: resource_scope fk_frsrpos13xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_scope
    ADD CONSTRAINT fk_frsrpos13xcx4wnkog82ssrfy FOREIGN KEY (resource_id) REFERENCES public.resource_server_resource(id);


--
-- Name: resource_policy fk_frsrpos53xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_policy
    ADD CONSTRAINT fk_frsrpos53xcx4wnkog82ssrfy FOREIGN KEY (resource_id) REFERENCES public.resource_server_resource(id);


--
-- Name: resource_policy fk_frsrpp213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_policy
    ADD CONSTRAINT fk_frsrpp213xcx4wnkog82ssrfy FOREIGN KEY (policy_id) REFERENCES public.resource_server_policy(id);


--
-- Name: resource_scope fk_frsrps213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_scope
    ADD CONSTRAINT fk_frsrps213xcx4wnkog82ssrfy FOREIGN KEY (scope_id) REFERENCES public.resource_server_scope(id);


--
-- Name: resource_server_scope fk_frsrso213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_server_scope
    ADD CONSTRAINT fk_frsrso213xcx4wnkog82ssrfy FOREIGN KEY (resource_server_id) REFERENCES public.resource_server(id);


--
-- Name: composite_role fk_gr7thllb9lu8q4vqa4524jjy8; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.composite_role
    ADD CONSTRAINT fk_gr7thllb9lu8q4vqa4524jjy8 FOREIGN KEY (child_role) REFERENCES public.keycloak_role(id);


--
-- Name: user_consent_client_scope fk_grntcsnt_clsc_usc; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_consent_client_scope
    ADD CONSTRAINT fk_grntcsnt_clsc_usc FOREIGN KEY (user_consent_id) REFERENCES public.user_consent(id);


--
-- Name: user_consent fk_grntcsnt_user; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_consent
    ADD CONSTRAINT fk_grntcsnt_user FOREIGN KEY (user_id) REFERENCES public.user_entity(id);


--
-- Name: group_attribute fk_group_attribute_group; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.group_attribute
    ADD CONSTRAINT fk_group_attribute_group FOREIGN KEY (group_id) REFERENCES public.keycloak_group(id);


--
-- Name: group_role_mapping fk_group_role_group; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.group_role_mapping
    ADD CONSTRAINT fk_group_role_group FOREIGN KEY (group_id) REFERENCES public.keycloak_group(id);


--
-- Name: realm_enabled_event_types fk_h846o4h0w8epx5nwedrf5y69j; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_enabled_event_types
    ADD CONSTRAINT fk_h846o4h0w8epx5nwedrf5y69j FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: realm_events_listeners fk_h846o4h0w8epx5nxev9f5y69j; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_events_listeners
    ADD CONSTRAINT fk_h846o4h0w8epx5nxev9f5y69j FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: identity_provider_mapper fk_idpm_realm; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.identity_provider_mapper
    ADD CONSTRAINT fk_idpm_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: idp_mapper_config fk_idpmconfig; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.idp_mapper_config
    ADD CONSTRAINT fk_idpmconfig FOREIGN KEY (idp_mapper_id) REFERENCES public.identity_provider_mapper(id);


--
-- Name: web_origins fk_lojpho213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.web_origins
    ADD CONSTRAINT fk_lojpho213xcx4wnkog82ssrfy FOREIGN KEY (client_id) REFERENCES public.client(id);


--
-- Name: scope_mapping fk_ouse064plmlr732lxjcn1q5f1; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.scope_mapping
    ADD CONSTRAINT fk_ouse064plmlr732lxjcn1q5f1 FOREIGN KEY (client_id) REFERENCES public.client(id);


--
-- Name: protocol_mapper fk_pcm_realm; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.protocol_mapper
    ADD CONSTRAINT fk_pcm_realm FOREIGN KEY (client_id) REFERENCES public.client(id);


--
-- Name: credential fk_pfyr0glasqyl0dei3kl69r6v0; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.credential
    ADD CONSTRAINT fk_pfyr0glasqyl0dei3kl69r6v0 FOREIGN KEY (user_id) REFERENCES public.user_entity(id);


--
-- Name: protocol_mapper_config fk_pmconfig; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.protocol_mapper_config
    ADD CONSTRAINT fk_pmconfig FOREIGN KEY (protocol_mapper_id) REFERENCES public.protocol_mapper(id);


--
-- Name: default_client_scope fk_r_def_cli_scope_realm; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.default_client_scope
    ADD CONSTRAINT fk_r_def_cli_scope_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: required_action_provider fk_req_act_realm; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.required_action_provider
    ADD CONSTRAINT fk_req_act_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: resource_uris fk_resource_server_uris; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.resource_uris
    ADD CONSTRAINT fk_resource_server_uris FOREIGN KEY (resource_id) REFERENCES public.resource_server_resource(id);


--
-- Name: role_attribute fk_role_attribute_id; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.role_attribute
    ADD CONSTRAINT fk_role_attribute_id FOREIGN KEY (role_id) REFERENCES public.keycloak_role(id);


--
-- Name: realm_supported_locales fk_supported_locales_realm; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.realm_supported_locales
    ADD CONSTRAINT fk_supported_locales_realm FOREIGN KEY (realm_id) REFERENCES public.realm(id);


--
-- Name: user_federation_config fk_t13hpu1j94r2ebpekr39x5eu5; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_federation_config
    ADD CONSTRAINT fk_t13hpu1j94r2ebpekr39x5eu5 FOREIGN KEY (user_federation_provider_id) REFERENCES public.user_federation_provider(id);


--
-- Name: user_group_membership fk_user_group_user; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.user_group_membership
    ADD CONSTRAINT fk_user_group_user FOREIGN KEY (user_id) REFERENCES public.user_entity(id);


--
-- Name: policy_config fkdc34197cf864c4e43; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.policy_config
    ADD CONSTRAINT fkdc34197cf864c4e43 FOREIGN KEY (policy_id) REFERENCES public.resource_server_policy(id);


--
-- Name: identity_provider_config fkdc4897cf864c4e43; Type: FK CONSTRAINT; Schema: public; Owner: keycloak
--

ALTER TABLE ONLY public.identity_provider_config
    ADD CONSTRAINT fkdc4897cf864c4e43 FOREIGN KEY (identity_provider_id) REFERENCES public.identity_provider(internal_id);


--
-- PostgreSQL database dump complete
--

